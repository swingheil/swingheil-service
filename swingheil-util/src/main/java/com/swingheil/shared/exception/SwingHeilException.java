package com.swingheil.shared.exception;

public class SwingHeilException extends RuntimeException {
	//
	private static final long serialVersionUID = 8378557144596883602L;

	public SwingHeilException(String message) {
		//
		super(message);
	}
	
	public SwingHeilException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
