package com.swingheil.util;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateUtil {
	//
	
	/**
	 * java.sql.Date 객체를 yyyy-MM-dd 형식의 문자열로 변환한다.
	 * 
	 * @param date {@link java.sql.Date}
	 * @return
	 */
	public static String convertSqlDateToString(java.sql.Date date) {
		//
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}
	
	/**
	 * yyyy-MM-dd 형식의 문자열을 java.sql.Date 객체로 변환한다.
	 * 
	 * @param dateStr yyyy-mm-dd
	 * @return {@link java.sql.Date}
	 */
	public static java.sql.Date convertStringToSqlDate(String dateStr) {
		//
		return Date.valueOf(dateStr);
	}
	
	/**
	 * java.sql.Time 객체를 hh:mm 형식의 문자열로 변환한다.
	 * 
	 * @param time {@link java.sql.Time}
	 * @return
	 */
	public static String convertSqlTimeToString(java.sql.Time time) {
		//
		String fullTime = time.toString();
		return fullTime.substring(0, 5);
	}
	
	/**
	 * hh:mm 형식의 문자열을 java.sql.Time 객체로 변환한다.
	 * 
	 * @param timeStr hh:mm
	 * @return {@link java.sql.Time}
	 */
	public static java.sql.Time convertStringToSqlTime(String timeStr) {
		//
		if (timeStr.length() == 5) {
			timeStr = timeStr + ":00";
		}
		return Time.valueOf(timeStr);
	}
}
