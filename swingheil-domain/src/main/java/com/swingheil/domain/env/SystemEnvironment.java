package com.swingheil.domain.env;

/**
 * 시스템 환경변수
 */
public class SystemEnvironment {
	//
	/** google app key */
	private String googleAppKey;

	public String getGoogleAppKey() {
		return googleAppKey;
	}

	public void setGoogleAppKey(String googleAppKey) {
		this.googleAppKey = googleAppKey;
	}
}
