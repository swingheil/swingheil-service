package com.swingheil.domain.env;

public class MobileConfig {
	//
	private String appKey;

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
}
