package com.swingheil.domain.event;

import java.io.Serializable;
import java.util.Date;

import com.swingheil.domain.group.Group;
import com.swingheil.domain.shared.ClassLogger;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.place.Address;
import com.swingheil.domain.type.place.GeoLocation;

public class Event implements Serializable {
	//
	private static final long serialVersionUID = 6359897223447998472L;
	
	private String id;
	private CodeName category;
	private String title;
	private String summary;
	private Period period;
	private Address place;
	private Group sponsor;
	private String detail;

	private String smallImageId;
	private String largeImageId;
	
	private boolean favorite;
	private boolean isNew;
	private Date regDate;
	private boolean visible;
	private boolean notice;
	
	private GeoLocation location;
	
	//--------------------------------------------------------------------------

	public Event() {
		//
		this.regDate = new Date();
	}
	
	//--------------------------------------------------------------------------
	
	public String getThumbnailUrl() {
		//
		return "/web/event/" + id + "/thumbnail";
	}
	
	public String getPosterUrl() {
		//
		return "/web/event/" + id + "/poster";
	}
	
	//--------------------------------------------------------------------------
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CodeName getCategory() {
		return category;
	}
	public void setCategory(CodeName category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Period getPeriod() {
		return period;
	}
	public void setPeriod(Period period) {
		this.period = period;
	}
	public Address getPlace() {
		return place;
	}
	public void setPlace(Address place) {
		this.place = place;
	}
	public Group getSponsor() {
		return sponsor;
	}
	public void setSponsor(Group sponsor) {
		this.sponsor = sponsor;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getSmallImageId() {
		return smallImageId;
	}
	public void setSmallImageId(String smallImageId) {
		this.smallImageId = smallImageId;
	}
	public String getLargeImageId() {
		return largeImageId;
	}
	public void setLargeImageId(String largeImageId) {
		this.largeImageId = largeImageId;
	}
	public boolean isFavorite() {
		return favorite;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
	public boolean isVisible() {
		return visible;
	}
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	public boolean isNotice() {
		return notice;
	}
	public void setNotice(boolean notice) {
		this.notice = notice;
	}
	public GeoLocation getLocation() {
		return location;
	}
	public void setLocation(GeoLocation location) {
		this.location = location;
	}
	
}