package com.swingheil.domain.group;

import java.io.Serializable;

import com.swingheil.domain.shared.ClassLogger;

/**
 * 클럽
 */
public class Club extends Group implements Serializable {
	//
	private static final long serialVersionUID = -1211489524934293633L;

	private Bar bar;
	
	//--------------------------------------------------------------------------
	
	public String getThumbnailUrl() {
		//
		return "/web/club/" + getId() + "/thumbnail";
	}
	
	public String getBannerUrl() {
		//
		return "/web/club/" + getId() + "/banner";
	}
	
	//--------------------------------------------------------------------------

	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}

	@Override
	public String toString() {
		//
		return super.toString() + ClassLogger.toString(this);
	}
}
