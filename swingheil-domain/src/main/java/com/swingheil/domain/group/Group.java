package com.swingheil.domain.group;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.swingheil.domain.shared.ClassLogger;

public class Group implements Serializable {
	//
	private static final long serialVersionUID = -9063054669398492758L;
	
	private String id;
	private String nameKor;
	private String nameEng;
	private String homePage;
	private Staff admin;
	private List<Staff> staffs;
	
	private boolean favorite;
	private boolean isNew;
	private Date lastUpdateDate;
	
	private String thumbnailImageId;
	private String bannerImageId;
	
	//--------------------------------------------------------------------------
	
	public Group() {
		//
		this.lastUpdateDate = new Date();
	}
	
	public Group(String id) {
		//
		this.id = id;
	}
	
	//--------------------------------------------------------------------------
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNameKor() {
		return nameKor;
	}
	public void setNameKor(String nameKor) {
		this.nameKor = nameKor;
	}
	public String getNameEng() {
		return nameEng;
	}
	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	public Staff getAdmin() {
		return admin;
	}
	public void setAdmin(Staff admin) {
		this.admin = admin;
	}
	public List<Staff> getStaffs() {
		return staffs;
	}
	public void setStaffs(List<Staff> staffs) {
		this.staffs = staffs;
	}
	public boolean isFavorite() {
		return favorite;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	public String getThumbnailImageId() {
		return thumbnailImageId;
	}
	public void setThumbnailImageId(String thumbnailImageId) {
		this.thumbnailImageId = thumbnailImageId;
	}
	public String getBannerImageId() {
		return bannerImageId;
	}
	public void setBannerImageId(String bannerImageId) {
		this.bannerImageId = bannerImageId;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}
