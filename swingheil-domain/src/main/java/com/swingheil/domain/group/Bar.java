package com.swingheil.domain.group;

import java.io.Serializable;
import java.util.List;

import com.swingheil.domain.shared.ClassLogger;
import com.swingheil.domain.type.enums.BarType;
import com.swingheil.domain.type.place.Address;

/**
 * 스윙바
 */
public class Bar extends Group implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private BarType type;
	private Address address;
	private List<Club> clubs;
	
	//--------------------------------------------------------------------------
	
	public Bar() {
		//
	}
	
	public Bar(String barId) {
		// 
		setId(barId);
	}
	
	//--------------------------------------------------------------------------

	public String getThumbnailUrl() {
		//
		return "/web/bar/" + getId() + "/thumbnail";
	}
	
	public String getBannerUrl() {
		//
		return "/web/bar/" + getId() + "/banner";
	}
	
	//--------------------------------------------------------------------------
	
	public BarType getType() {
		return type;
	}

	public void setType(BarType type) {
		this.type = type;
	}
	
	public Address getAddress() {
		return address; 
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public List<Club> getClubs() {
		return clubs;
	}

	public void setClubs(List<Club> clubs) {
		this.clubs = clubs;
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}