package com.swingheil.domain.group;

import java.io.Serializable;

import com.swingheil.domain.shared.ClassLogger;
import com.swingheil.domain.type.enums.StaffState;
import com.swingheil.domain.user.SocialUser;

public class Staff implements Serializable {
	//
	private static final long serialVersionUID = -4240744283013826231L;
	
	private SocialUser roleUser;
	private StaffState state;
	
	//--------------------------------------------------------------------------
	
	public Staff(SocialUser roleUser) {
		//
		this.roleUser = roleUser;
	}
	
	//--------------------------------------------------------------------------
	
	public String getId() {
		//
		return roleUser.getId();
	}
	
	public String getNickname() {
		//
		return roleUser.getNickname();
	}
	
	public SocialUser getRoleUser() {
		return roleUser;
	}
	public void setRoleUser(SocialUser roleUser) {
		this.roleUser = roleUser;
	}
	public StaffState getState() {
		return state;
	}
	public void setState(StaffState state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}

}
