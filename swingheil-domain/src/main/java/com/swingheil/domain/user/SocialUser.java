package com.swingheil.domain.user;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.swingheil.domain.shared.ClassLogger;
import com.swingheil.domain.type.enums.UserState;

public class SocialUser implements Serializable {
	//
	private static final long serialVersionUID = 8795678187105547856L;
	
	private String id; // generated Id
	private String nickname;
	private List<Device> devices;
	
	private String name;
	private String email;
	private String phone;
	private String password;
	private UserState state;
	
	//--------------------------------------------------------------------------
	
	public SocialUser() {
	}
	
	public SocialUser(String nickname) {
		this.nickname = nickname;
	}
	
	//--------------------------------------------------------------------------
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Device> getDevices() {
		return devices;
	}
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}

	public void addDevice(Device device) {
		//
		if (devices == null) {
			devices = new ArrayList<Device>();
		}
		devices.add(device);
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}

	public UserState getState() {
		return state;
	}

	public void setState(UserState state) {
		this.state = state;
	}
}
