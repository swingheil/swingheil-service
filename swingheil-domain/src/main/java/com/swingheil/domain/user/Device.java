package com.swingheil.domain.user;

import java.io.Serializable;

import com.swingheil.domain.shared.ClassLogger;

public class Device implements Serializable {
	//
	private static final long serialVersionUID = -6405210823404942126L;
	
	private String deviceUid;
	private String modelId;
	private String osVersion;
	private String registrationId;
	
	//--------------------------------------------------------------------------
	// constructor
	
	public Device() {
		//
	}
	
	public Device(String deviceUid) {
		//
		this.deviceUid = deviceUid;
	}
	
	public Device(String deviceUid, String modelId, String osVersion, String registrationId) {
		//
		this.deviceUid = deviceUid;
		this.modelId = modelId;
		this.osVersion = osVersion;
		this.registrationId = registrationId;
	}

	//--------------------------------------------------------------------------
	// getter/setter
	
	public String getDeviceUid() {
		return deviceUid;
	}

	public void setDeviceUid(String deviceUid) {
		this.deviceUid = deviceUid;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	
	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}