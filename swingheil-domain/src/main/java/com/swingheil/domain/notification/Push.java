package com.swingheil.domain.notification;

import java.io.Serializable;
import java.util.Date;

public class Push implements Serializable {

	private static final long serialVersionUID = -4149031175387744663L;
	
	private String id;
	private int alertCount;
	private Date attempt;
	private Date created;
	private String deviceId;
	private String deviceType;
	private String errorMsg;
	private String message;
	private String pushType;
	private String sound;
	private String state;
	private String tag1;
	private String tag2;
	private String tag3;
	private String tag4;
	
	public Push() {
		this.created = new Date();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getAlertCount() {
		return alertCount;
	}
	public void setAlertCount(int alertCount) {
		this.alertCount = alertCount;
	}
	public Date getAttempt() {
		return attempt;
	}
	public void setAttempt(Date attempt) {
		this.attempt = attempt;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPushType() {
		return pushType;
	}
	public void setPushType(String pushType) {
		this.pushType = pushType;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTag1() {
		return tag1;
	}
	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}
	public String getTag2() {
		return tag2;
	}
	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}
	public String getTag3() {
		return tag3;
	}
	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}
	public String getTag4() {
		return tag4;
	}
	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

}
