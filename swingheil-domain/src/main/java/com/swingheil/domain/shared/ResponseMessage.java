package com.swingheil.domain.shared;

public class ResponseMessage {
	
	/** 성공여부 */
	private boolean success;
	
	/** 메시지 객체 */
	private Object message;
	
	//--------------------------------------------------------------------------
	
	public ResponseMessage(boolean success) {
		//
		this.success = success;
	}
	
	public ResponseMessage(Object message) {
		//
		this.success = true;
		this.message = message;
	}
	
	public ResponseMessage(boolean success, Object message) {
		//
		this.success = success;
		this.message = message;
	}
	
	public ResponseMessage(Throwable e) {
		//
		this.success = false;
		this.message = e.getMessage();
	}

	//--------------------------------------------------------------------------
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public Object getMessage() {
		return message;
	}
	public void setMessage(Object message) {
		this.message = message;
	}
}
