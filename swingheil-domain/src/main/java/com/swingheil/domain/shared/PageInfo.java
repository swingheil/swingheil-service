package com.swingheil.domain.shared;

public class PageInfo {
	//
	private int page;
	private int limit;
	
	// 검색조건없이 조회할지 여부
	// 예) 이벤트 목록조회 시, visible 여부를 무시할 때, true로 세팅함
	private boolean force; 
	
	//--------------------------------------------------------------------------
	
	public PageInfo() {
		//
		this.page = 1;
		this.limit = 10;
	}
	
	public PageInfo(int page, int limit) {
		//
		this.page = page;
		this.limit = limit;
	}
	
	//--------------------------------------------------------------------------
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public boolean isForce() {
		return force;
	}
	public void setForce(boolean force) {
		this.force = force;
	}
	
}
