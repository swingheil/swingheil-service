package com.swingheil.domain.shared;

import java.io.Serializable;
import java.util.List;

public class Page<T> implements Serializable {
	//
	private static final long serialVersionUID = 5677419699978202169L;
	
	private PageInfo pageInfo;
	private List<T> results;
	private boolean next;

	//--------------------------------------------------------------------------
	
	public Page(PageInfo pageInfo) {
		// 
		this.pageInfo = pageInfo;
	}

	public Page(List<T> results, PageInfo pageInfo) {
		//
		this.results = results;
		this.pageInfo = pageInfo;
	}
	
	//--------------------------------------------------------------------------
	

	public boolean hasNext() {
		//
		return next;
	}
	public boolean hasResults() {
		//
		return (results != null && !results.isEmpty()) ? true : false;
	}
	
	public boolean isNext() {
		return next;
	}
	public void setNext(boolean next) {
		this.next = next;
	}
	public PageInfo getCondition() {
		return pageInfo;
	}
	public void setCondition(PageInfo condition) {
		this.pageInfo = condition;
	}
	public List<T> getResults() {
		return results;
	}
	public void setResults(List<T> results) {
		this.results = results;
	}
}
