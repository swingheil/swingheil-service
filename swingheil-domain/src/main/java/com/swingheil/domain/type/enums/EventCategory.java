package com.swingheil.domain.type.enums;

import com.swingheil.domain.type.CodeName;

public enum EventCategory {
	//
	SwingHeil("01", "스윙하일"),
	SwingBar("02", "스윙바"),
	Club("03", "동호회");
	
	private String code;
	private String nameKor;
	
	//--------------------------------------------------------------------------
	
	private EventCategory(String code, String nameKor) {
		//
		this.code = code;
		this.nameKor = nameKor;
	}
	
	//--------------------------------------------------------------------------
	
	public String code() {
		return code;
	}
	
	public String nameKor() {
		return nameKor;
	}
	
	public CodeName convertCodeName() {
		//
		return new CodeName(code, nameKor);
	}
	
	public static EventCategory findBy(String code) {
		//
		for (EventCategory category : values()) {
			if (category.code().equals(code)) {
				return category;
			}
		}
		return null;
	}
}
