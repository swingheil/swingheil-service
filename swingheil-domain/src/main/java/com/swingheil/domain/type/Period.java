package com.swingheil.domain.type;

import java.io.Serializable;

import com.swingheil.domain.type.enums.PeriodType;

public class Period implements Serializable {
	//
	private static final long serialVersionUID = -5728403425217852395L;
	
	private PeriodType type;
	private DateTime from;
	private DateTime to;
	
	//--------------------------------------------------------------------------

	public Period() {
		//
		this.type = PeriodType.Day;
	}
	
	public Period(DateTime from) {
		// 
		this.type = PeriodType.Day;
		this.from = from;
	}
	
	public Period(String fromDate) {
		//
		this.type = PeriodType.Day;
		this.from = new DateTime(fromDate);
	}
	
	public Period(DateTime from, DateTime to) {
		// 
		this.type = PeriodType.Term;
		this.from = from;
		this.to = to;
	}
	
	public Period(String fromDate, String toDate) {
		//
		this.type = PeriodType.Term;
		this.from = new DateTime(fromDate);
		this.to = new DateTime(toDate);
	}
	
	//--------------------------------------------------------------------------
	
	public PeriodType getType() {
		return type;
	}
	public void setType(PeriodType type) {
		this.type = type;
	}
	public DateTime getFrom() {
		return from;
	}
	public void setFrom(DateTime from) {
		this.from = from;
	}
	public DateTime getTo() {
		return to;
	}
	public void setTo(DateTime to) {
		this.to = to;
	}
}
