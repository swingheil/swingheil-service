package com.swingheil.domain.type.place;

import java.io.Serializable;
import java.util.List;

import com.swingheil.domain.shared.ClassLogger;

/**
 * 약도
 */
public class RoughMap implements Serializable {
	//
	private static final long serialVersionUID = -8453085839944569912L;

	/** 지하철 출구 */
	private Subway subway;
	
	/** 경로 좌표들 */
	private List<GeoLocation> points;

	//--------------------------------------------------------------------------
	
	public Subway getSubway() {
		return subway;
	}
	
	public void setSubway(Subway subway) {
		this.subway = subway;
	}
	
	public List<GeoLocation> getPoints() {
		return points;
	}

	public void setPoints(List<GeoLocation> points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}
