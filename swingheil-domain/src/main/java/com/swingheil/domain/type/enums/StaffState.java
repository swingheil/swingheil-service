package com.swingheil.domain.type.enums;

public enum StaffState {
	
	Requested("R", "승인요청중"),
	Active("A", "정상");
	
	private String code;
	private String krName;
	
	//--------------------------------------------------------------------------
	
	private StaffState(String code, String krName) {
		//
		this.code = code;
		this.krName = krName;
	}
	
	//--------------------------------------------------------------------------
	
	public String code() {
		//
		return this.code;
	}
	
	public String krName() {
		//
		return this.krName;
	}
	
	public static StaffState findBy(String code) {
		//
		for (StaffState value : values()) {
			if (value.code().equals(code)) {
				return value;
			}
		}
		return null;
	}

}
