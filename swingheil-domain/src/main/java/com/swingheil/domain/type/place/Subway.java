package com.swingheil.domain.type.place;

import java.io.Serializable;
import java.util.List;

import com.swingheil.domain.shared.ClassLogger;

/**
 * 지하철 정보
 */
public class Subway implements Serializable {
	//
	private static final long serialVersionUID = -8453085839944569912L;

	/** 지하철명 */
	private List<Station> stations;
	
	/** 지하철라인 */
	private List<Line> lines;
	
	public List<Station> getStations() {
		return stations; 
	}

	public void setStations(List<Station> stations) {
		this.stations = stations;
	}

	public List<Line> getLines() {
		return lines;
	}

	public void setLines(List<Line> lines) {
		this.lines = lines;
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
	
	/**
	 * 지하철 노선
	 */
	public static class Line implements Serializable {
		//
		private static final long serialVersionUID = -4219877625517675569L;

		/** 식별번호 */
		private int id;
		
		/** 노선명 */
		private String name;

		//----------------------------------------------------------------------
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return ClassLogger.toString(this);
		}
	}
}
