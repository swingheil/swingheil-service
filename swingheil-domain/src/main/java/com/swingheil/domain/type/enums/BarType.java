package com.swingheil.domain.type.enums;

/**
 * 스윙바 유형
 */
public enum BarType {
	//
	BallRoom("01", "스윙바"), 
	PracticeRoom("02", "연습실");
	
	private String code;
	private String nameKor;

	//--------------------------------------------------------------------------
	
	private BarType(String code, String nameKor) {
		this.code = code;
		this.nameKor = nameKor;
	}
	
	//--------------------------------------------------------------------------
	
	public String getCode() {
		return this.code;
	}
	
	public String nameKor() {
		return this.nameKor;
	}

	public static BarType findBy(String code) {
		for (BarType value : values()) {
			if (value.getCode().equals(code)) {
				return value;
			}
		}
		return null;
	}
}
