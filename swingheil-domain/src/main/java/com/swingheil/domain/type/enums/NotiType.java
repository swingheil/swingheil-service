package com.swingheil.domain.type.enums;

public enum NotiType {
	App("01", "앱공지"), Bar("02", "바공지"), Club("03", "클럽공지"), Instructor("04",
			"강사공지"), SwingHeil("05", "스윙하일");

	private String code;
	private String nameKor;

	private NotiType(String code, String nameKor) {
		this.code = code;
		this.nameKor = nameKor;
	}

	public String getCode() {
		return code;
	}

	public String getNameKor() {
		return nameKor;
	}
	
	public static NotiType findBy(String code) {
		for (NotiType value : values()) {
			if (value.getCode().equals(code)) {
				return value;
			}
		}
		return null;
	}
}
