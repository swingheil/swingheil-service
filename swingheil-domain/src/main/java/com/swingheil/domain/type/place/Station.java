package com.swingheil.domain.type.place;

import java.io.Serializable;

import com.swingheil.domain.shared.ClassLogger;

/**
 * 지하철 정보
 */
public class Station implements Serializable {
	//
	private static final long serialVersionUID = -8453085839944569912L;

	/** 지하철명 */
	private String name;
	
	/** 출구정보 */
	private String exit;

	public Station() {
		//
	}
	
	/**
	 * 생성자
	 * @param stationName
	 * @param exitNo
	 */
	public Station(String stationName, String exitNo) {
		// 
		this.name = stationName;
		this.exit = exitNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExit() {
		return exit;
	}

	public void setExit(String exit) {
		this.exit = exit;
	}
	
	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}
