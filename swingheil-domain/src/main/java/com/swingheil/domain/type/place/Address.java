package com.swingheil.domain.type.place;

import java.io.Serializable;

import com.swingheil.domain.shared.ClassLogger;

public class Address implements Serializable {
	//
	private static final long serialVersionUID = -8187698333925461823L;
	
	/** 구 (예: 강남구, 대구 중구..) */
	private String district;
	
	/** 도로명 주소 */
	private String road;
	
	/** 지명 주소 */
	private String land;
	
	/** 약도 */
	private RoughMap map;
	
	//--------------------------------------------------------------------------
	
	public Address() {
		//
	}
	
	public Address(String district) {
		//
		this.district = district;
	}
	
	public Address(String district, String road, String land) {
		//
		this.district = district;
		this.road = road;
		this.land = land;
	}
	
	//--------------------------------------------------------------------------
	
	public String getDistrict() {
		return district;
	}
	
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getLand() {
		return land;
	}

	public void setLand(String land) {
		this.land = land;
	}

	public RoughMap getMap() {
		return map;
	}

	public void setMap(RoughMap map) {
		this.map = map;
	}

	@Override
	public String toString() {
		return ClassLogger.toString(this);
	}
}