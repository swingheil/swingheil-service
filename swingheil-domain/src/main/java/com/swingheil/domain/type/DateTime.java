package com.swingheil.domain.type;

import java.io.Serializable;

public class DateTime implements Serializable {
	//
	private static final long serialVersionUID = -6959811170394366997L;
	
	// 2014-08-15
	private String date;
	// 11:10
	private String time;
	
	//--------------------------------------------------------------------------
	
	public DateTime() {
		//
	}
	
	public DateTime(String date) {
		// 
		this.date = date;
	}
	
	//--------------------------------------------------------------------------
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}

}
