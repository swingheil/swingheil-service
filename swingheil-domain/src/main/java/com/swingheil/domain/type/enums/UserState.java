package com.swingheil.domain.type.enums;

public enum UserState {
	Requested("R", "승인대기중"),
	Normal("N", "정상");
	
	private String code;
	private String krName;
	
	private UserState(String code, String krName){
		this.code = code;
		this.krName = krName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getKrName() {
		return krName;
	}

	public void setKrName(String krName) {
		this.krName = krName;
	}
}
