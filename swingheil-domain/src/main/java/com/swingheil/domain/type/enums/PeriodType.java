package com.swingheil.domain.type.enums;

public enum PeriodType {
	//
	Day("D", "일시"),
	Term("T", "기간");
	
	private String code;
	private String nameKor;
	
	//--------------------------------------------------------------------------
	
	private PeriodType(String code, String nameKor) {
		//
		this.code = code;
		this.nameKor = nameKor;
	}
	
	//--------------------------------------------------------------------------
	
	public String code() {
		return this.code;
	}
	
	public String nameKor() {
		return this.nameKor;
	}
	
	public static PeriodType findBy(String code) {
		//
		for(PeriodType value : values()) {
			if (value.code().equals(code)) {
				return value;
			}
		}
		return null;
	}
}