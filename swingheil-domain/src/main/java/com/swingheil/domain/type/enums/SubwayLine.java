package com.swingheil.domain.type.enums;

public enum SubwayLine {
	
	One("1호선", 0),
	Two("2호선", 1),
	Three("3호선", 2),
	Four("4호선", 3),
	Five("5호선", 4),
	Six("6호선", 5),
	Seven("7호선", 6),
	Eight("8호선", 7),
	Nine("9호선", 8),
	Bundang("분당선", 9),
	Inchoen("인천선", 10),
	Sinbundang("신분당선", 11),
	Gyoungeui("경의선", 12),
	Jungang("중앙선", 13),
	Gyoungchun("경춘선", 14),
	Gonghang("공항철도", 15),
	Daegu1("대구 1호선", 16),
	Daegu2("대구 2호선", 17),
	Daegeon1("대전 1호선", 18);

	private String nameKor;
	private int lineId;
	
	private SubwayLine(String nameKor, int lineId) {
		//
		this.nameKor = nameKor;
		this.lineId = lineId;
	}

	public String getNameKor() {
		return nameKor;
	}

	public int getLineId() {
		return lineId;
	}

	public static SubwayLine findById(int lineId) {
		// 
		for (SubwayLine value : values()) {
			if (value.getLineId() == lineId) {
				return value;
			}
		}
		return null;
	}
}
