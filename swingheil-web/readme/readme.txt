1. SSH Private key 파일 위치 설정
   maven을 통한 openshift 서버로 배포를 위해 아래와 같이 세팅한다.
   settings.xml 파일의 server 요소에서 privatekey 파일위치 설정
   
	<?xml version="1.0" encoding="UTF-8"?>   
	<settings xmlns="http://maven.apache.org/settings/1.0.0"    
	          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"    
	          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">   
	  <servers>   
	    <server>
	      <id>openShift</id>
	      <privateKey>/Users/HaroldKim/.ssh/id_rsa</privateKey>
	      <passphrase></passphrase>
	    </server>  
	  </servers>   
	</settings>

    
2. WAS DataSource 설정
   이클립스 Servers > context.xml 에 아래와 같이 DataSource 설정을 한다.
  
   <Resource name="jdbc/MySQLDS"
              url="jdbc:mysql://localhost:3306/swingheildb?useUnicode=true&amp;characterEncoding=UTF-8"
              driverClassName="com.mysql.jdbc.Driver"
              username="swingheil"
              password="swingheil"
              auth="Container"
              type="javax.sql.DataSource"
              maxActive="20"
              maxIdle="5"
              maxWait="10000"
       />

3. 서버 구동 시 mysql 드라이버를 못찾는 에러가 나는 경우,
   WAS 인스턴스 루트디렉토리의 lib 아래에 mysql 드라이버 jar 파일을 복사한다.

   
   