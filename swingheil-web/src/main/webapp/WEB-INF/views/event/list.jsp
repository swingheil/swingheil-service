<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<title>이벤트 목록</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #event").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>

	<div class="container">
		<div class="row">
			<!-- ★★★ Aside Menu -->
			<%@ include file="/WEB-INF/views/common/menu.jspf"%>

			<!-- ★★★ Contents -->
			<div class="col-sm-9 col-lg-9">
				<div class="page-header2">
					<h3>이벤트 목록</h3>
				</div>

				<!-- ★★★ Tab Panel -->
				<div id="communityList" class="tab-content">
					<!-- ★★★ 전체클럽 -->
					<div class="tab-pane fade active in" id="all">
						<div>
							<a class="btn btn-success" href="${ctx}/web/event/register">이벤트 등록</a>
						</div>
						<br/>
						<!-- ★★★ 회원목록 -->
						<div class="table-responsive">
							<table id="staffList" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<td></td>
										<td>일자</td>
										<td>제목</td>
										<td>주최</td>
										<td>장소</td>
										<td>숨김</td>
										</tr>
								</thead>
								<tbody>
									<c:forEach var="event" items="${events}">
										<tr>
											<td><img class="img-thumbnail" src="${ctx}/web/event/${event.id}/thumbnail" style="width: 100px; height: 100px;"/></td>
											<td>${event.period.from.date}</td>
											<td><c:if test="${event.notice == true}">[공지] </c:if><a href="${ctx}/web/event/${event.id}">${event.title}</a></td>
											<td>${event.sponsor.nameKor}</td>
											<td>${event.place.district}</td>
											<td>
											<c:choose>
												<c:when test="${event.visible == true}">
													보임
												</c:when>
												<c:otherwise>
													숨김
												</c:otherwise>
											</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<%@ include file="/WEB-INF/views/common/footer.jspf"%>
	</div>
</body>
</html>