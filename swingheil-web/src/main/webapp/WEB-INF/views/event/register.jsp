<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>이벤트 등록</title>
<script type="text/javascript" src="http://openapi.map.naver.com/openapi/naverMap.naver?ver=2.0&key=ec01f1aac7a9dc2623526edaf4892172"></script>
<<script type="text/javascript" src="${ctx}/resources/event/register.js"></script>
<script type="text/javascript">
$(function() {
	$("#cat-navi #event").addClass("active");
});
</script>
</head>
<body>
<c:choose>
	<c:when test="${processType == 'modify'}">
		<c:set var="korTitle" value="이벤트 수정" />
		<c:set var="buttonName" value="저장" />
		<c:set var="actionUrl" value="${ctx}/web/event/${event.id}/modify" />
	</c:when>
	<c:otherwise>
		<c:set var="korTitle" value="이벤트 등록" />
		<c:set var="buttonName" value="등록" />
		<c:set var="actionUrl" value="${ctx}/web/event/register" />
	</c:otherwise>
</c:choose>

<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
	<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>${korTitle}</h3>
			</div>
			<div class="well bs-component">
				<form id="form" class="form-horizontal" action="${actionUrl}" method="post" enctype="multipart/form-data">
					<fieldset>
						<c:choose>
							<c:when test="${processType == 'modify' }">
								<div class="form-group">
									<label class="col-lg-2 control-label">카테고리</label>
									<div class="col-md-4">
										<select name="category" class="form-control" id="category" readonly="readonly" >
											<option value="${event.category.code}">${event.category.name}</option>
										</select>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div class="form-group">
									<label class="col-lg-2 control-label">카테고리</label>
									<div class="col-md-4">
										<select name="category" class="form-control" id="category" <c:if test="${processType == 'modify'}"> readonly="readonly" </c:if> >
											<c:if test="${superAdminYn == 'Y'}">
												<option value="01">스윙하일</option>
											</c:if>
											<option value="02">스윙바</option>
											<option value="03">동호회</option>
										</select>
									</div>
								</div>
							</c:otherwise>
						</c:choose>

						<div class="form-group" id="sponsorArea" style="display: none;">
							<label class="col-lg-2 control-label">주최</label>
							<div class="col-lg-10">
								<select name="sponsor" id="sponsor" class="form-control">
								<c:if test="${event.sponsor != null}">
									<option value="${event.sponsor.id}">${event.sponsor.nameKor}</option>
								</c:if>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-2 control-label">기간</label>
							<div class="col-xs-3">
								<input type="text" value="${event.period.from.date}" class="form-control" name="fromDate" placeholder="yyyy-mm-dd">
							</div>
							<div class="col-xs-2">
								<input type="text" value="${event.period.from.time}" class="form-control" name="fromTime" placeholder="hh:mm">
							</div>
							<div class="col-xs-3">
								<input type="text" value="${event.period.to.date}" class="form-control" name="toDate" placeholder="yyyy-mm-dd">
							</div>
							<div class="col-xs-2">
								<input type="text" value="${event.period.to.time}" class="form-control" name="toTime" placeholder="hh:mm">
							</div>
						</div>
						<div class="form-group">
							<label for="eventName" class="col-lg-2 control-label">이벤트명</label>
							<div class="col-lg-10">
								<input type="text" value="${event.title}" class="form-control" name="eventName"
									id="eventName" placeholder="이벤트명">
							</div>
						</div>
						<div class="form-group">
							<label for="summary" class="col-lg-2 control-label">요약설명</label>
							<div class="col-lg-10">
								<input type="text" value="${event.summary}" class="form-control" name="summary"
									id="summary" placeholder="이벤트 요약설명">
							</div>
						</div>
						<div class="form-group">
							<label for="detail" class="col-lg-2 control-label">상세설명</label>
							<div class="col-lg-10">
								<textarea class="form-control" rows="3" name="detail"
									id="detail">${event.detail}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="place" class="col-lg-2 control-label">장소</label>
							<div class="col-lg-10">
								<input type="text" value="${event.place.district}" class="form-control" name="district" id="district" placeholder="지역(ex. 강남구)">
								<input type="text" value="${event.place.road}" class="form-control" name="roadAddress" id="roadAddress" placeholder="도로명 주소">
								<input type="text" value="${event.place.land}" class="form-control" name="landAddress" id="landAddress" placeholder="지번 주소">
							</div>
						</div>
						<div class="form-group">
							<label for="visible" class="col-lg-2 control-label">이벤트 보이기</label>
							<div class="col-lg-10">
								<div class="checkbox">
									<input type="checkbox" name="visible" 
										<c:choose>
											<c:when test="${event.visible == true}"> checked="checked" </c:when>
											<c:when test="${event.id == null}">checked="chekced" </c:when>
										</c:choose>
									>
									<input type="hidden" name="_visible">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="visible" class="col-lg-2 control-label">공지사항</label>
							<div class="col-lg-10">
								<div class="checkbox">
									<input type="checkbox" name="notice" <c:if test="${event.notice == true}"> checked="checked" </c:if>>
								</div>
							</div>
						</div>
	
						<div class="form-group">
							<label for="largeImage" class="col-lg-2 control-label">상세이미지</label>
							<div class="col-lg-10">
								<c:if test="${processType == 'modify' }">
									<c:if test="${event.largeImageId != null}">
										<img class="img-thumbnail" src="${ctx}/web/event/${event.id}/poster" />
									</c:if>
								</c:if>
								<input type="file" class="form-control" name="largeImage" id="largeImage" />
							</div>
						</div>
						<div class="form-group">
							<label for="detail" class="col-lg-2 control-label">지도</label>
							<div class="col-lg-10">
								<div class="input-group">
									<input type="text" class="form-control" id="initAddress" placeholder="지번 주소를 입력하세요" />
									<span class="input-group-btn">
										<button id="btnGetGeo" class="btn btn-parimary" type="button">검색</button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="detail" class="col-lg-2 control-label"></label>
							<div class="col-lg-10">
								<div id="map"/></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button onclick="location.href='${ctx}/web/event/list'; return false;" class="btn btn-default">취소</button>
								<button type="submit" class="btn btn-primary">${buttonName}</button>
								<c:if test="${event.id != null}">
									<button type="button" id="btnDelete" class="btn btn-danger col-lg-offset-8">삭제</button>
								</c:if>
							</div>
						</div>
	
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
<script type="text/javascript">

	$("#category").change(function(){
		
		$("#sponsor").html("").val("");
		var category = $(this).val();
		if (category == "01") {
			$("#sponsorArea").hide();
		} else {
			$("#sponsorArea").show();
			var url;
			if (category == "02") { 
				url = ctx + "/rest/bar/getManagedBarList?userId=${loginUser.id}";
			} else {
				url = ctx + "/rest/club/getManagedClubList?userId=${loginUser.id}";
			}
			
			$.getJSON(url, function(results) {
				var optionHtml = '';
				if (results && results.length > 0) {
					for (var i = 0, length = results.length; i < length; i++) {
						if (category == "03") {
							optionHtml += '<option value="'+results[i].id+'" data-bar-id="'+results[i].bar.id+'">'+results[i].nameKor+'</option>';
						} else {
							optionHtml += '<option value="'+results[i].id+'">'+results[i].nameKor+'</option>';
						}
					}
				}
				$("#sponsor").append(optionHtml).change();
			});
		}
	});
	
	$("#sponsor").change(function(){
		var categoryType = $("#category").val();
		var sponsorId = $("#sponsor").val();
		var barId = null;
		
		$("#district").val("");
		$("#roadAddress").val("");
		$("#landAddress").val("");
		
		if (categoryType === "02") { // 스윙바(02)
			barId = sponsorId;
		} else if(categoryType === "03") { // 동호회(03)
			barId = $("#sponsor").find("option:selected").data("barId");
		}
		if (barId != null) {
			var url = ctx + "/rest/bar/getBar?barId="+barId+"&userId=${loginUser.id}";
			$.getJSON(url, function(result){
				if(result && result.address) {
					$("#district").val(result.address.district);
					$("#roadAddress").val(result.address.road);
					$("#landAddress").val(result.address.land);
				}
			});
		}
	});
	
	$("#form").submit(function() {
		if($("#category").val() != "01") {
			if($("#sponsor").val() == null) {
				alert("주최를 입력해주세요");
				return false;
			} else {
				return true;
			}
		}
	});
	
	$("#initAddress").keyup(function(event) {
		if(event.keycode == 13) {
			console.log("aaaaaa");
			alert('aaaa');
		}
	});
	
	$("#btnDelete").click(function(evt) {
		evt.preventDefault(); // form submit 방지
		
		if(confirm("이벤트를 삭제하시겠습니까?")) {
			
			var url = ctx + "/web/event/${event.id}/delete";
			
			window.location.href = url;
		}
	});
	
	var categoryVal = $("#category").val();
	
	if(categoryVal == null) {
		$("#category").val("02").trigger("change");	
	} else {
		$("#category").val(categoryVal).trigger("change");
	}
	
</script>

</body>
</html>