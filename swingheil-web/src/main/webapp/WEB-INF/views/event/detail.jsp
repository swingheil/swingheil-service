<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>이벤트 상세</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #event").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>이벤트 상세</h3>
			</div>
			
			<div class="well bs-component">
				<div class="form-horizontal">
					<input type="hidden" name="id" value="${bar.id}" />
					<fieldset>
						<div class="form-group">
							<label class="col-lg-2 control-label">분류</label>
							<div class="col-lg-10">
								<c:choose>
									<c:when test="${event.category.code == '01'}">
										<input type="text" class="form-control" disabled="disabled" value="스윙하일">
									</c:when>
									<c:when test="${event.category.code == '02'}">
										<input type="text" class="form-control" disabled="disabled" value="스윙바">
									</c:when>
									<c:when test="${event.category.code == '03'}">
										<input type="text" class="form-control" disabled="disabled" value="동호회">
									</c:when>
								</c:choose>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">기간(from~to)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${event.period.from.date} <c:if test="${event.period.to.date != null}"> ~ ${event.period.to.date}</c:if>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">이벤트명</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${event.title}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">요약설명</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${event.summary}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">장소</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${event.place.district}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">주최자</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">상세설명</label>
							<div class="col-lg-10">
								<textarea class="form-control" disabled="disabled">${event.detail}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label for="visible" class="col-lg-2 control-label">이벤트 보이기</label>
							<div class="col-lg-10">
								<div class="checkbox">
									<input type="checkbox" disabled="disabled" <c:if test="${event.visible == true}"> checked="checked" </c:if> >
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="visible" class="col-lg-2 control-label">공지사항</label>
							<div class="col-lg-10">
								<div class="checkbox">
									<input type="checkbox" disabled="disabled" <c:if test="${event.notice == true}"> checked="checked" </c:if> >
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">큰이미지</label>
							<div class="col-lg-10">
								<c:choose>
								<c:when test="${event.largeImageId != null}">
									<img class="img-thumbnail" src="${ctx}/web/event/${event.id}/poster" />
								</c:when>
								<c:otherwise>
									등록된 이미지가 없습니다.
								</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button onclick="location.href='${ctx}/web/event/${event.id}/modify'; return false;"  class="btn btn-info">수정</button>
								<button onclick="location.href='${ctx}/web/event/list'; return false;" class="btn btn-default">목록</button>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
</body>
</html>