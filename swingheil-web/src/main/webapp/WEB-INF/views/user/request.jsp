<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/views/common/common.jsp"%>
    <style type="text/css">
        body {
            padding-top: 100px;
            padding-bottom: 40px;
            background-color: #ecf0f1;
        }
        .login-header {
            max-width: 400px;
            padding: 15px 29px 25px;
            margin: 0 auto;
            background-color: #2c3e50;
            color: white;
            text-align: center;
            -webkit-border-radius: 15px 15px 0px 0px;
            -moz-border-radius: 15px 15px 0px 0px;
            border-radius: 15px 15px 0px 0px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .login-footer {
            max-width: 400px;
            margin: 0 auto 20px;
            padding-left: 10px;
        }
        .form-signin {
            max-width: 400px;
            padding: 29px;
            margin: 0 auto 20px;
            background-color: #fff;
            -webkit-border-radius: 0px 0px 15px 15px;
            -moz-border-radius: 0px 0px 15px 15px;
            border-radius: 0px 0px 15px 15px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 15px;
        }
        .form-signin input[type="text"],
        .form-signin input[type="password"] {
            font-size: 16px;
            height: auto;
            margin-bottom: 15px;
            padding: 7px 9px;
        }
        .form-btn {
            text-align: center;
            padding-top: 20px;
        }

    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<script type="text/javascript">
$(function(){
	$("#btnConfirm").click(function(){
		
	});
	
	function registerNickName(){
		var callback = function(response) {
			if(response.success == true) {
				
			}
		}
		
		var params = {
			"nickname" : $("#nickName").val(),
			"deviceUid" : null,
			"modelId" : null,
			"osVersion" : null,
			"registrationId" : null
		};
		
		sendRequest (ctx + "/rest/user/register", params, callback);
	}
	
	function registerEmail(){
		
	}
	
	function registerAsStaff(){
		
	}
});
</script>
<body>
<div class="container">

    <!-- header -->
    <div class="login-header">
        <h2 class="form-signin-heading">스윙하일 관리자 가입</h2>
    </div>

    <!-- form -->
    <form class="form-signin" action="${ctx}/login" method="post">
        <input type="text" class="form-control" id="inputEmail" placeholder="이메일" required>
        <input type="password" class="form-control" id="inputPassword" placeholder="비밀번호" required>
        <input type="password" class="form-control" id="inputPasswordRepeate" placeholder="비밀번호 확인" required>
        <input type="password" class="form-control" id="nickName" placeholder="닉네임" required>
        <input type="password" class="form-control" id="telNum" placeholder="전화번호" required>
        
        <select name="category" class="form-control" id="category" <c:if test="${processType == 'modify'}"> readonly="readonly" </c:if> >
			<option value="02">스윙바</option>
			<option value="03">동호회</option>
		</select>
        
        <div class="row form-btn">
            <button class="btn btn-large btn-warning" type="button" id="btnConfirm">요청</button>
        </div>
    </form>

    <!-- footer -->
    <div class="login-footer">
        <p>© SwingHeil 2014.</p>
    </div>
</div>
</body>
</html>