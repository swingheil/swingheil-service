<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>동호회 상세정보</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #club").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>동호회 상세</h3>
			</div>
			<div class="well bs-component">
				<div class="form-horizontal">
					<input type="hidden" name="id" value="${bar.id}" />
					<fieldset>
						<div class="form-group">
							<label class="col-lg-2 control-label">스윙바</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${club.bar.nameKor}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(한글)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${club.nameKor}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(영문)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${club.nameEng}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">홈페이지</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${club.homePage}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">썸네일</label>
							<div class="col-lg-10">
								<img class="img-thumbnail" src="${ctx}/web/club/${club.id}/thumbnail" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">배너</label>
							<div class="col-lg-10">
								<img class="img-thumbnail" src="${ctx}/web/club/${club.id}/banner" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button onclick="location.href='${ctx}/web/club/${club.id}/modify'; return false;" class="btn btn-info">수정</button>
								<button onclick="location.href='${ctx}/web/club/'; return false;" class="btn btn-default">목록</button>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
</body>
</html>