<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>동호회 상세</title>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>동호회 수정</h3>
			</div>
			
			<div class="well bs-component">
				<form class="form-horizontal" action="${ctx}/web/club/${club.id}/modify"
					method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="${club.id}" />
					<fieldset>
						<c:if test="${club.thumbnailImageId != null}">
							<img class="img-thumbnail" src="${ctx}/web/club/${club.id}/banner"/><br/><br/>
						</c:if>
						<div class="form-group">
							<label class="col-lg-2 control-label">스윙바</label>
							<div class="col-lg-10">
								<select name="barId" class="form-control">
									<c:forEach var="bar" items="${bars}">
										<option value="${bar.id}" <c:if test="${bar.id == club.bar.id}">selected</c:if>>${bar.nameKor}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(한글)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameKor" value="${club.nameKor}" placeholder="명칭(한글)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(영문)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameEng" value="${club.nameEng}" placeholder="명칭(영문)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">홈페이지</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="homePage" value="${club.homePage}" placeholder="홈페이지 URL">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">썸네일</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="thumbnailImage">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">배너</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="bannerImage">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary">저장</button>
								<button onclick="location.href='${ctx}/web/bar/'; return false;" class="btn btn-default">취소</button>
							</div>
						</div>
	
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
</body>
</html>