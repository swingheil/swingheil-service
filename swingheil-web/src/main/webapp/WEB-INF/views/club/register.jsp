<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>동호회 등록</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-6">
			<div class="well bs-component">
				<form class="form-horizontal" action="${ctx}/web/club/register"
					method="post" enctype="multipart/form-data">
					<fieldset>
						<legend>동호회 상세</legend>
						<div class="form-group">
							<label class="col-lg-2 control-label">스윙바</label>
							<div class="col-lg-10">
								<select name="barId" class="form-control">
									<c:forEach var="bar" items="${bars}">
										<option value="${bar.id}">${bar.nameKor}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(한글)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameKor" placeholder="명칭(한글)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(영문)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameEng" placeholder="명칭(영문)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">홈페이지</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="homePage" placeholder="홈페이지 URL">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">썸네일</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="thumbnailImage">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">배너</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="bannerImage">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button onclick="location.href='${ctx}/web/club/list'; return false;" class="btn btn-default">취소</button>
								<button type="submit" class="btn btn-primary">동호회 등록</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</body>
</html>