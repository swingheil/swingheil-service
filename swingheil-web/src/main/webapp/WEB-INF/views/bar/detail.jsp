<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>스윙바 상세</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #bar").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>스윙바 상세</h3>
			</div>
			
			<div class="well bs-component">
				<div class="form-horizontal">
					<input type="hidden" name="id" value="${bar.id}" />
					<fieldset>
						<div class="form-group">
							<label class="col-lg-2 control-label">구분</label>
							<div class="col-lg-10">
								<c:choose>
									<c:when test="${bar.type.code == '01'}">
										<input type="text" class="form-control" disabled="disabled" value="스윙바">
									</c:when>
									<c:when test="${bar.type.code == '02'}">
										<input type="text" class="form-control" disabled="disabled" value="연습실">
									</c:when>
								</c:choose>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">스윙바명</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${bar.nameKor}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">홈페이지</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${bar.homePage}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">주소</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" disabled="disabled" value="${bar.address.district}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">썸네일</label>
							<div class="col-lg-10">
								<img class="img-thumbnail" src="${ctx}/web/bar/${bar.id}/thumbnail" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">배너</label>
							<div class="col-lg-10">
								<img class="img-thumbnail" src="${ctx}/web/bar/${bar.id}/banner" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button onclick="location.href='${ctx}/web/bar/${bar.id}/modify'; return false;" class="btn btn-info">수정</button>
								<button onclick="location.href='${ctx}/web/bar/'; return false;" class="btn btn-default">목록</button>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
</body>
</html>