<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>스윙바 등록</title>
</head>
<body>
<div class="row">
	<div class="col-lg-6">
		<div class="well bs-component">
			<form id="registerForm" class="form-horizontal" action="${ctx}/web/bar/register"
				method="post" enctype="multipart/form-data">
				<fieldset>
					<legend>스윙바 상세</legend>
					<div class="form-group">
						<label class="col-lg-2 control-label">구분</label>
						<div class="col-lg-10">
							<select name="barType" class="form-control">
								<option value="01">스윙바</option>
								<option value="02">연습실</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">명칭(한글)</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="nameKor" placeholder="명칭(한글)">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">명칭(영문)</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="nameEng" placeholder="명칭(영문)">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">홈페이지</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="homePage" placeholder="홈페이지 URL">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">주소</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" name="district" id="district" placeholder="지역(ex. 강남구)">
							<input type="text" class="form-control" name="roadAddress" placeholder="도로명 주소">
							<input type="text" class="form-control" name="landAddress" placeholder="지번 주소">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">지하철</label>
						<div class="col-lg-10">
							<div class="col-xs-4">
								<select name="subwayLines" class="form-control" multiple>
									<option value="">::지하철선택::</option>
									<option value="0">1호선</option>
									<option value="1">2호선</option>
									<option value="2">3호선</option>
									<option value="3">4호선</option>
									<option value="4">5호선</option>
									<option value="5">6호선</option>
									<option value="6">7호선</option>
									<option value="7">8호선</option>
									<option value="8">9호선</option>
									<option value="9">분당선</option>
									<option value="10">인천선</option>
									<option value="11">신분당선</option>
									<option value="12">경의선</option>
									<option value="13">중앙선</option>
									<option value="14">경춘선</option>
									<option value="15">공항철도</option>
									<option value="16">대구 1호선</option>
									<option value="17">대구 2호선</option>
									<option value="18">대전 1호선</option>
								</select>
							</div>
							<div class="col-xs-4">
								<input type="text" class="form-control" name="stationName" placeholder="지하철명">
							</div>
							<div class="col-xs-3">
								<input type="text" class="form-control" name="exitNo" placeholder="출구">
							</div>
						</div>
					</div>
					<div id="gpsPointsArea">
						<div class="form-group gps-point">
							<label class="col-lg-2 control-label">찾아가는길<br/><button id="addPoint" onclick="return false;">경로추가</button></label>
							<div class="col-lg-10">
								<div class="col-xs-3">
									<input type="text" class="form-control" name="gpsPoints[0].latitude" placeholder="경도">
								</div>
								<div class="col-xs-3">
									<input type="text" class="form-control" name="gpsPoints[0].longitude" placeholder="위도">
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control" name="gpsPoints[0].description" placeholder="명칭">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">썸네일</label>
						<div class="col-lg-10">
							<input type="file" class="form-control" name="thumbnailImage">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">배너</label>
						<div class="col-lg-10">
							<input type="file" class="form-control" name="bannerImage">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-10 col-lg-offset-2">
							<button onclick="location.href='${ctx}/web/bar/list'; return false;" class="btn btn-default">취소</button>
							<button type="submit" class="btn btn-primary">스윙바 등록</button>
						</div>
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>
<script>

	$("#registerForm").submit(function(event){
		//
		var valid = true;
		$("input[type=text]").each(function(){
			if ($(this).val() == '') {
				$(this).focus();
				valid = false;
				return false;
			}
		});

		if (valid == false) 
			return false;
	});

	
	$("#addPoint").click(function(){
		
		var nextIndex = $("div.gps-point").length;
		var pointHtml = '';
		pointHtml += '<div class="form-group gps-point">';
		pointHtml += '<label class="col-lg-2 control-label">GPS좌표'+(nextIndex+1)+'</button></label>';
		pointHtml += '<div class="col-lg-10">';
		pointHtml += '<div class="col-xs-3">';
		pointHtml += '<input type="text" class="form-control" name="gpsPoints['+nextIndex+'].latitude" placeholder="경도">';
		pointHtml += '</div>';
		pointHtml += '<div class="col-xs-3">';
		pointHtml += '<input type="text" class="form-control" name="gpsPoints['+nextIndex+'].longitude" placeholder="위도">';
		pointHtml += '</div>';
		pointHtml += '<div class="col-xs-4">';
		pointHtml += '<input type="text" class="form-control" name="gpsPoints['+nextIndex+'].description" placeholder="명칭">';
		pointHtml += '</div>';
		pointHtml += '<div class="col-xs-2">';
		pointHtml += '<button name="deletePoint" class="form-control btn btn-warning" onclick="return false;">X</button>';
		pointHtml += '</div>';
		pointHtml += '</div>';
		pointHtml += '</div>';
		
		$("#gpsPointsArea").append(pointHtml);
		$("#gpsPointsArea").find("button[name=deletePoint]").hide().filter(":last").show();
		return false;
	});
	
	$("#gpsPointsArea").on("click", "button[name=deletePoint]", function(){
		$(this).parents("div.gps-point:first").remove();
		$("#gpsPointsArea").find("button[name=deletePoint]").hide().filter(":last").show();
		return false;
	});


</script>
</body>
</html>