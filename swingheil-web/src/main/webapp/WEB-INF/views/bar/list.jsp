<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<title>스윙바 목록</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #bar").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>

	<div class="container">
		<div class="row">
			<!-- ★★★ Aside Menu -->
			<%@ include file="/WEB-INF/views/common/menu.jspf"%>

			<!-- ★★★ Contents -->
			<div class="col-sm-9 col-lg-9">
				<div class="page-header2">
					<h3>스윙바 목록</h3>
				</div>

				<!-- ★★★ Tab Panel -->
				<div id="communityList" class="tab-content">
					<!-- ★★★ 전체클럽 -->
					<div class="tab-pane fade active in" id="all">
						<br/>
						<!-- ★★★ 회원목록 -->
						<div class="table-responsive">
							<table id="staffList" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th class="text-center"></th>
										<th class="text-center">스윙바명</th>
										<th class="text-center">장소</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="bar" items="${bars}">
										<tr>
										<td><img class="img-thumbnail" src="${ctx}/web/bar/${bar.id}/thumbnail" style="width: 100px; height: 100px;"/></td>
										<td><a href="${ctx}/web/bar/${bar.id}">${bar.nameKor}</a></td>
										<td>${bar.address.district}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<%@ include file="/WEB-INF/views/common/footer.jspf"%>
	</div>
</body>
</html>