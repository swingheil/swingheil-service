<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>스윙바 수정</title>
<script type="text/javascript">
$(function() {
	$("#cat-navi #bar").addClass("active");
});
</script>
</head>
<body>
<%@ include file="/WEB-INF/views/common/header.jspf"%>
<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>
		<div class="col-lg-9">
			<div class="page-header2">
				<h3>스윙바 수정</h3>
			</div>
			
			<div class="well bs-component">
				<form class="form-horizontal" action="${ctx}/web/bar/${bar.id}/modify"
					method="post" enctype="multipart/form-data">
					<input type="hidden" name="id" value="${bar.id}" />
					<fieldset>
						<c:if test="${bar.thumbnailImageId != null}">
								<img class="img-thumbnail" src="${ctx}/web/bar/${bar.id}/banner"/><br/><br/>
							</c:if>
						<div class="form-group">
							<label class="col-lg-2 control-label">구분</label>
							<div class="col-lg-10">
								<select name="barType" class="form-control">
									<option value="01" <c:if test="${bar.type.code == '01' }">selected</c:if>>스윙바</option>
									<option value="02" <c:if test="${bar.type.code == '02' }">selected</c:if>>연습실</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(한글)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameKor" value="${bar.nameKor}" placeholder="명칭(한글)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">명칭(영문)</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="nameEng" value="${bar.nameEng}" placeholder="명칭(영문)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">홈페이지</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="homePage" value="${bar.homePage}" placeholder="홈페이지 URL">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">주소</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="district" id="district" value="${bar.address.district}" placeholder="지역(ex. 강남구)">
								<input type="text" class="form-control" name="roadAddress" value="${bar.address.road}" placeholder="도로명 주소">
								<input type="text" class="form-control" name="landAddress" value="${bar.address.land}" placeholder="지번 주소">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">썸네일</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="thumbnailImage">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">배너</label>
							<div class="col-lg-10">
								<input type="file" class="form-control" name="bannerImage">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary">저장</button>
								<button onclick="location.href='${ctx}/web/bar/'; return false;" class="btn btn-default">취소</button>
							</div>
						</div>
	
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
</div>
</body>
</html>