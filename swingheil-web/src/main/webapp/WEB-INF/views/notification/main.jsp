<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request" />
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="/WEB-INF/views/common/common.jsp" %>
<title>Push 메시지 알림</title>
</head>
<body>

<!-- Main Navigation ========================================================================================== -->
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./index.html">SwingHeil</a>
        </div>
    </div>
</div>

<!-- Container ======================================================================================= -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">

            <div class="page-header">
                <h2 id="container">메시지 전송</h2>
            </div>

            <div class="well">
                <form class="form-horizontal" action="${ctx}/notification/push" method="post">
                    <fieldset>
                        <div class="form-group">
                            <label for="textArea" class="col-lg-2 control-label">전송할 메시지를 입력하세요.</label>

                            <div class="col-lg-10">
                                <textarea class="form-control" rows="3" name="message" id="textArea"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-lg-10 col-lg-offset-2">
                                <button type="submit" class="btn btn-primary">확인</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

    </div>

</div>

<script src="${ctx}/resources/common/js/jquery-2.1.0.js"></script>
<script src="${ctx}/resources/common/js/bootstrap.min.js"></script>
<script src="${ctx}/resources/common/js/bootswatch.js"></script>
</body>
</html>