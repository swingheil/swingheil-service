<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/WEB-INF/views/common/common.jsp"%>
<title>Insert title here</title>
<script type="text/javascript" src="${ctx}/resources/staff/js/staff.js"></script>
<script type="text/javascript" src="${ctx}/resources/staff/js/modal.js"></script>
<script type="text/javascript">
$(function() {
	$("#cat-navi #staff").addClass("active");
});
</script>
</head>
<body>
	
<%@ include file="/WEB-INF/views/common/header.jspf"%>

<div class="container">
	<div class="row">
		<!-- ★★★ Aside Menu -->
		<%@ include file="/WEB-INF/views/common/menu.jspf"%>

		<!-- ★★★ Contents -->
		<div class="col-sm-9 col-lg-9">
			<div class="page-header2">
				<h3>운영자 관리</h3>
			</div>

			<!-- ★★★ Tab Panel -->
			<div id="communityList" class="tab-content">
				<!-- ★★★ 전체클럽 -->
				<div class="tab-pane fade active in" id="all">
					<!-- ★★★ 검색조건 -->
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<select id="searchOption" class="form-control">
										<option value="bar">스윙바</option>
										<option value="club">동호회</option>
									</select>
								</div>
								<div class="col-md-6">
									<select id="searchResult" class="form-control col-md-6">
									</select>
								</div>
							</div>
							<form class="form-search">
								<!-- Single button -->
								<div class="input-group">
									<div class="controls">
									</div>
								</div>
							</form>
						</div>
					</div>
					
					<div>
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addStaff">운영자 추가</button>
					</div>
					
					<br/>

					<!-- ★★★ 회원목록 -->
					<div class="table-responsive">
						<table id="staffList" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th class="text-center"></th>
									<th class="text-center">닉네임</th>
									<th class="text-center">이메일</th>
									<th class="text-center">신청상태</th>
								</tr>
							</thead>
							<tbody>
								<tr>
								</tr>
							</tbody>
						</table>
					</div>
					
					<div id="staffMenu" class="row">
						<div class="col-md-4">
						<select id="command" class="form-control">
							<option value="delegateAdmin">관리자위임</option>
							<option value="withdrawAsStaff">직위해제</option>
							<option value="activateAsStaff">운영자승인</option>
						</select>
						</div>
						<div class="col-md-4">
							<button type="button" class="btn btn-parimary" id="btnCommandConfirm">실행</button>
						</div>
					</div>

					<!-- ★★★ Pagination -->
					<!-- 
					<div class="text-center">
						<ul class="pagination">
							<li class="disabled"><a href="#">«</a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">»</a></li>
						</ul>
					</div> -->
				</div>
			</div>

		</div>
	</div>
	
	<div id="addStaff" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
		      		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		      		<h4 class="modal-title">사용자 조회</h4>
		    	</div>
	    		<div class="modal-body">
	    			<div class="row">
	    				<div class="col-md-4">
	    					<select id="userSearchOption" class="form-control">
								<option value="nickname">닉네임</option>
								<option value="tel">전화번호</option>
								<option value="email">이메일</option>
							</select>
	    				</div>
	    				<div class="col-md-6">
	    					<input id="keyword" type="text" class="form-control">
	    				</div>
	    				<div class="col-md-1">
	    					<button id="btnUserSearch" type="button" class="btn btn-default">검색</button>
	    				</div>
	    			</div>
	    			<br/>
	    			<table id="userList" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th class="text-center">닉네임</th>
								<th class="text-center">이메일</th>
							</tr>
						</thead>
						<tbody>
							<tr></tr>
						</tbody>
					</table>
		    	</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
		    	</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<%@ include file="/WEB-INF/views/common/footer.jspf"%>
	
</div>
</body>
</html>