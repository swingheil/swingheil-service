$(function() {
	$("#searchOption").change(function() {

		var callback = function(response) {
			
			if(response.success == true) {
				appendList(response.message);
			}
		}
		
		var params = {
			"type" : $(this).val()
		};
		
		sendRequest (ctx + "/web/staff/list", params, callback);
	});
	
	$("#searchResult").change(function() {
		
		var callback = function(response) {
			if(response.success == true) {
				
				appendStaffs(response.message);
			} else {
				$("#staffList > tbody").html("");
			}
		}
		
		var params = {
			"type" : $("#searchOption").val(),
			"id" : $(this).val()
		};
		
		sendRequest(ctx + "/web/staff/staffList", params, callback);
	});
	
	$("#btnCommandConfirm").click(function() {
		var selection = $("#command").val();
		var userId = $("#staffList input:checked").parent().parent().attr("id");
		
		console.log(selection);
		
		if(selection == 'delegateAdmin') {
			delegateAsAdmin(userId);
		} else if(selection == 'withdrawAsStaff') {
			withdrawAsStaff(userId);
		} else if(selection == 'activateAsStaff') {
			activiateAsStaff(userId);
		}
	});
	
	function appendList(results) {
		
		$("#searchResult").html('');
		
		for(var i in results) {

			result = results[i];
			
			$('#searchResult').append('<option value="' + result.id +'">' + result.nameKor + '</option>');
		}
		
		$("#searchResult").trigger("change");
	}
	
	function appendStaffs(staffList) {
		
		$("#staffList > tbody").html("");
		
		for(var i in staffList) {
			
			staff = staffList[i];
			
			var roleUser = staff.roleUser;
			
			var rowHtml = '<tr id="' + staff.id + '">'
				+ '<td><input type="checkbox"></td>'
				+ '<td>' + staff.nickname + '</td>';
			
			if(roleUser.email == null) {
				rowHtml = rowHtml + '<td></td>';
			} else {
				rowHtml = rowHtml + '<td>' + roleUser.email + '</td>';
			}
			
			if(staff.state == "Requested") {
				rowHtml = rowHtml + '<td>승인대기</td>';
			} else if(staff.state == "Active") {
				rowHtml = rowHtml + '<td>승인</td>';
			} else {
				rowHtml = rowHtml + '<td>알수없음</td>';
			}
			
			rowHtml = rowHtml + '</tr>';
			
			$("#staffList > tbody").append(rowHtml);
		}
		
		$("#staffList tr input:checkbox").click(function() {
			
			$("#staffList tr td input:checkbox").prop("checked", false);
			
			var checked = $(this).prop("checked");
			
			$(this).prop("checked", !checked);
			
			$("#staffMenu").show();
		});
	}
	
	function joinAsStaff(userId) {
		
		var callback = function(response) {
			if(response.success == true) {
				$("#addStaff").modal("hide");
				$("#searchResult").trigger("change");
			}
		}
		
		var params = {
			"type" : $("#searchOption").val(),
			"id" : $("#searchResult").val(),
			"userId" : userId
		};
		
		sendRequest(ctx + "/web/staff/joinAsStaff", params, callback);
	}
	
	function withdrawAsStaff(userId) {
		 
		var callback = function(response) {
			if(response.success == true) {
				$("#searchResult").trigger("change");
			}
		}
		
		var params = {
			"type" : $("#searchOption").val(),
			"id" : $("#searchResult").val(),
			"userId" : userId
		};
		
		sendRequest(ctx + "/web/staff/withdrawAsStaff", params, callback);
	}
	
	function activiateAsStaff(userId) {
		
		var callback = function(response) {
			if(response.success == true) {
				alert("승인되었습니다");
				$("#searchResult").trigger("change");
			}
		}
		
		var params = {
			"type" : $("#searchOption").val(),
			"id" : $("#searchResult").val(),
			"userId" : userId
		};
		
		sendRequest(ctx + "/web/staff/activateAsStaff", params, callback);
	}
	
	function delegateAsAdmin(userId) {
		
		var callback = function(response) {
			if(response.success == true) {
				alert("관리자가 위임되었습니다");
			}
		}
		
		var params = {
			"type" : $("#searchOption").val(),
			"id" : $("#searchResult").val(),
			"userId" : userId
		};
		
		sendRequest(ctx + "/web/staff/delegateAsAdmin", params, callback);
	}
	
	// modal components
	$("#keyword").keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $("#btnUserSearch").trigger("click");
	    }
	});
	
	$("#btnUserSearch").click(function() {
		
		var callback = function(response) {
			if(response.success == true) {
				
				appendUsers(response.message);
			}
		}
		
		var params = {
			"userSearchOption" : $("#userSearchOption").val(),
			"keyword" : $("#keyword").val()
		};
		
		sendRequest(ctx + "/web/staff/searchUser", params, callback);
	});
	
	// 모달에 검색된 사용자 리스트를 추가
	function appendUsers(userList) {
		$("#userList > tbody").html('');
		
		for(var i in userList) {
			var user = userList[i];
			
			var rowHtml = '<tr id="' + user.id + '"><td>' + user.nickname + '</td><td>';
			
			if(user.email != null) {
				rowHtml = rowHtml + user.email;
			}
			
			rowHtml = rowHtml + '</td></tr>';
			
			$("#userList > tbody").append(rowHtml);
		}
		
		$('#userList tr').click(function (event) {
			
			var nickname = $(this).find("td:first").html();
			
			if(confirm(nickname + '님을 운영자로 추가하시겠습니까?')) {
				joinAsStaff($(this).attr("id"));
			}
		});
	}
	
	$("#searchOption").trigger("change");
});