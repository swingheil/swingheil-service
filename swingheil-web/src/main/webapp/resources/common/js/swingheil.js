function sendRequest(url, param, callback) {
	$.ajax({
		type : "POST",
		url  : url,
		data : param,
		success : function(response, status, request) {
			if(response.success == true) {
				callback(response);
			} else {
				alert("fail!! " + response.message);
				callback(response);
			}
		}
	});
}