package com.swingheil.rest.user.controller;


//@Controller
//@RequestMapping("/device")
public class DeviceController {
	
//	private Logger log = LoggerFactory.getLogger(this.getClass());
//	
//	@Autowired
//	private DeviceService deviceService;
//	
//	/**
//	 * 디바이스를 등록한다.
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public @ResponseBody Device registerDevice(Device device) {
//		log.debug("/device/register() is called."); 
//		log.debug(device.toString()); 
//		
//		deviceService.registerDevice(
//				device.getDeviceUid(),
//				device.getUserId(),
//				device.getModelId(),
//				device.getOsVersion(), 
//				device.getRegistrationId());  
//		 
//		return deviceService.getDevice(device.getDeviceUid());
//	}
//	
//	/**
//	 * 디바이스를 조회한다.
//	 * 
//	 * @param deviceUid
//	 * @return
//	 */
//	@RequestMapping(value = "/getDevice/{deviceUid}", method = RequestMethod.GET)
//	public @ResponseBody Device getDevice(@PathVariable String deviceUid) {
//		log.debug("/device/getDevice/{} is called.", deviceUid);
//		
//		return deviceService.getDevice(deviceUid); 
//	}
//	
//	/**
//	 * 사용자의 디바이스 리스트를 조회한다.
//	 * 
//	 * @param deviceUid
//	 * @return
//	 */
//	@RequestMapping(value = "/getDeviceList/{userId}", method = RequestMethod.GET)
//	public @ResponseBody List<Device> getDeviceList(@PathVariable String userId) {
//		log.debug("/device/getDeviceList/{} is called.", userId);
//		
//		return deviceService.getDeviceList(userId);
//	}
//	
//	/**
//	 * 사용자의 디바이스를 수정한다.
//	 * 
//	 * @return
//	 */
//	@RequestMapping(value = "/update", method = RequestMethod.POST)
//	public @ResponseBody Device updateDevice(@ModelAttribute Device device) {
//		log.debug("/device/update() is called."); 
//		log.debug(device.toString()); 
//		
//		deviceService.updateDevice(
//				device.getDeviceUid(),
//				device.getUserId(),
//				device.getModelId(), 
//				device.getOsVersion(),
//				device.getRegistrationId());
//		 
//		return deviceService.getDevice(device.getDeviceUid());
//	}
//	
//	/**
//	 * 사용자의 디바이스를 삭제한다.
//	 * 
//	 * @param deviceUid
//	 * @return
//	 */
//	@RequestMapping(value = "/removeDevice/{deviceUid}", method = RequestMethod.GET)
//	public @ResponseBody void removeDevice(@PathVariable String deviceUid) {
//		log.debug("/device/removeDevice/{} is called.", deviceUid);
//		
//		deviceService.removeDevice(deviceUid);  
//	}
}
