package com.swingheil.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.swingheil.comp.process.BarService;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.shared.ResponseMessage;

@Controller
@RequestMapping("/rest/bar")
public class BarRestController {
	//
	@Autowired
	private BarService barService;
	
	@RequestMapping(value = "/getBarList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Bar> getBarList(
			@RequestParam(value = "userId", required = false) String userId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return barService.getBars(userId, pageInfo);
	}
	
	@RequestMapping(value = "/getManagedBarList", method = RequestMethod.GET)
	@ResponseBody
	public List<Bar> getManagedBarList(
			@RequestParam(value = "userId") String userId){
		//
		return barService.getManagedBars(userId);
	}
	
	@RequestMapping(value = "/getBar", method = RequestMethod.GET)
	@ResponseBody
	public Bar getBar(
			@RequestParam("barId") String barId, 
			@RequestParam("userId") String userId) {
		//
		return barService.getBar(userId, barId);
	}
	
	@RequestMapping(value = "/joinAsStaff", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage requestAuthority(String userId, String barId) {
		//
		try {
			barService.joinAsStaff(barId, userId);
			return new ResponseMessage(true);
		} catch(Exception ex) {
			return new ResponseMessage(false, ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage follow(
			@RequestParam("userId") String userId, 
			@RequestParam("barId") String barId) {
		//
		barService.followBar(userId, barId);
		return new ResponseMessage(true);
	}
	
	@RequestMapping(value = "/unfollow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage unfollow(
			@RequestParam("userId") String userId, 
			@RequestParam("barId") String barId) {
		//
		barService.unfollowBar(userId, barId);
		return new ResponseMessage(true);
	}
}
