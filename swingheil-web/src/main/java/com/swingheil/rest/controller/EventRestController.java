package com.swingheil.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.swingheil.comp.process.EventService;
import com.swingheil.comp.process.UserService;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.type.enums.EventCategory;

@Controller
@RequestMapping("/rest/event")
public class EventRestController {
	//
	@Autowired
	private EventService eventService;
	
	@Autowired
	private UserService userService;
	
	/**
	 * 사용자가 즐겨찾기한 동호회/바의 이벤트 리스트 조회
	 * <pre>
	 *   최근 등록된 이벤트순으로 조회한다.
	 * </pre>
	 * 
	 * @param userId
	 * @param page
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/getEventList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Event> getEventList(
			@RequestParam("userId") String userId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return eventService.getEvents(userId, pageInfo);
	}
	
	@RequestMapping(value = "/getFavoriteEventList", method = RequestMethod.GET)
	@ResponseBody
	public List<Event> getFavoriteEventList(@RequestParam("userId") String userId) {
		//
		return eventService.getFavoriteEvents(userId);
	}
	
	@RequestMapping(value = "/getSwingheilEventList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Event> getSwingheilEventList(
			@RequestParam("userId") String userId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return eventService.getEventsByCategory(
				userId, EventCategory.SwingHeil.code(), pageInfo);
	}
	
	@RequestMapping(value = "/getClubEventList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Event> getClubEventList(
			@RequestParam("userId") String userId, 
			@RequestParam("clubId") String clubId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return eventService.getEventsByClubId(userId, clubId, pageInfo);
	}
	
	@RequestMapping(value = "/getBarEventList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Event> getBarEventList(
			@RequestParam("userId") String userId, 
			@RequestParam("barId") String barId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return eventService.getEventsByBarId(userId, barId, pageInfo);
	}
	
	@RequestMapping(value = "/getEvent", method = RequestMethod.GET)
	@ResponseBody
	public Event getEvent(@RequestParam("userId") String userId, @RequestParam("eventId") String eventId) {
		//
		return eventService.getEvent(userId, eventId);
	}
	
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage follow(@RequestParam("userId") String userId, @RequestParam("eventId") String eventId) {
		//
		eventService.followEvent(userId, eventId);
		return new ResponseMessage(true);
	}
	
	@RequestMapping(value = "/unfollow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage unfollow(@RequestParam("userId") String userId, @RequestParam("eventId") String eventId) {
		//
		eventService.unfollowEvent(userId, eventId);
		return new ResponseMessage(true);
	}
}
