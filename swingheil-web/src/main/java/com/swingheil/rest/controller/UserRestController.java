package com.swingheil.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.swingheil.comp.process.UserService;
import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@Controller
@RequestMapping("/rest/user")
public class UserRestController {

	@Autowired
	private UserService userService;
	
	/**
	 * 사용자(디바이스) 등록
	 * @return
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage register(
			@RequestParam("nickname") String nickname, 
			@RequestParam("deviceUid") String deviceUid,
			@RequestParam("modelId") String modelId,
			@RequestParam("osVersion") String osVersion,
			@RequestParam("registrationId") String registrationId) {  
		//
		SocialUser user = userService.getUserByDeviceUid(deviceUid);
		
		// 기기 사용자가 등록되지 않은 경우만 등록처리
		if (user == null) {
			Device device = new Device(deviceUid, modelId, osVersion, registrationId);
			user = userService.registerUser(nickname, device);
		}
		return new ResponseMessage(user);
	}
	
	/**
	 * deviceUid로 사용자 기본정보 조회
	 * 
	 * @param deviceUid
	 * @return
	 */
	@RequestMapping(value = "/getUserByDeviceUid", method = RequestMethod.GET)
	@ResponseBody	
	public ResponseMessage getUserByDeviceUid(
			@RequestParam("deviceUid") String deviceUid) {
		//
		SocialUser user = userService.getUserByDeviceUid(deviceUid);
		return new ResponseMessage(user);
	}
	
	/**
	 * 이메일 등록
	 * 
	 * @param deviceUid
	 * @param userId
	 * @param email
	 * @param password
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "/registerEmail", method = RequestMethod.POST)
	@ResponseBody	
	public ResponseMessage registerEmail(
			@RequestParam("deviceUid") String deviceUid,
			@RequestParam("userId") String userId,
			@RequestParam("email") String email,
			@RequestParam("password") String password,
			@RequestParam("phone") String phone) {
		//
		// 등록된 deviceUid 인가?
		if (userService.getUserByDeviceUid(deviceUid) == null) {
			return new ResponseMessage(true);
		} 
		
		// 등록된 userId 인가?
		if (userService.getUserByUserid(userId) == null) {
			return new ResponseMessage(true);
		} 
		
		userService.updateUserDetail(userId, email, password, phone);
		return new ResponseMessage(true);
	}
}
