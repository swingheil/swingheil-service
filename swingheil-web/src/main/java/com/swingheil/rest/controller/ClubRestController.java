package com.swingheil.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.swingheil.comp.process.ClubService;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.shared.ResponseMessage;

@Controller
@RequestMapping("/rest/club")
public class ClubRestController {
	//
	@Autowired
	private ClubService clubService;
	
	@RequestMapping(value = "/getClubList", method = RequestMethod.GET)
	@ResponseBody
	public Page<Club> getClubList(
			@RequestParam(value = "userId", required = false) String userId, 
			@RequestParam(value = "page", defaultValue = "1") int page, 
			@RequestParam(value = "limit", defaultValue = "10") int limit) {
		//
		PageInfo pageInfo = new PageInfo(page, limit);
		return clubService.getClubs(userId, pageInfo);
	}
	
	@RequestMapping(value = "/getManagedClubList", method = RequestMethod.GET)
	@ResponseBody
	public List<Club> getManagedBarList(
			@RequestParam(value = "userId") String userId){
		//
		return clubService.getManagedClubs(userId);
	}	
	
	@RequestMapping(value = "/getClub", method = RequestMethod.GET)
	@ResponseBody
	public Club getClub(
			@RequestParam("clubId") String clubId, 
			@RequestParam("userId") String userId) {
		//
		return clubService.getClub(userId, clubId);
	}
	
	@RequestMapping(value = "/joinAsStaff", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage requestAuthority(String userId, String clubId) {
		//
		try {
			clubService.joinAsStaff(clubId, userId);
			
			return new ResponseMessage(true);
		} catch(Exception ex) {
			return new ResponseMessage(false, ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/follow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage follow(
			@RequestParam("userId") String userId, 
			@RequestParam("clubId") String clubId) {
		//
		clubService.followClub(userId, clubId);
		return new ResponseMessage(true);
	}
	
	@RequestMapping(value = "/unfollow", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage unfollow(
			@RequestParam("userId") String userId, 
			@RequestParam("clubId") String clubId) {
		//
		clubService.unfollowClub(userId, clubId);
		return new ResponseMessage(true);
	}
}
