package com.swingheil.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.swingheil.comp.process.UserService;
import com.swingheil.domain.type.enums.UserState;
import com.swingheil.domain.user.SocialUser;
import com.swingheil.web.controller.util.MessagePage;
import com.swingheil.web.session.SessionManager;

@Controller
public class LoginController {
	
	@Autowired
	UserService userService;
	
	//
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		//
		return "user/login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(
			@RequestParam("inputEmail") String email, 
			@RequestParam("inputPassword") String password, 
			HttpServletRequest req) {
		// 
		SessionManager sessionManager = SessionManager.getInstance(req);
		if (sessionManager.login(email, password)) {
			//
			String userId = sessionManager.getLoginId();
			
			if(userId.equals("admin")) {
				return new ModelAndView("redirect:/web/event/list");
			}
			
			SocialUser user =  userService.getUserByUserid(userId);
			
			if(user.getState() == UserState.Requested){
				String linkURL = "/login";
				return MessagePage.error("승인 대기중입니다", linkURL);
			}
			
			return new ModelAndView("redirect:/web/event/list");
			
		} else {
			//
			req.setAttribute("email", email);
			req.setAttribute("password", password);
			
			String message = "로그인에 실패하였습니다. 회원정보를 확인하세요";
			String linkURL = "/login";
			
			return MessagePage.error(message, linkURL);
		}
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest req) {
		//
		SessionManager.getInstance(req).logout();
		
		return "redirect:/login";
	}
}
