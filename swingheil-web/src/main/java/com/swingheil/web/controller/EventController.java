package com.swingheil.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.swingheil.comp.entity.shared.util.FileInfo;
import com.swingheil.comp.entity.shared.util.FileRepository;
import com.swingheil.comp.process.BarService;
import com.swingheil.comp.process.ClubService;
import com.swingheil.comp.process.EventNotiService;
import com.swingheil.comp.process.EventService;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.enums.EventCategory;
import com.swingheil.web.controller.command.EventCmd;
import com.swingheil.web.controller.util.ImageUtil;
import com.swingheil.web.session.LoginRequired;
import com.swingheil.web.session.SessionManager;

@Controller
@RequestMapping("/web/event")
@LoginRequired(true)
public class EventController {

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private EventService eventService;
	
	@Autowired
	private BarService barService;
	
	@Autowired
	private ClubService clubService;
	
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private EventNotiService eventNotiService;

	// --------------------------------------------------------------------------
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String getEventList(HttpServletRequest request, Model model) {
		//

		String userId = SessionManager.getInstance(request).getLoginId();

		PageInfo condition = new PageInfo(1, 99999);
		condition.setForce(true);
		
		Page<Event> page = eventService.getEvents(userId, condition);
		
		if(page.getResults() != null && page.getResults().size() > 0){
			for(Event event : page.getResults()) {
				
				if(event.getSponsor() == null) {
					continue;
				}
				
				String id = event.getSponsor().getId();
				
				if(StringUtils.hasText(id) == false){
					continue;
				}
				
				if(event.getCategory().getCode().equals(EventCategory.SwingBar.code())) {
					Bar bar = barService.getBar(null, id);
					event.setSponsor(bar);
				} else if(event.getCategory().getCode().equals(EventCategory.Club.code())) {
					Club club = clubService.getClub(null, id);
					event.setSponsor(club);
				}
			}
		}
		
		model.addAttribute("events", page.getResults());

		return "event/list";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerForm(HttpServletRequest request) {
		//
		if (SessionManager.getInstance(request).isSuperAdmin()) {
			request.setAttribute("superAdminYn", "Y");
		}
		
		return "event/register";
	}
	
	@RequestMapping(value = "/{eventId}/modify", method = RequestMethod.GET)
	public String modifyForm(@PathVariable("eventId") String eventId, HttpServletRequest request) {
		//
		String userId = SessionManager.getInstance(request).getLoginId();

		Event event = eventService.getEvent(userId, eventId);
		request.setAttribute("processType", "modify");
		request.setAttribute("event", event);
		
		return "event/register";
	}
	
	@RequestMapping(value = "/{eventId}/delete", method = RequestMethod.GET)
	public String deleteEvent(@PathVariable("eventId") String eventId, HttpServletRequest request) {
		//
		String userId = SessionManager.getInstance(request).getLoginId();
		 
		Event prevEvent = eventService.getEvent(null, eventId);
		String sponsorId = prevEvent.getSponsor().getId();
		
		boolean removable = false;
		if (SessionManager.getInstance(request).isSuperAdmin()) {
			removable = true;
		} else {
			List<Club> clubs = clubService.getManagedClubs(userId);
			if (clubs != null) {
				for (Club club : clubs) {
					if (club.getId().equals(sponsorId)) {
						removable = true;
						break;
					}
				}
			}
			
			if (removable) {
				List<Bar> bars = barService.getManagedBars(userId);
				for (Bar bar : bars) {
					if (bar.getId().equals(sponsorId)) {
						removable = true;
						break;
					}
				}
			}
		}
		
		if (removable) {
			String prevLargeImageId = prevEvent.getLargeImageId();
			String prevSmallImageId = prevEvent.getSmallImageId();
			
			
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevLargeImageId)) {
				fileRepository.remove(prevLargeImageId);
			}
			if (!StringUtils.isEmpty(prevSmallImageId)) {
				fileRepository.remove(prevSmallImageId);
			}
			
			// 이벤트 즐겨찾기 해제
			eventService.unfollowEvent(userId, eventId);
			eventService.removeEvent(eventId);
		}
		
		return "redirect:/web/event/list";
	}
	
	@RequestMapping(value = "/{eventId}/modify", method = RequestMethod.POST)
	public String modifyEvent(@PathVariable("eventId") String eventId, EventCmd command, HttpServletRequest request) throws IOException {
		//
		String userId = SessionManager.getInstance(request).getLoginId();
		Event prevEvent = eventService.getEvent(userId, eventId);
		String prevLargeImageId = prevEvent.getLargeImageId();
		String prevSmallImageId = prevEvent.getSmallImageId();

		// 이전 이벤트 객체에 수정대상 필드만 세팅
		Event event = command.createDomain(prevEvent); 
		event.setId(eventId);
		event.setLargeImageId(prevLargeImageId);
		event.setSmallImageId(prevSmallImageId);
		
		MultipartFile largeImage = command.getLargeImage();
		if (largeImage.getSize() > 0) {
			
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevLargeImageId)) {
				fileRepository.remove(prevLargeImageId);
			}
			if (!StringUtils.isEmpty(prevSmallImageId)) {
				fileRepository.remove(prevSmallImageId);
			}
			
			InputStream largeImageStream = null;
			
			if(largeImage.getSize() > 500 * 1024) {
				largeImageStream = ImageUtil.resize(largeImage, 1920);
			} else {
				largeImageStream = largeImage.getInputStream();
			}
			
			FileInfo largeFile = fileRepository.store(
					largeImageStream,
					largeImage.getOriginalFilename(),
					largeImage.getContentType());
			event.setLargeImageId(largeFile.getId());
			
			// Thumbnail 이미지 생성
			FileInfo smallFile = fileRepository.store(
					ImageUtil.resize(largeImage, 150),
					"thumbnail_" + largeImage.getOriginalFilename(),
					largeImage.getContentType());
			event.setSmallImageId(smallFile.getId());
		}
		
		eventService.modifyEvent(event);
		
		return "redirect:/web/event/" + eventId;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerEvent(EventCmd command) throws IOException {
		//
		Event event = command.createDomain(null);

		MultipartFile largeImage = command.getLargeImage();
		if (largeImage.getSize() > 0) {
			
			InputStream largeImageStream = null;
			
			if(largeImage.getSize() > 500 * 1024) {
				largeImageStream = ImageUtil.resize(largeImage, 1920);
			} else {
				largeImageStream = largeImage.getInputStream();
			}
			
			FileInfo largeFile = fileRepository.store(
					largeImageStream,
					largeImage.getOriginalFilename(),
					largeImage.getContentType());
			event.setLargeImageId(largeFile.getId());
			
			// Thumbnail 이미지 생성
			FileInfo smallFile = fileRepository.store(
					ImageUtil.resize(largeImage, 150),
					"thumbnail_" + largeImage.getOriginalFilename(),
					largeImage.getContentType());
			event.setSmallImageId(smallFile.getId());
		}

		eventService.registerEvent(event);
		
		eventNotiService.sendPushMessage(event);
		
		return "redirect:/web/event/list";
	}

	@RequestMapping(value = "/{eventId}", method = RequestMethod.GET)
	public String getEvent(HttpServletRequest request,
			@PathVariable("eventId") String eventId, Model model) {
		//
		String userId = SessionManager.getInstance(request).getLoginId();

		Event event = eventService.getEvent(userId, eventId);
		model.addAttribute("event", event);

		return "event/detail";
	}

	@LoginRequired(false)
	@RequestMapping(value = "/{eventId}/thumbnail", method = RequestMethod.GET)
	public void getSmallImage(@PathVariable("eventId") String eventId,
			HttpServletResponse response) throws IOException {
		//
		Event event = eventService.getEvent(null, eventId);
		
		if (!StringUtils.isEmpty(event.getSmallImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(event.getSmallImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(event.getSmallImageId(), response.getOutputStream());
		} else {
			InputStream in = servletContext.getResourceAsStream("/noimage.png");
			IOUtils.copy(in, response.getOutputStream());
			IOUtils.closeQuietly(in);
		}
	}

	@LoginRequired(false)
	@RequestMapping(value = "/{eventId}/poster", method = RequestMethod.GET)
	public void getLargeImage(@PathVariable("eventId") String eventId,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		//
		Event event = eventService.getEvent(null, eventId);
		
		if (!StringUtils.isEmpty(event.getLargeImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(event.getLargeImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(event.getLargeImageId(), response.getOutputStream());
		} else {
			InputStream in = servletContext.getResourceAsStream("/noimage.png");
			IOUtils.copy(in, response.getOutputStream());
			IOUtils.closeQuietly(in);
		}
	}
}
