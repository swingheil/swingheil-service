package com.swingheil.web.controller.command;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.DateTime;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.enums.PeriodType;
import com.swingheil.domain.type.place.Address;

public class EventCmd {
	//
	private String category;
	private String sponsor;
	private String fromDate; // yyyy-mm-dd
	private String fromTime; // hh:mm
	private String toDate;   // yyyy-mm-dd
	private String toTime;   // hh:mm
	private String eventName;
	private String summary;
	private String district;
	private String roadAddress;
	private String landAddress;
	private String detail;
	private boolean visible;
	private boolean notice;
	
	private MultipartFile largeImage;
	
	//--------------------------------------------------------------------------
	
	public Event createDomain(Event event) {
		//
		if (event == null) {
			event = new Event();
		}
		
		event.setCategory(new CodeName(category, ""));
		if (!StringUtils.isEmpty(sponsor)) {
			event.setSponsor(new Group(sponsor));
		} else {
			event.setSponsor(new Group()); // swingheil is that group id is null.
		}
		
		Period period = new Period();
		event.setPeriod(period);

		if (!StringUtils.isEmpty(fromDate)) {
			DateTime from = new DateTime();
			if (!StringUtils.isEmpty(fromDate)) from.setDate(fromDate);
			if (!StringUtils.isEmpty(fromTime)) from.setTime(fromTime);
			period.setFrom(from);
			period.setType(PeriodType.Day);
		}
		
		if (!StringUtils.isEmpty(toDate)) {
			DateTime to = new DateTime();
			if (!StringUtils.isEmpty(toDate))  to.setDate(toDate);
			if (!StringUtils.isEmpty(toTime))  to.setTime(toTime);
			period.setTo(to);
			period.setType(PeriodType.Term);
		}
		
		event.setTitle(eventName);
		event.setSummary(summary);
		event.setPlace(new Address(district, roadAddress, landAddress));
		event.setDetail(detail);
		event.setVisible(visible);
		event.setNotice(notice);
		
		return event;
	}
	
	//--------------------------------------------------------------------------
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSponsor() {
		return sponsor;
	}
	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getRoadAddress() {
		return roadAddress;
	}
	public void setRoadAddress(String roadAddress) {
		this.roadAddress = roadAddress;
	}
	public String getLandAddress() {
		return landAddress;
	}

	public void setLandAddress(String landAddress) {
		this.landAddress = landAddress;
	}

	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public MultipartFile getLargeImage() {
		return largeImage;
	}
	public void setLargeImage(MultipartFile largeImage) {
		this.largeImage = largeImage;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isNotice() {
		return notice;
	}

	public void setNotice(boolean notice) {
		this.notice = notice;
	}
}
