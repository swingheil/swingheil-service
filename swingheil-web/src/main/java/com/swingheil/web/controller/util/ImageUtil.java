package com.swingheil.web.controller.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.springframework.web.multipart.MultipartFile;

public class ImageUtil {
	
	/**
	 * 이미지를 주어진 크기로 변환하여 InputStream으로 리턴한다.
	 * 
	 * @param in   이미지파일의 InputStream
	 * @param size 변환할 사이즈(px)
	 * @return
	 * @throws IOException 
	 */
	public static InputStream resize(MultipartFile file, int size) throws IOException {
		
		BufferedImage sourceImage = ImageIO.read(file.getInputStream());
		BufferedImage convertedImage = Scalr.resize(sourceImage, size);
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		String oriFileName = file.getOriginalFilename();
		String imageFormat = oriFileName.substring(oriFileName.lastIndexOf(".") + 1);
		ImageIO.write(convertedImage, imageFormat, bos);
		InputStream newIn = new ByteArrayInputStream(bos.toByteArray());
		
		return newIn;
	}

}
