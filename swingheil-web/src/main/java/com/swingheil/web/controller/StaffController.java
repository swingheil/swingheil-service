package com.swingheil.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.swingheil.comp.process.BarService;
import com.swingheil.comp.process.ClubService;
import com.swingheil.comp.process.EventService;
import com.swingheil.comp.process.UserService;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.user.SocialUser;
import com.swingheil.web.session.LoginRequired;
import com.swingheil.web.session.SessionManager;

@Controller
@RequestMapping("/web/staff")
@LoginRequired(true)
public class StaffController {

	@Autowired
	UserService userService;

	@Autowired
	EventService eventService;

	@Autowired
	BarService barService;

	@Autowired
	ClubService clubService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getAdminMain(Model model) {

		// PageInfo condition = new PageInfo(1, 99999);
		// Page<Event> page = eventService.getEvents(null, condition);
		// model.addAttribute("events", page.getResults());

		return "staff/list";
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getBarOrClubList(
			@RequestParam(value = "type", required = true) String type,
			HttpServletRequest request) {

		// String userId = SessionManager.getInstance(req).getLoginId();
		String userId = SessionManager.getInstance(request).getLoginId();

		ResponseMessage message = null;

		if (type.equals("bar") == true) {

			List<Bar> bars = barService.getManagedBars(userId);

			message = new ResponseMessage(true, bars);

		} else if (type.equals("club") == true) {

			List<Club> clubs = clubService.getManagedClubs(userId);

			message = new ResponseMessage(true, clubs);

		} else {

			message = new ResponseMessage(false);
		}

		return message;
	}

	@RequestMapping(value = "/staffList", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage getStaffList(
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id,
			HttpServletRequest request) {

		// String userId = SessionManager.getInstance(req).getLoginId();
		String userId = SessionManager.getInstance(request).getLoginId();

		ResponseMessage message = null;

		try {
			if (type.equals("bar")) {

				Bar bar = barService.getBar(userId, id);
				
				Assert.notNull(bar, "관리중인 스윙바가 없습니다");
				
				message = new ResponseMessage(true, bar.getStaffs());

			} else if (type.equals("club")) {

				Club club = clubService.getClub(userId, id);
				
				Assert.notNull(club, "관리중인 동호회가 없습니다");

				message = new ResponseMessage(true, club.getStaffs());

			} else {
				message = new ResponseMessage(false);
			}
		} catch(Exception ex) {
			message = new ResponseMessage(false, ex.getMessage());
		}
		

		return message;

	}

	@RequestMapping(value = "/searchUser", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage searchUser(
			@RequestParam(value = "userSearchOption", required = true) String option,
			@RequestParam(value = "keyword", required = true) String keyword) {

		ResponseMessage message = null;
		List<SocialUser> user = null;

		if (option.equals("nickname")) {

			user = userService.getUsersByNickname(keyword);

		} else if (option.equals("tel")) {

			user = userService.getUsersByPhoneNo(keyword);

		} else if (option.equals("email")) {

			user = userService.getUsersByEmail(keyword);
		}

		message = new ResponseMessage(true, user);

		return message;
	}

	@RequestMapping(value = "/joinAsStaff", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage joinAsStaff(
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "userId", required = true) String userId) {

		ResponseMessage message = null;

		try {
			if (type.equals("bar")) {

				// TODO 이미 가입되어 있는 경우를 체크해야 함

				barService.joinAsStaff(id, userId);

				message = new ResponseMessage(true);

			} else if (type.equals("club")) {

				// TODO 이미 가입되어 있는 경우 체크해야 함

				clubService.joinAsStaff(id, userId);

				message = new ResponseMessage(true);

			} else {

				message = new ResponseMessage(false);
			}
		} catch (Exception ex) {
			message = new ResponseMessage(false, ex.getMessage());
		}

		return message;
	}

	@RequestMapping(value = "/withdrawAsStaff", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage withdrawAsStaff(
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "userId", required = true) String userId) {
		ResponseMessage message = null;

		try {
			if (type.equals("bar")) {

				barService.withdrawAsStaff(id, userId);

				message = new ResponseMessage(true);

			} else if (type.equals("club")) {

				clubService.withdrawAsStaff(id, userId);

				message = new ResponseMessage(true);

			} else {

				message = new ResponseMessage(false);
			}
		} catch (Exception ex) {
			message = new ResponseMessage(false);
		}

		return message;
	}

	@RequestMapping(value = "/activateAsStaff", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage activateStaff(
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "userId", required = true) String userId) {

		ResponseMessage message = null;
		
		try {
			
			if(type.equals("bar")) {
				
				barService.activateAsStaff(id, userId);
				
				message = new ResponseMessage(true);
				
			} else if(type.equals("club")) {
				
				clubService.activateAsStaff(id, userId);
				
				message = new ResponseMessage(true);
				
			} else {
				message = new ResponseMessage(false);
			}
			
		} catch(Exception ex) {
			message = new ResponseMessage(false, ex.getMessage());
		}
		
		return message;
	}

	@RequestMapping(value = "/delegateAsAdmin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseMessage delegateAdmin(
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "userId", required = true) String userId) {

		ResponseMessage message = null;

		try {

			if (type.equals("bar")) {

				barService.deleageAdmin(id, userId);

				message = new ResponseMessage(true);

			} else if (type.equals("club")) {

				clubService.delegateAdmin(id, userId);

				message = new ResponseMessage(true);

			} else {
				message = new ResponseMessage(false);
			}

		} catch (Exception ex) {
			message = new ResponseMessage(false);
		}

		return message;
	}
}
