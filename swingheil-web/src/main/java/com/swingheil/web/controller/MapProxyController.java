package com.swingheil.web.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.swingheil.domain.shared.ResponseMessage;

@Controller
@RequestMapping("/web/proxy")
public class MapProxyController {

	RestTemplate restTemplate;
	
	@ResponseBody
	@RequestMapping(value = "/")
	public ResponseMessage proxy(
			@RequestParam(value = "query") String query,
			HttpServletResponse response) {
		
		ResponseMessage message = null;
		
		try {
			
			String url = "http://openapi.map.naver.com/api/geocode.php?key=ec01f1aac7a9dc2623526edaf4892172&encoding=utf-8&coord=latlng&query=" + encodeURIComponent(query);
			HttpRequestBase request = new HttpGet(url);
			
			HttpClient client = new DefaultHttpClient();
			HttpResponse res = client.execute(request);
			
			String xml = EntityUtils.toString(res.getEntity(), HTTP.UTF_8);
			
			JSONObject jsonObj = XML.toJSONObject(xml);
			
			message = new ResponseMessage(true, xml);
			
		} catch(Exception ex) {
			message = new ResponseMessage(false, ex.getMessage());
		}
		
		return message;
	}
	
	private String encodeURIComponent(String query) {
		String result = null;
		
		try {
			
			result = URLEncoder.encode(query, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
			
		} catch(UnsupportedEncodingException ex) {
			result = query;
		}
		
		return result;
	}
 }
