package com.swingheil.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.swingheil.comp.entity.shared.util.FileInfo;
import com.swingheil.comp.entity.shared.util.FileRepository;
import com.swingheil.comp.process.BarService;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.type.place.RoughMap;
import com.swingheil.web.controller.command.BarCmd;
import com.swingheil.web.controller.util.ImageUtil;
import com.swingheil.web.controller.util.MessagePage;
import com.swingheil.web.session.LoginRequired;
import com.swingheil.web.session.SessionManager;

@Controller
@RequestMapping("/web/bar")
@LoginRequired(true)
public class BarController {
	
	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private BarService barService;
	
	//--------------------------------------------------------------------------

//	@RequestMapping(value = "/list", method = RequestMethod.GET)
//	public String getBarList(Model model) {
//		//
//		PageInfo condition = new PageInfo(1, 99999);
//		Page<Bar> page = barService.getBars(null, condition);
//		model.addAttribute("bars", page.getResults());
//		
//		return "bar/list";
//	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getManagedBar(HttpServletRequest request, Model model) {

		String userId = SessionManager.getInstance(request).getLoginId();
		
		List<Bar> bars = barService.getManagedBars(userId);
		
		 model.addAttribute("bars", bars);

		return "bar/list";
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public ResponseMessage getManagedBarList(HttpServletRequest request, Model model) {
		try {
			
			String userId = SessionManager.getInstance(request).getLoginId();
			
			List<Bar> bars = barService.getManagedBars(userId);
			
			ResponseMessage message = new ResponseMessage(true, bars);
			
			return message;
			
		} catch(Exception ex) {
			ResponseMessage message = new ResponseMessage(false, ex.getMessage());
			
			return message;
		}
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerForm() {
		//
		return "bar/register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerBar(BarCmd command) throws IOException {
		//
		Bar bar = command.createDomain();
		
		MultipartFile thumbnailImage = command.getThumbnailImage();
		if (thumbnailImage.getSize() > 0) {
			
			InputStream thumbnailStream = null;
			
			if(thumbnailImage.getSize() > 500 * 1024) {
				thumbnailStream = ImageUtil.resize(thumbnailImage, 1920);
			} else {
				thumbnailStream = thumbnailImage.getInputStream();
			}
			
			FileInfo thumbnailFile = fileRepository.store(
//					thumbnailImage.getInputStream(), 
					thumbnailStream,
					thumbnailImage.getOriginalFilename(), 
					thumbnailImage.getContentType());
			bar.setThumbnailImageId(thumbnailFile.getId());
		}
		
		MultipartFile bannerImage = command.getBannerImage();
		if (bannerImage.getSize() > 0) {
			
			InputStream bannerImageStream = null;
			
			if(bannerImage.getSize() > 500 * 1024) {
				bannerImageStream = ImageUtil.resize(bannerImage, 1920);
			} else {
				bannerImageStream = bannerImage.getInputStream();
			}
			
			FileInfo bannerFile = fileRepository.store(
//					bannerImage.getInputStream(), 
					bannerImageStream,
					bannerImage.getOriginalFilename(), 
					bannerImage.getContentType());
			bar.setBannerImageId(bannerFile.getId());
		}
		
		barService.registerBar(bar);
		return "redirect:/web/bar/";
	}
	
	@RequestMapping(value = "/{barId}", method = RequestMethod.GET)
	public String getBar(@PathVariable("barId") String barId, Model model) {
		//
		Bar bar = barService.getBar(null, barId);
		model.addAttribute("bar", bar);
		
		return "bar/detail";
	}
	
	@RequestMapping(value = "/{barId}/modify", method = RequestMethod.GET)
	public ModelAndView modifyForm(@PathVariable("barId") String barId, HttpServletRequest request) {
		//
		ModelAndView mav = new ModelAndView("bar/modify");
		
		Bar bar = barService.getBar(null, barId);
		mav.addObject("bar", bar);

		if (SessionManager.getInstance(request).hasAuthority(bar) == false) {
			return MessagePage.error("수행권한이 없습니다.", "/web/staff/");
		}
		
		return mav;
	}
	
	@RequestMapping(value = "/{barId}/modify", method = RequestMethod.POST)
	public String modifyBar(BarCmd command) throws IOException {
		//
		Bar bar = barService.getBar(null, command.getId());
		String prevThumbnailId = bar.getThumbnailImageId();
		String prevBannerId = bar.getBannerImageId();
		
		RoughMap prevMap = null;
		if (bar.getAddress() != null) {
			prevMap = bar.getAddress().getMap();
		}
		
		bar = command.createDomain(bar);
		
		// 지하철 정보는 화면에서 입력받지 않으므로 
		// 이전에 저장된 정보를 세팅함
		bar.getAddress().setMap(prevMap);
		
		MultipartFile thumbnailImage = command.getThumbnailImage();
		if (thumbnailImage.getSize() > 0) {
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevThumbnailId)) {
				fileRepository.remove(prevThumbnailId);
			}
			
			InputStream thumbnailStream = null;
			
			if(thumbnailImage.getSize() > 500 * 1024) {
				thumbnailStream = ImageUtil.resize(thumbnailImage, 1920);
			} else {
				thumbnailStream = thumbnailImage.getInputStream();
			}
			
			FileInfo thumbnailFile = fileRepository.store(
//					thumbnailImage.getInputStream(),
					thumbnailStream,
					thumbnailImage.getOriginalFilename(), 
					thumbnailImage.getContentType());
			bar.setThumbnailImageId(thumbnailFile.getId());
		} else {
			// 업로드된 이미지가 없으면 이전 이미지 유지
			bar.setThumbnailImageId(prevThumbnailId);
		}
		
		MultipartFile bannerImage = command.getBannerImage();
		if (bannerImage.getSize() > 0) {
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevBannerId)) {
				fileRepository.remove(prevBannerId);
			}
			
			InputStream bannerImageStream = null;
			
			if(bannerImage.getSize() > 500 * 1024) {
				bannerImageStream = ImageUtil.resize(bannerImage, 1920);
			} else {
				bannerImageStream = bannerImage.getInputStream();
			}
			
			FileInfo bannerFile = fileRepository.store(
//					bannerImage.getInputStream(), 
					bannerImageStream,
					bannerImage.getOriginalFilename(), 
					bannerImage.getContentType());
			bar.setBannerImageId(bannerFile.getId());
		} else {
			// 업로드된 이미지가 없으면 이전 이미지 유지
			bar.setBannerImageId(prevBannerId);
		}
		
		barService.modifyBar(bar);
		return "redirect:/web/bar/";
	}
	
	@LoginRequired(false)
	@RequestMapping(value = "/{barId}/thumbnail", method = RequestMethod.GET)
	public void getSmallImage(@PathVariable("barId") String barId, HttpServletResponse response) throws IOException {
		//
		Bar bar = barService.getBar(null, barId);
		
		if (!StringUtils.isEmpty(bar.getThumbnailImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(bar.getThumbnailImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(bar.getThumbnailImageId(), response.getOutputStream());
		}
	}
	
	@LoginRequired(false)
	@RequestMapping(value = "/{barId}/banner", method = RequestMethod.GET)
	public void getLargeImage(@PathVariable("barId") String barId, HttpServletResponse response) throws IOException {
		//
		Bar bar = barService.getBar(null, barId);
		if (!StringUtils.isEmpty(bar.getBannerImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(bar.getBannerImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(bar.getBannerImageId(), response.getOutputStream());
		}
	}
}
