package com.swingheil.web.controller.command;

import org.springframework.web.multipart.MultipartFile;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;

public class ClubCmd {
	//
	private String id; // 수정할 때만 세팅함
	private String nameKor;
	private String nameEng;
	private String homePage;
	private String barId;
	
	private MultipartFile thumbnailImage;
	private MultipartFile bannerImage;
	
	//--------------------------------------------------------------------------
	
	public Club createDomain() {
		//
		Club club = new Club();
		club.setId(id);
		club.setNameKor(nameKor);
		club.setNameEng(nameEng);
		club.setHomePage(homePage);
		club.setBar(new Bar(barId));

		return club;
	}
	
	public Club createDomain(Club prevClub) {
		//
		Club club = prevClub;
		club.setId(id);
		club.setNameKor(nameKor);
		club.setNameEng(nameEng);
		club.setHomePage(homePage);
		club.setBar(new Bar(barId));
		
		club.setAdmin(prevClub.getAdmin());
		club.setStaffs(prevClub.getStaffs());

		return club;
	}

	//--------------------------------------------------------------------------

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNameKor() {
		return nameKor;
	}

	public void setNameKor(String nameKor) {
		this.nameKor = nameKor;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getBarId() {
		return barId;
	}

	public void setBarId(String barId) {
		this.barId = barId;
	}

	public MultipartFile getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(MultipartFile thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public MultipartFile getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(MultipartFile bannerImage) {
		this.bannerImage = bannerImage;
	}
}
