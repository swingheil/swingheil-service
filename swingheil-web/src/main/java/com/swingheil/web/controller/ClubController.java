package com.swingheil.web.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.swingheil.comp.entity.shared.util.FileInfo;
import com.swingheil.comp.entity.shared.util.FileRepository;
import com.swingheil.comp.process.BarService;
import com.swingheil.comp.process.ClubService;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.web.controller.command.ClubCmd;
import com.swingheil.web.controller.util.ImageUtil;
import com.swingheil.web.controller.util.MessagePage;
import com.swingheil.web.session.LoginRequired;
import com.swingheil.web.session.SessionManager;

@Controller
@RequestMapping("/web/club")
@LoginRequired(true)
public class ClubController {
	
	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private ClubService clubService;
	
	@Autowired
	private BarService barService;
	
	//--------------------------------------------------------------------------
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getManagedBar(HttpServletRequest request, Model model) {

		String userId = SessionManager.getInstance(request).getLoginId();
		
		List<Club> clubs = clubService.getManagedClubs(userId);
		if (clubs != null) {
			for (Club club : clubs) {
				club.setBar(barService.getBar(null, club.getBar().getId()));
			}
		}
		
		model.addAttribute("clubs", clubs);

		return "club/list";
	}
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String getClubList(HttpServletRequest request, Model model) {
		//
		String userId = SessionManager.getInstance(request).getLoginId();
		
		List<Club> clubs = clubService.getManagedClubs(userId);
		
		model.addAttribute("clubs", clubs);
		
		return "club/";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerForm(Model model) {
		//
		PageInfo pageInfo = new PageInfo(1, 99999);
		Page<Bar> page = barService.getBars(null, pageInfo);
		
		model.addAttribute("bars", page.getResults());
		return "club/register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerClub(ClubCmd command) throws IOException {
		//
		Club club = command.createDomain();
		
		MultipartFile thumbnailImage = command.getThumbnailImage();
		if (thumbnailImage.getSize() > 0) {
			
			InputStream thumbnailStream = null;
			
			if(thumbnailImage.getSize() > 500 * 1024) {
				thumbnailStream = ImageUtil.resize(thumbnailImage, 1920);
			} else {
				thumbnailStream = thumbnailImage.getInputStream();
			}
			
			FileInfo thumbnailFile = fileRepository.store(
//					thumbnailImage.getInputStream(), 
					thumbnailStream,
					thumbnailImage.getOriginalFilename(), 
					thumbnailImage.getContentType());
			club.setThumbnailImageId(thumbnailFile.getId());
		}
		
		MultipartFile bannerImage = command.getBannerImage();
		if (bannerImage.getSize() > 0) {
			
			InputStream bannerImageStream = null;
			
			if(bannerImage.getSize() > 500 * 1024) {
				bannerImageStream = ImageUtil.resize(bannerImage, 1920);
			} else {
				bannerImageStream = bannerImage.getInputStream();
			}
			
			FileInfo bannerFile = fileRepository.store(
//					bannerImage.getInputStream(), 
					bannerImageStream,
					bannerImage.getOriginalFilename(), 
					bannerImage.getContentType());
			club.setBannerImageId(bannerFile.getId());
		}

		clubService.registerClub(club);
		return "redirect:/web/club/";
	}
	
	@RequestMapping(value = "/{clubId}", method = RequestMethod.GET)
	public String getClub(@PathVariable("clubId") String clubId, Model model) {
		//
		Club club = clubService.getClub(null, clubId);
		model.addAttribute("club", club);
		
		return "club/detail";
	}
	
	@RequestMapping(value = "/{clubId}/modify", method = RequestMethod.GET)
	public ModelAndView modifyForm(@PathVariable("clubId") String clubId, HttpServletRequest request) {
		//
		// Bar 목록 조회
		PageInfo pageInfo = new PageInfo(1, 99999);
		Page<Bar> page = barService.getBars(null, pageInfo);
		
		ModelAndView mav = new ModelAndView("club/modify");
		mav.addObject("bars", page.getResults());	
		
		Club club = clubService.getClub(null, clubId);
		mav.addObject("club", club);

		if (SessionManager.getInstance(request).hasAuthority(club) == false) {
			return MessagePage.error("수행권한이 없습니다.", "/web/staff/");
		}
		
		return mav;
	}

	@RequestMapping(value = "/{clubId}/modify", method = RequestMethod.POST)
	public String modifyClub(ClubCmd command) throws IOException {
		//
		// 기존에 등록된 이미지 
		Club club = clubService.getClub(null, command.getId());
		String prevThumbnailId = club.getThumbnailImageId();
		String prevBannerId = club.getBannerImageId();
		
		club = command.createDomain(club);
		
		MultipartFile thumbnailImage = command.getThumbnailImage();
		if (thumbnailImage.getSize() > 0) {
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevThumbnailId)) {
				fileRepository.remove(prevThumbnailId);
			}
			
			InputStream thumbnailStream = null;
			
			if(thumbnailImage.getSize() > 500 * 1024) {
				thumbnailStream = ImageUtil.resize(thumbnailImage, 1920);
			} else {
				thumbnailStream = thumbnailImage.getInputStream();
			}
			
			FileInfo thumbnailFile = fileRepository.store(
//					thumbnailImage.getInputStream(), 
					thumbnailStream,
					thumbnailImage.getOriginalFilename(), 
					thumbnailImage.getContentType());
			club.setThumbnailImageId(thumbnailFile.getId());
		} else {
			// 업로드된 이미지가 없으면 이전 이미지 유지
			club.setThumbnailImageId(prevThumbnailId);
		}
		
		MultipartFile bannerImage = command.getBannerImage();
		if (bannerImage.getSize() > 0) {
			// 이전 이미지 삭제
			if (!StringUtils.isEmpty(prevBannerId)) {
				fileRepository.remove(prevBannerId);
			}
			
			InputStream bannerImageStream = null;
			
			if(bannerImage.getSize() > 500 * 1024) {
				bannerImageStream = ImageUtil.resize(bannerImage, 1920);
			} else {
				bannerImageStream = bannerImage.getInputStream();
			}
			FileInfo bannerFile = fileRepository.store(
//					bannerImage.getInputStream(), 
					bannerImageStream,
					bannerImage.getOriginalFilename(), 
					bannerImage.getContentType());
			club.setBannerImageId(bannerFile.getId());
		} else {
			// 업로드된 이미지가 없으면 이전 이미지 유지
			club.setBannerImageId(prevBannerId);
		}

		clubService.modifyClub(club);
		return "redirect:/web/club/";
	}	
	
	@LoginRequired(false)
	@RequestMapping(value = "/{clubId}/thumbnail", method = RequestMethod.GET)
	public void getSmallImage(@PathVariable("clubId") String clubId, HttpServletResponse response) throws IOException {
		//
		Club club = clubService.getClub(null, clubId);
		if (!StringUtils.isEmpty(club.getThumbnailImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(club.getThumbnailImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(club.getThumbnailImageId(), response.getOutputStream());
		}
	}
	
	@LoginRequired(false)
	@RequestMapping(value = "/{clubId}/banner", method = RequestMethod.GET)
	public void getLargeImage(@PathVariable("clubId") String clubId, HttpServletResponse response) throws IOException {
		//
		Club club = clubService.getClub(null, clubId);
		if (!StringUtils.isEmpty(club.getBannerImageId())) {
			FileInfo fileInfo = fileRepository.getFileInfo(club.getBannerImageId());
			
			response.setContentType(fileInfo.getContentType());
			fileRepository.writeTo(club.getBannerImageId(), response.getOutputStream());
		}
	}
}
