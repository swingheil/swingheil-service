package com.swingheil.web.controller.command;

import org.springframework.web.multipart.MultipartFile;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.type.enums.BarType;
import com.swingheil.domain.type.place.Address;

public class BarCmd {
	//
	private String id;
	private String barType;
	private String nameKor;
	private String nameEng;
	private String homePage;
	
	private String district;
	private String roadAddress;
	private String landAddress;
	
	private MultipartFile thumbnailImage;
	private MultipartFile bannerImage;
	
	//--------------------------------------------------------------------------
	
	public Bar createDomain() {
		Bar bar = new Bar();
		bar.setId(id);
		bar.setType(BarType.findBy(barType));
		bar.setNameKor(nameKor);
		bar.setNameEng(nameEng);
		bar.setHomePage(homePage);
		
		Address address = new Address(district, roadAddress, landAddress);
		bar.setAddress(address);
		
		return bar;
	}
	
	public Bar createDomain(Bar prevBar) {
		//
		Bar bar = prevBar;
		bar.setId(id);
		bar.setType(BarType.findBy(barType));
		bar.setNameKor(nameKor);
		bar.setNameEng(nameEng);
		bar.setHomePage(homePage);
		bar.setAdmin(prevBar.getAdmin());
		bar.setStaffs(prevBar.getStaffs());
		
		Address address = new Address(district, roadAddress, landAddress);
		bar.setAddress(address);

		return bar;
	}

	//--------------------------------------------------------------------------
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getBarType() {
		return barType;
	}

	public void setBarType(String barType) {
		this.barType = barType;
	}

	public String getNameKor() {
		return nameKor;
	}

	public void setNameKor(String nameKor) {
		this.nameKor = nameKor;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getRoadAddress() {
		return roadAddress;
	}

	public void setRoadAddress(String roadAddress) {
		this.roadAddress = roadAddress;
	}

	public String getLandAddress() {
		return landAddress;
	}

	public void setLandAddress(String landAddress) {
		this.landAddress = landAddress;
	}

	public MultipartFile getThumbnailImage() {
		return thumbnailImage;
	}

	public void setThumbnailImage(MultipartFile thumbnailImage) {
		this.thumbnailImage = thumbnailImage;
	}

	public MultipartFile getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(MultipartFile bannerImage) {
		this.bannerImage = bannerImage;
	}
}
