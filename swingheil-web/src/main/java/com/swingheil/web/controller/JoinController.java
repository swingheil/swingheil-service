package com.swingheil.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.swingheil.comp.process.BarService;
import com.swingheil.comp.process.ClubService;
import com.swingheil.comp.process.UserService;
import com.swingheil.domain.shared.ResponseMessage;
import com.swingheil.domain.user.SocialUser;

@Controller
@RequestMapping(value = "/request")
public class JoinController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private BarService barService;
	
	@Autowired
	private ClubService clubService;
	
	@RequestMapping(value = "")
	public String home(){
		return "user/request";
	}
	
	@RequestMapping(value = "/confirm", method = RequestMethod.POST)
	public ResponseMessage request(
			@RequestParam(value = "nickname") String nickname,
			@RequestParam(value = "password") String password,
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "type", required = true) String type,
			@RequestParam(value = "id", required = true) String id){
		
		ResponseMessage message = null;
		
		try {
			
			SocialUser user = userService.registerUser(nickname, null);
			
			userService.updateUserDetail(user.getId(), email, password, phone);
			
			if(type.equals("bar")){
				barService.joinAsStaff(id, user.getId());
			} else if(type.equals("club")) {
				clubService.joinAsStaff(id, user.getId());
			} else {
				throw new RuntimeException("유효하지 않는 타입입니다");
			}
			
			message = new ResponseMessage(true, "요청되었습니다");
		} catch(Exception ex){
			message = new ResponseMessage(false, ex.getMessage());
		}
		
		return message;
	}
}
