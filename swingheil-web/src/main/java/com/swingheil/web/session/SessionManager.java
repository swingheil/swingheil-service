package com.swingheil.web.session;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.swingheil.comp.process.UserService;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.user.SocialUser;

public class SessionManager {
	//
	private static final String LOGIN_USER = "loginUser";
	private static final String IS_SUPER_ADMIN = "isSuperAdmin";
	
	// 슈퍼관리자 정보 
	private static final String SUPER_ADMIN_EMAIL = "app@swingheil.com";
	private static final String SUPER_ADMIN_PASSWORD = "swingheil1";
	
	
	private HttpSession session;
	
	private SessionManager(HttpServletRequest req) {
		//
		this.session = req.getSession();
	}
	
	//--------------------------------------------------------------------------
	
	public static SessionManager getInstance(HttpServletRequest req) {
		//
		return new SessionManager(req);
	}

	public boolean isLogin() {
		//
		return (session.getAttribute(LOGIN_USER) != null) ? true : false;
	}
	
	public boolean login(String email, String password) {
		//
		WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
		UserService userService = context.getBean(UserService.class);

		if (SUPER_ADMIN_EMAIL.equals(email) && SUPER_ADMIN_PASSWORD.equals(password)) {
			SocialUser adminUser = new SocialUser("Admin");
			adminUser.setId("admin");
			session.setAttribute(LOGIN_USER, adminUser);
			session.setAttribute(IS_SUPER_ADMIN, true);
			return true;
		}
		
		String userId = userService.getUserIdByEmailAndPassword(email, password);
		if (userId != null) {
			SocialUser loginUser = userService.getUserByUserid(userId);
			session.setAttribute(LOGIN_USER, loginUser);
			return true;
		} else {
			session.invalidate();
			return false;
		}
	}
	
	public void logout() {
		//
		session.invalidate();
	}

	public String getLoginEmail() {
		// 
		if (isLogin()) {
			SocialUser loginTowner = (SocialUser) session.getAttribute(LOGIN_USER);
			return loginTowner.getEmail();
		}
		return null;
	}
	
	public String getLoginId() {
		// 
		if (isLogin()) {
			SocialUser loginUser = (SocialUser) session.getAttribute(LOGIN_USER);
			return loginUser.getId();
		}
		return null;
	}

	public boolean isSuperAdmin() {
		//
		
		if (session != null && session.getAttribute(IS_SUPER_ADMIN) != null) {
			return ((Boolean) session.getAttribute(IS_SUPER_ADMIN)).booleanValue();
		}
		return false;
	}

	public boolean hasAuthority(Group group) {
		//
		boolean authority = false;
		if (group != null && group.getStaffs() != null) {
			
			List<Staff> staffs = group.getStaffs();
			for (Staff staff : staffs) {
				if (getLoginId().equals(staff.getId())) {
					authority = true;
					break;
				}
			}
		}
		return authority;
	}
}
