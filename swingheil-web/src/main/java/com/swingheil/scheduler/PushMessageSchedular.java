package com.swingheil.scheduler;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.swingheil.comp.entity.PushEntity;
import com.swingheil.comp.process.NotificationService;
import com.swingheil.domain.notification.Push;

@Service
public class PushMessageSchedular {

	@Autowired
	NotificationService notiService;
	
	@Autowired
	PushEntity pushEntity;
	
	@Scheduled(fixedDelay = 1000 * 60)
	public void sendPushMessage(){
		List<Push> pushes = pushEntity.retrieveAllByState();
		
		for(Push push : pushes) {
			try {
				notiService.sendPushMessage("", push.getMessage(), push.getPushType(), push.getTag1(), push.getDeviceId());
				
				push.setState("Success");
				
			} catch(Exception ex) {
				push.setState("Fail");
				push.setErrorMsg(ex.getMessage());
			}
			
			push.setAttempt(new Date());
			
			pushEntity.update(push);
		}
	}
}
