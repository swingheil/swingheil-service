package com.swingheil.comp.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.swingheil.comp.shared.BaseMongoTestCase;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.DateTime;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.place.Address;

@UsingDataSet(locations = "/dataset/events.json", loadStrategy = LoadStrategyEnum.CLEAN_INSERT)
public class EventEntityTest extends BaseMongoTestCase {

	@Autowired
	private EventEntity eventEntity;
	
	@Test
	public void testCreate() {
		//
		Event event = new Event();
		event.setCategory(new CodeName("02", "행사구분2"));
		
		Group sponsor = new Group();
		sponsor.setId("1");
		sponsor.setNameKor("스윙에이드");
		event.setSponsor(sponsor);
		event.setTitle("테스트 행사");
		event.setSummary("테스트 행사 요약");
		
		DateTime from = new DateTime();
		from.setDate("2014-05-27");
		from.setTime("10:00");
		DateTime to = new DateTime();
		to.setDate("2014-05-27");
		to.setTime("12:00:00");
		Period period = new Period(from, to);
		event.setPeriod(period);
		Address place = new Address();
		place.setDistrict("서울 강남구");
		place.setRoad("도로명 주소");
		place.setLand("지명 주소");
		event.setPlace(place);
		event.setDetail("상세 설명");

		String eventId = eventEntity.create(event);
		
		event = eventEntity.retrieve(eventId);
		assertThat(event.getTitle(), is("테스트 행사"));
		assertThat(event.getSummary(), is("테스트 행사 요약"));
	}

	@Test
	public void testRead() {
		//
		Event event = eventEntity.retrieve("1");
		
		assertThat(event.getCategory().getCode(), is("02"));
		assertThat(event.getTitle(), is("스윙에이드 졸업공연"));
		assertThat(event.getSummary(), is("졸업공연에 여러분을 모십니다."));
	}
	
	@Test
	public void testRetriveAllByCategory() {
		//
		// 테스트 데이터 (등록순)
		// 1. 스윙에이드 졸업공연
		// 2. 스윙에이드 졸업공연2
		// 3. 스윙에이드 졸업공연3

		Page<Event> page = eventEntity.retriveAllByCategory("02", new PageInfo(1, 2));
		assertTrue(page.hasNext());
		assertThat(page.getResults().size(), is(2));
		assertThat(page.getResults().get(0).getTitle(), is("스윙에이드 졸업공연3"));
	}

	@Test
	public void testUpdate() {
		//
		Event event = eventEntity.retrieve("1");
		event.setTitle("수정 행사명");
		
		eventEntity.update(event);
		
		event = eventEntity.retrieve("1");
		assertThat(event.getTitle(), is("수정 행사명"));
	}

	@Test
	public void testDelete() {
		//
		eventEntity.delete("1");
		
		assertNull(eventEntity.retrieve("1"));
	}
	
	@Test
	public void testRetrieveAll() {
		//
		// 테스트 데이터 (등록순)
		// 1. 스윙에이드 졸업공연
		// 2. 스윙에이드 졸업공연2
		// 3. 스윙에이드 졸업공연3

		Page<Event> page = eventEntity.retriveAll(new PageInfo(1, 2));
		assertTrue(page.hasNext());
		assertThat(page.getResults().size(), is(2));
		assertThat(page.getResults().get(0).getTitle(), is("스윙에이드 졸업공연3"));
		
		page = eventEntity.retriveAll(new PageInfo(2, 1));
		assertTrue(page.hasNext());
		assertThat(page.getResults().size(), is(1));
		assertThat(page.getResults().get(0).getTitle(), is("스윙에이드 졸업공연2"));
		
		page = eventEntity.retriveAll(new PageInfo(2, 2));
		assertFalse(page.hasNext());
		assertThat(page.getResults().size(), is(1));
		assertThat(page.getResults().get(0).getTitle(), is("스윙에이드 졸업공연"));
	}

}
