package com.swingheil.comp.process;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.swingheil.comp.shared.BaseMongoTestCase;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@UsingDataSet(locations = "/dataset/users.json", loadStrategy = LoadStrategyEnum.CLEAN_INSERT)
public class UserServiceTest extends BaseMongoTestCase {

	@Autowired
	private UserService userService;
	
	@Test
	public void testRegisterUser() {
		//
		String nickname = "hong";
		Device device = new Device("a", "b", "c", "d");
		
		SocialUser socialUser = userService.registerUser(nickname, device);
		
		assertThat(socialUser.getNickname(), is("hong"));
		device = socialUser.getDevices().get(0);
		
		assertThat(device.getDeviceUid(), is("a"));
		assertThat(device.getModelId(), is("b"));
		assertThat(device.getOsVersion(), is("c"));
		assertThat(device.getRegistrationId(), is("d"));
	}
	
	@Test
	public void testGetAllUsers() {
		List<SocialUser> users = userService.getAllUsers();
		assertThat(users.size(), is(1));
	}
	
	@Test
	public void testGetUserDeviceList() {
		//
		String userId = "1";
		List<Device> devices = userService.getUserDeviceList(userId);
		
		assertThat(devices.size(), is(2));
		
		Device device = devices.get(0);
		
		assertThat(device.getDeviceUid(), is("deviceUid"));
		assertThat(device.getModelId(), is("modelId"));
		assertThat(device.getOsVersion(), is("osVersion"));
		assertThat(device.getRegistrationId(), is("regId"));
	}
	
	@Test
	public void testAddUserDevice() {
		//
		String userId = "1";
		Device device = new Device("a", "b", "c", "d");
		
		userService.addUserDevice(userId, device);
		
		List<Device> devices = userService.getUserDeviceList(userId);
		
		assertThat(devices.size(), is(3));
		
		device = devices.get(2);
		
		assertThat(device.getDeviceUid(), is("a"));
		assertThat(device.getModelId(), is("b"));
		assertThat(device.getOsVersion(), is("c"));
		assertThat(device.getRegistrationId(), is("d"));
	}
	
	@Test
	public void testRemoveUserDevice() {
		//
		String userId = "1";
		String deviceUid = "deviceUid";
		
		userService.removeUserDevice(userId, deviceUid);
		
		List<Device> devices = userService.getUserDeviceList(userId);
		assertThat(devices.size(), is(1));
	}
	
	
	@Test
	public void testGetUsersByName() {
		//
		String nickname = "kimgisa";
		
		List<SocialUser> users = userService.getUsersByNickname(nickname);
		assertThat(users.get(0).getNickname(), is("kimgisa"));
	}
	
	@Test
	public void testGetUsersByPhoneNo() {
		//
		String phoneNo = "010-1234-1234";
		
		List<SocialUser> users = userService.getUsersByPhoneNo(phoneNo);
		assertThat(users.get(0).getNickname(), is("kimgisa"));
		assertThat(users.get(0).getPhone(), is("010-1234-1234"));
	}
	
	@Test
	public void testGetUserByDeviceUid() {
		//
		String deviceUid = "notExistUser120938130981";
		SocialUser socialUser = userService.getUserByDeviceUid(deviceUid);
		assertNull(socialUser);
		
		deviceUid = "deviceUid2";
		socialUser = userService.getUserByDeviceUid(deviceUid);
		assertThat(socialUser.getEmail(), is("test@a.com"));
		assertThat(socialUser.getNickname(), is("kimgisa"));
		
		// 중요한 개인정보가 null인지 체크
		assertNull(socialUser.getPhone());
		assertNull(socialUser.getPassword());
		
	}
	
	@Test
	public void testLoginAsUser() {
		assertNotNull(userService.getUserIdByEmailAndPassword("test@a.com", "1234"));
		assertNull(userService.getUserIdByEmailAndPassword("aaaaaa@a.com", "1234"));
		assertNull(userService.getUserIdByEmailAndPassword("test@a.com", "bsadfasd"));
	}
}
