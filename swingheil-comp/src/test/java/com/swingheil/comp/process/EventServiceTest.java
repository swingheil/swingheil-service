package com.swingheil.comp.process;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.swingheil.comp.shared.BaseMongoTestCase;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.DateTime;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.place.Address;

@UsingDataSet(locations = {"/dataset/bars.json", "/dataset/events.json", "/dataset/users.json"}, 
	loadStrategy = LoadStrategyEnum.CLEAN_INSERT)
public class EventServiceTest extends BaseMongoTestCase {
	//
	@Autowired
	private EventService eventService;
	
	@Autowired
	private BarService barService;
	
	@Test
	public void testRegisterEvent() {
		//
		Event event = new Event();
		event.setCategory(new CodeName("02", "행사구분2"));
		Group sponsor = new Group();
		sponsor.setId("1");
		sponsor.setNameKor("스윙에이드");
		event.setSponsor(sponsor);
		event.setTitle("테스트 행사");
		event.setSummary("테스트 행사 요약");
		
		DateTime from = new DateTime();
		from.setDate("2014-05-27");
		from.setTime("10:00:00");
		DateTime to = new DateTime();
		to.setDate("2014-05-27");
		to.setTime("12:00:00");
		Period period = new Period(from, to);
		event.setPeriod(period);
		Address place = new Address();
		place.setDistrict("서울 강남구");
		place.setRoad("도로명 주소");
		place.setLand("지명 주소");
		event.setPlace(place);
		event.setDetail("상세 설명");

		String eventId = eventService.registerEvent(event);
		
		event = eventService.getEvent("1", eventId);
		assertThat(event.getTitle(), is("테스트 행사"));
		assertThat(event.getSummary(), is("테스트 행사 요약"));
		
		assertThat(event.getSponsor().getNameKor(), is("스윙에이드"));

		Bar bar = barService.getBar(null, "1");
		assertThat(event.getRegDate(), is(bar.getLastUpdateDate()));
	}
	
	@Test
	public void testGetEvent() {
		//
		Event event = eventService.getEvent("1", "1");
		
		assertThat(event.getCategory().getCode(), is("02"));
		assertThat(event.getTitle(), is("스윙에이드 졸업공연"));
		assertThat(event.getSummary(), is("졸업공연에 여러분을 모십니다."));
		assertTrue(event.isFavorite());
	}
	
	@Test
	public void testGetEvents() {
		String userId = "1";
		PageInfo pageInfo = new PageInfo(1, 5);
		
		Page<Event> page = eventService.getEvents(userId, pageInfo);
		assertThat(page.getResults().size(), is(3));
	}
	
	@Test
	public void testGetEventsByBarId() {
		//
		String barId = "2";
		String userId = "1"; // kimgisa
		PageInfo pageInfo = new PageInfo(1, 5);
		
		Page<Event> page = eventService.getEventsByBarId(userId, barId, pageInfo);
		System.out.println(page.getResults().size());
	}
	
	@Test
	public void testUpdateEvent() {
		//
		Event event = eventService.getEvent("1", "1");
		event.setTitle("수정 행사명");
		
		eventService.modifyEvent(event);
		
		event = eventService.getEvent("1", "1");
		assertThat(event.getTitle(), is("수정 행사명"));
	}
	
	@Test
	public void testDropEvent() {
		//
		eventService.removeEvent("1");
		assertNull(eventService.getEvent("1", "1"));
	}
	
}
