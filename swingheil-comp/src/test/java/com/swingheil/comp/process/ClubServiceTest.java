package com.swingheil.comp.process;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.lordofthejars.nosqlunit.annotation.UsingDataSet;
import com.lordofthejars.nosqlunit.core.LoadStrategyEnum;
import com.swingheil.comp.shared.BaseMongoTestCase;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.type.enums.StaffState;

public class ClubServiceTest extends BaseMongoTestCase {

	@Autowired
	private ClubService clubService;
	
	@Test
	public void testActivateStaff() {
		
		Club club = clubService.getClub("10", "30");
		assertEquals(StaffState.Requested, club.getStaffs().get(0).getState());
		
		clubService.activateAsStaff("30", "10");
		
		club = clubService.getClub("10", "30");
		assertEquals(StaffState.Active, club.getStaffs().get(0).getState());
	}
}
