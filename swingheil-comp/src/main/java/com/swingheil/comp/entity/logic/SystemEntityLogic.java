package com.swingheil.comp.entity.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.SystemEntity;
import com.swingheil.comp.entity.repo.SysEnvRepository;
import com.swingheil.domain.env.SystemEnvironment;

@Repository
public class SystemEntityLogic implements SystemEntity {
	//
	@Autowired
	private SysEnvRepository repository;
	
	@Override
	public SystemEnvironment retrieve() {
		// 
		return repository.findOne("swingheil").createDomain();
	}

}
