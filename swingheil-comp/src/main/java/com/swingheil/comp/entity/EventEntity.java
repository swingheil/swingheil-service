package com.swingheil.comp.entity;

import java.util.List;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface EventEntity {
	//
	String create(Event event);
	
	Event retrieve(String eventId);
	Page<Event> retriveAllByCategory(String category, PageInfo pageInfo);
	Page<Event> retriveAll(PageInfo pageInfo);
	Page<Event> retriveAllByClubId(String clubId, PageInfo pageInfo);
	Page<Event> retriveAllByBarId(String barId, PageInfo pageInfo);
	
	Page<Event> retriveAllBySponsorIds(List<String> groupIds, PageInfo pageInfo);

	void update(Event event);
	void delete(String eventId);
	
}
