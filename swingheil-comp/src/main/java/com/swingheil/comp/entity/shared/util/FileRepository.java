package com.swingheil.comp.entity.shared.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Component;

import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSFile;

@Component
public class FileRepository {
	
	@Autowired
	private GridFsOperations gridOperations;
	
	public FileInfo store(InputStream inputStream, String fileName, String contentType) {
		//
		GridFSFile file = gridOperations.store(inputStream, fileName, contentType);
		
		FileInfo fileInfo = new FileInfo();
		fileInfo.setId(file.getId().toString());
		fileInfo.setName(fileName);
		fileInfo.setContentType(contentType);
		
		return fileInfo;
	}
	
	public FileInfo getFileInfo(String fileId) {
		//
		Query query = new Query(Criteria.where("_id").is(new ObjectId(fileId)));
		GridFSDBFile file = gridOperations.findOne(query);
		
		FileInfo fileInfo = new FileInfo();
		fileInfo.setId(fileId);
		fileInfo.setName(file.getFilename());
		fileInfo.setContentType(file.getContentType());
		
		return fileInfo;
	}
	
	public void remove(String fileId) {
		//
		Query query = new Query(Criteria.where("_id").is(new ObjectId(fileId)));
		gridOperations.delete(query);
	}
	
	public void writeTo(String fileId, OutputStream outputStream) throws IOException {
		//
		Query query = new Query(Criteria.where("_id").is(new ObjectId(fileId)));
		GridFSDBFile file = gridOperations.findOne(query);
		
		file.writeTo(outputStream);
	}
}
