package com.swingheil.comp.entity.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.EventDoc;

public interface EventRepository extends MongoRepository<EventDoc, String> {
	//
	Page<EventDoc> findAllBySponsorIdIn(List<String> sponsorIds, Pageable pageable);
	Page<EventDoc> findAllBySponsorIdInAndVisible(List<String> sponsorIds, boolean visible, Pageable pageable); 
	
	Page<EventDoc> findAllByCategoryCode(String category, Pageable pageable);
	Page<EventDoc> findAllByCategoryCodeAndVisible(String category, boolean visible, Pageable pageable);
	
	Page<EventDoc> findAllBySponsorIdAndCategoryCode(String groupId, String code, Pageable pageable);
	Page<EventDoc> findAllBySponsorIdAndCategoryCodeAndVisible(String barId, String code, boolean visible, Pageable pageable);
}
