package com.swingheil.comp.entity.repo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

import com.mongodb.BasicDBObject;
import com.swingheil.comp.entity.doc.UserDoc;
import com.swingheil.comp.entity.doc.UserDoc.DeviceDoc;
import com.swingheil.comp.entity.repo.UserRepositoryCustom;
import com.swingheil.domain.user.SocialUser;

public class UserRepositoryImpl extends SimpleMongoRepository<UserDoc, String> implements UserRepositoryCustom {
	//
	@Autowired
	public UserRepositoryImpl(MongoRepositoryFactory factory, MongoTemplate template) {
		super(factory.<UserDoc, String>getEntityInformation(UserDoc.class), template);
	}

	@Override
	public void addFavoriteEvent(String userId, String eventId) {
		// 
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.addToSet("favoriteEvents", eventId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}

	@Override
	public void removeFavoriteEvent(String userId, String eventId) {
		//
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.pull("favoriteEvents", eventId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}
	
	@Override
	public void addFavoriteClub(String userId, String clubId) {
		// 
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.addToSet("favoriteClubs", clubId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}
	
	@Override
	public void removeFavoriteClub(String userId, String clubId) {
		//
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.pull("favoriteClubs", clubId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}
	
	@Override
	public void addFavoriteBar(String userId, String barId) {
		// 
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.addToSet("favoriteBars", barId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}
	
	@Override
	public void removeFavoriteBar(String userId, String barId) {
		//
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.pull("favoriteBars", barId);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}

	@Override
	public void addUserDevice(String userId, DeviceDoc device) {
		// 
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.push("devices", device);
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}

	@Override
	public void removeUserDevice(String userId, String deviceUid) {
		// 
		Query query = new Query(Criteria.where("_id").is(userId));
		Update update = new Update();
		update.pull("devices", new BasicDBObject("deviceUid", deviceUid));
		
		getMongoOperations().updateFirst(query, update, UserDoc.class);
	}

	@Override
	public SocialUser findByDeviceUid(String deviceUid) {
		//
		Query query = new Query(Criteria.where("devices").elemMatch(Criteria.where("deviceUid").is(deviceUid)));
		
		UserDoc userDoc = getMongoOperations().findOne(query, UserDoc.class);
		return (userDoc != null) ? userDoc.createDomain() : null;
	}

	@Override
	public List<SocialUser> findByBar(String barId) {
		//
		Query query = new Query(Criteria.where("favoriteBars").is(barId));
		
		List<UserDoc> users = getMongoOperations().find(query, UserDoc.class);
		
		return (users != null) ? UserDoc.createDomains(users) : null;
	}

	@Override
	public List<SocialUser> findByClub(String clubId) {
		//
		Query query = new Query(Criteria.where("favoriteClubs").is(clubId));
		
		List<UserDoc> users = getMongoOperations().find(query, UserDoc.class);
		
		return (users != null) ? UserDoc.createDomains(users) : null;
	}
}
