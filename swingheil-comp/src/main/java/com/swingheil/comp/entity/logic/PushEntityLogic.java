package com.swingheil.comp.entity.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.PushEntity;
import com.swingheil.comp.entity.doc.PushDoc;
import com.swingheil.comp.entity.repo.PushRepository;
import com.swingheil.comp.entity.shared.util.IdGenerator;
import com.swingheil.domain.notification.Push;

@Repository
public class PushEntityLogic implements PushEntity {

	@Autowired
	private PushRepository repository;
	
	@Autowired
	private IdGenerator idGenerator;
	
	
	@Override
	public String create(Push push) {
		//
		push.setId(idGenerator.generate("push"));
		PushDoc doc = new PushDoc(push);
		repository.save(doc);
		
		return doc.getId();

	}

	@Override
	public Push retrieve(String pushId) {
		// 
		PushDoc doc = repository.findOne(pushId);
		if (doc != null) {
			return doc.createDomain();
		}
		return null;
	}

	@Override
	public void update(Push push) {
		//
		PushDoc doc = new PushDoc(push);
		repository.save(doc);
	}

	@Override
	public void delete(String pushId) {
		// 
		repository.delete(pushId);
	}

	@Override
	public List<Push> retrieveAllByState() {
		//
		return repository.findAllByState("Created");
	}

}
