package com.swingheil.comp.entity.doc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.type.enums.UserState;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@Document(collection = "users")
public class UserDoc {
	//
	@Id
	private String id;
	private String nickname;
	private String name;
	private String email;
	private String phone;
	private String password;
	private UserState state;
	
	private List<DeviceDoc> devices;
	private List<String> favoriteEvents;
	private List<String> favoriteClubs;
	private List<String> favoriteBars;
	
	//--------------------------------------------------------------------------
	
	public UserDoc() {
		//
	}
	
	public UserDoc(SocialUser user) {
		//
		this.id = user.getId();
		this.nickname = user.getNickname();
		this.name = user.getName();
		this.email = user.getEmail();
		this.phone = user.getPhone();
		this.password = user.getPassword();
		this.state = user.getState();
		
		if (user.getDevices() != null) {
			//
			devices = new ArrayList<UserDoc.DeviceDoc>();
			for (Device device : user.getDevices()) {
				devices.add(new DeviceDoc(device));
			}
		}
	}
	
	//--------------------------------------------------------------------------
	
	public static List<SocialUser> createDomains(List<UserDoc> docs) {
		// 
		if (docs != null) {
			List<SocialUser> users = new ArrayList<SocialUser>();
			for (UserDoc doc : docs) {
				users.add(doc.createDomain());
			}
			return users;
		}
		return null;
	}
	
	public SocialUser createDomain() {
		//
		SocialUser user = new SocialUser(nickname);
		user.setId(id);
		user.setName(name);
		user.setEmail(email);
		user.setPhone(phone);
		user.setPassword(password);
		user.setState(state);
		
		if (devices != null) {
			//
			for (DeviceDoc deviceDoc : devices) {
				user.addDevice(deviceDoc.createDomain());
			}
		}
		
		return user;
	}

	//--------------------------------------------------------------------------
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<DeviceDoc> getDevices() {
		return devices;
	}	
	public void setDevices(List<DeviceDoc> devices) {
		this.devices = devices;
	}
	public List<String> getFavoriteEvents() {
		return favoriteEvents;
	}
	public void setFavoriteEvents(List<String> favoriteEvents) {
		this.favoriteEvents = favoriteEvents;
	}
	public List<String> getFavoriteClubs() {
		return favoriteClubs;
	}
	public void setFavoriteClubs(List<String> favoriteClubs) {
		this.favoriteClubs = favoriteClubs;
	}
	public List<String> getFavoriteBars() {
		return favoriteBars;
	}
	public void setFavoriteBars(List<String> favoriteBars) {
		this.favoriteBars = favoriteBars;
	}

	//--------------------------------------------------------------------------

	public static class DeviceDoc {
		//
		private String deviceUid;
		private String modelId;
		private String osVersion;
		private String registrationId;
		
		//----------------------------------------------------------------------

		public DeviceDoc() {
			//
		}
		
		public DeviceDoc(Device device) {
			//
			this.deviceUid = device.getDeviceUid();
			this.modelId = device.getModelId();
			this.osVersion = device.getOsVersion();
			this.registrationId = device.getRegistrationId();
		}
		
		//----------------------------------------------------------------------
		
		public Device createDomain() {
			//
			return new Device(deviceUid, modelId, osVersion, registrationId);
		}
		
		//----------------------------------------------------------------------
		
		public String getDeviceUid() {
			return deviceUid;
		}
		public void setDeviceUid(String deviceUid) {
			this.deviceUid = deviceUid;
		}
		public String getModelId() {
			return modelId;
		}
		public void setModelId(String modelId) {
			this.modelId = modelId;
		}
		public String getOsVersion() {
			return osVersion;
		}
		public void setOsVersion(String osVersion) {
			this.osVersion = osVersion;
		}
		public String getRegistrationId() {
			return registrationId;
		}
		public void setRegistrationId(String registrationId) {
			this.registrationId = registrationId;
		}
	}

	public UserState getState() {
		return state;
	}

	public void setState(UserState state) {
		this.state = state;
	}
	
}
