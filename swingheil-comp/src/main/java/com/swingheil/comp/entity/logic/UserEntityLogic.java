package com.swingheil.comp.entity.logic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.UserEntity;
import com.swingheil.comp.entity.doc.UserDoc;
import com.swingheil.comp.entity.doc.UserDoc.DeviceDoc;
import com.swingheil.comp.entity.repo.UserRepository;
import com.swingheil.comp.entity.shared.util.IdGenerator;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@Repository
public class UserEntityLogic implements UserEntity {

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private IdGenerator idGenerator;
	
	@Override
	public String create(SocialUser user) {
		//
		user.setId(idGenerator.generate("user"));
		repository.save(new UserDoc(user));
		
		return user.getId();
	}

	@Override
	public SocialUser retrieve(String userId) {
		// 
		UserDoc userDoc = repository.findOne(userId);
		if (userDoc != null) {
			return userDoc.createDomain();
		}
		return null;
	}
	
	@Override
	public List<SocialUser> retrieveAll() {
		// 
		List<UserDoc> userDocs = repository.findAll();
		return UserDoc.createDomains(userDocs);
	}

	@Override
	public List<SocialUser> retrieveByNickname(String nickname) {
		// 
		return repository.findByNickname(nickname);
	}
	
	@Override
	public List<SocialUser> retrieveByPhoneNo(String phoneNo) {
		// 
		return repository.findByPhone(phoneNo);
	}

	@Override
	public List<SocialUser> retrieveByEmail(String email) {
		// 
		return repository.findByEmail(email);
	}

	@Override
	public void addFavoriteEvent(String userId, String eventId) {
		// 
		repository.addFavoriteEvent(userId, eventId);
	}

	@Override
	public void removeFavoriteEvent(String userId, String eventId) {
		// 
		repository.removeFavoriteEvent(userId, eventId);
	}
	
	@Override
	public List<String> retriveFavoriteEvents(String userId) {
		// 
		UserDoc user = repository.findOne(userId);
		return (user != null) ? user.getFavoriteEvents() : new ArrayList<String>();
	}
	
	@Override
	public List<String> retriveFavoriteClubs(String userId) {
		// 
		UserDoc user = repository.findOne(userId);
		return (user != null) ? user.getFavoriteClubs() : new ArrayList<String>();
	}
	
	@Override
	public List<String> retriveFavoriteBars(String userId) {
		// 
		UserDoc user = repository.findOne(userId);
		return (user != null) ? user.getFavoriteBars() : new ArrayList<String>();
	}

	@Override
	public void addDevice(String userId, Device device) {
		// 
		repository.addUserDevice(userId, new DeviceDoc(device));
	}

	@Override
	public void removeDevice(String userId, String deviceUid) {
		// 
		repository.removeUserDevice(userId, deviceUid);
	}

	@Override
	public void addFavoriteClub(String userId, String clubId) {
		// 
		repository.addFavoriteClub(userId, clubId);
	}

	@Override
	public void removeFavoriteClub(String userId, String clubId) {
		//
		repository.removeFavoriteClub(userId, clubId);
	}
	@Override
	public void addFavoriteBar(String userId, String barId) {
		// 
		repository.addFavoriteBar(userId, barId);
	}
	
	@Override
	public void removeFavoriteBar(String userId, String barId) {
		//
		repository.removeFavoriteBar(userId, barId);
	}

	@Override
	public SocialUser retrieveByDeviceUid(String deviceUid) {
		//
		return repository.findByDeviceUid(deviceUid);
	}

	@Override
	public void update(SocialUser user) {
		//
		UserDoc userDoc = new UserDoc(user);
		repository.save(userDoc);
	}

	@Override
	public List<SocialUser> retrieveByBar(String barId) {
		//
		return repository.findByBar(barId);
	}

	@Override
	public List<SocialUser> retrieveByClub(String clubId) {
		//
		return repository.findByClub(clubId);
	}
}
