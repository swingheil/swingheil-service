package com.swingheil.comp.entity;

import java.util.List;

import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface ClubEntity {
	//
	String create(Club club);
	void update(Club club);

	Club retrieve(String clubId);
	Page<Club> retrieveAll(PageInfo pageInfo);
	List<Club> retriveManagedClubs(String adminId);
	void addStaff(String clubId, Staff staff);
	void removeStaff(String clubId, String staffId);
	
	void activateAsStaff(String clubId, String staffId);
	
	void delegateAdmin(String clubId, Staff staff);
	
	List<Club> retrieveByBar(String barId);
}
