package com.swingheil.comp.entity.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.SysEnvDoc;

public interface SysEnvRepository extends MongoRepository<SysEnvDoc, String> {
	//
}
