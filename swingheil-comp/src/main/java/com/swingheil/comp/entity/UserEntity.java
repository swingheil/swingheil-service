package com.swingheil.comp.entity;

import java.util.List;

import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

public interface UserEntity {
	//
	String create(SocialUser user);
	SocialUser retrieve(String userId);
	List<SocialUser> retrieveAll();
	
	List<SocialUser> retrieveByNickname(String nickname);
	List<SocialUser> retrieveByPhoneNo(String phoneNo);
	List<SocialUser> retrieveByEmail(String email);
	
	List<SocialUser> retrieveByBar(String barId);
	List<SocialUser> retrieveByClub(String clubId);

	void addDevice(String userId, Device device);
	void removeDevice(String userId, String deviceUid);

	// 즐겨찾기
	void addFavoriteEvent(String userId, String eventId);
	void addFavoriteClub(String userId, String clubId);
	void addFavoriteBar(String userId, String barId);
	List<String> retriveFavoriteEvents(String userId);
	List<String> retriveFavoriteClubs(String userId);
	List<String> retriveFavoriteBars(String userId);
	void removeFavoriteEvent(String userId, String eventId);
	void removeFavoriteClub(String userId, String clubId);
	void removeFavoriteBar(String userId, String barId);
	
	SocialUser retrieveByDeviceUid(String deviceUid);
	void update(SocialUser user);
	
}
