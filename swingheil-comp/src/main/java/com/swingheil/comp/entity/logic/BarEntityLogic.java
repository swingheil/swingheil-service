package com.swingheil.comp.entity.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.BarEntity;
import com.swingheil.comp.entity.doc.BarDoc;
import com.swingheil.comp.entity.doc.StaffDoc;
import com.swingheil.comp.entity.repo.BarRepository;
import com.swingheil.comp.entity.shared.util.IdGenerator;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

@Repository
public class BarEntityLogic implements BarEntity {
	//
	@Autowired
	private BarRepository repository;
	
	@Autowired
	private IdGenerator idGenerator;
	
	@Override
	public String create(Bar bar) {
		// 
		// bar, club 구분 없이 group 키로 채번
		bar.setId(idGenerator.generate("group")); 
		BarDoc doc = new BarDoc(bar);
		repository.save(doc);
		
		return doc.getId();
	}

	@Override
	public void update(Bar bar) {
		// 
		repository.save(new BarDoc(bar));
	}

	@Override
	public Bar retrieve(String barId) {
		//
		BarDoc doc = repository.findOne(barId);
		if (doc != null) {
			return doc.createDomain();
		}
		return null;
	}

	@Override
	public Page<Bar> retrieveAll(PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "lastUpdateDate");
		
		org.springframework.data.domain.Page<BarDoc> resultPage = 
				repository.findAll(pageRequest);
		
		Page<Bar> page = new Page<Bar>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(BarDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}
	@Override
	public void addStaff(String barId, Staff staff) {
		// 
		repository.addStaff(barId, new StaffDoc(staff));
	}

	@Override
	public void removeStaff(String barId, String staffId) {
		// 
		repository.removeStaff(barId, staffId);
	}
	
	@Override
	public void activateAsStaff(String barId, String staffId) {
		//
		
		repository.activateAsStaff(barId, staffId);
	}

	@Override
	public void delegateAdmin(String barId, Staff staff) {
		//
		repository.delegateAdmin(barId, new StaffDoc(staff));
	}

	@Override
	public List<Bar> retriveManagedBars(String adminId) {
		//
		
		Sort sort = new Sort(Sort.Direction.ASC, "nameKor");
		
		List<BarDoc> barDocs = repository.findAllByStaffsId(adminId, sort);

		return BarDoc.createDomains(barDocs);
	}
}
