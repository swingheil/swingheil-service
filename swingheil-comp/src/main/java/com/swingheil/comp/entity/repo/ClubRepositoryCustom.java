package com.swingheil.comp.entity.repo;

import com.swingheil.comp.entity.doc.StaffDoc;

public interface ClubRepositoryCustom {
	
	void addStaff(String clubId, StaffDoc staffDoc);
	void removeStaff(String clubId, String staffId);
	
	void activateAsStaff(String clubId, String staffId);
	
	void delegateAdmin(String clubId, StaffDoc staffDoc);
}
