package com.swingheil.comp.entity.doc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Staff;

@Document(collection = "clubs")
public class ClubDoc {
	//
	@Id
	private String id;
	private String nameKor;
	private String nameEng;
	private String homePage;
	private String barId;
	private StaffDoc admin;
	private List<StaffDoc> staffs;
	private long lastUpdateDate;
	
	// image
	private String thumbnailImageId;
	private String bannerImageId;
	
	//--------------------------------------------------------------------------
	
	public ClubDoc() {
		//
	}
	
	public ClubDoc(Club club) {
		//
		this.id = club.getId();
		this.nameKor = club.getNameKor();
		this.nameEng = club.getNameEng();
		this.homePage = club.getHomePage();
		if (club.getBar() != null) {
			this.barId = club.getBar().getId();
		}
		this.lastUpdateDate = club.getLastUpdateDate().getTime();
		this.thumbnailImageId = club.getThumbnailImageId();
		this.bannerImageId = club.getBannerImageId();
		
		if(club.getAdmin() != null) {
			this.admin = new StaffDoc(club.getAdmin());			
		}
		
		List<StaffDoc> staffs = new ArrayList<StaffDoc>();
		
		for(Staff staff : club.getStaffs()) {
			StaffDoc staffDoc = new StaffDoc(staff);
			staffs.add(staffDoc);
		}
		
		this.staffs = staffs;
	}

	//--------------------------------------------------------------------------
	
	public static List<Club> createDomains(List<ClubDoc> docs) {
		// 
		if (docs != null) {
			List<Club> clubs = new ArrayList<Club>();
			for (ClubDoc doc : docs) {
				clubs.add(doc.createDomain());
			}
			return clubs;
		}
		return null;
	}
	
	public Club createDomain() {
		//
		Club club = new Club();
		club.setId(id);
		club.setNameKor(nameKor);
		club.setNameEng(nameEng);
		club.setHomePage(homePage);
		if (barId != null) {
			club.setBar(new Bar(barId));
		}
		
		if (admin != null) {
			club.setAdmin(admin.createDomain());
		}
		
		if (staffs != null) {
			club.setStaffs(StaffDoc.createDomains(staffs));
		}
		club.setLastUpdateDate(new Date(lastUpdateDate));
		
		club.setThumbnailImageId(thumbnailImageId);
		club.setBannerImageId(bannerImageId);
		
		return club;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNameKor() {
		return nameKor;
	}

	public void setNameKor(String nameKor) {
		this.nameKor = nameKor;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public String getBarId() {
		return barId;
	}

	public void setBarId(String barId) {
		this.barId = barId;
	}

	public StaffDoc getAdmin() {
		return admin;
	}

	public void setAdmin(StaffDoc admin) {
		this.admin = admin;
	}

	public List<StaffDoc> getStaffs() {
		return staffs;
	}

	public void setStaffs(List<StaffDoc> staffs) {
		this.staffs = staffs;
	}
	
	public long getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(long lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getThumbnailImageId() {
		return thumbnailImageId;
	}

	public void setThumbnailImageId(String thumbnailImageId) {
		this.thumbnailImageId = thumbnailImageId;
	}

	public String getBannerImageId() {
		return bannerImageId;
	}

	public void setBannerImageId(String bannerImageId) {
		this.bannerImageId = bannerImageId;
	}
}
