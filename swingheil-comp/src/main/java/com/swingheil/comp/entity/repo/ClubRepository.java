package com.swingheil.comp.entity.repo;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.ClubDoc;

public interface ClubRepository extends MongoRepository<ClubDoc, String>, ClubRepositoryCustom {
	//
	List<ClubDoc> findAllByStaffsId(String adminId);
	
	List<ClubDoc> findAllByBarId(String barId);
	
	List<ClubDoc> findAllByStaffsId(String adminId, Sort sort);
}
