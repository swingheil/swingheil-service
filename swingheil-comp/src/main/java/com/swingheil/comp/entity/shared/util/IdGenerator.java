package com.swingheil.comp.entity.shared.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

@Component
public class IdGenerator {
	//
	@Autowired
	private MongoTemplate template;
	
	public String generate(String key) {
		//
		Query query = new Query(Criteria.where("_id").is(key));
		Update update = new Update();
		update.inc("seq", 1);
		FindAndModifyOptions options = FindAndModifyOptions.options()
				.returnNew(true)
				.upsert(true);
		IdSequence sequence = template.findAndModify(query, update, options, IdSequence.class);
		return Integer.toString(sequence.getSeq());
	}
	
	//--------------------------------------------------------------------------
	
	@Document(collection = "sequences")
	public static class IdSequence {
		//
		@Id
		private String id;
		private int seq;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public int getSeq() {
			return seq;
		}
		public void setSeq(int seq) {
			this.seq = seq;
		}
	}
}
