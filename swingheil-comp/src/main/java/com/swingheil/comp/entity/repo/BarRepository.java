package com.swingheil.comp.entity.repo;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.BarDoc;

public interface BarRepository extends MongoRepository<BarDoc, String>, BarRepositoryCustom {
	//
	List<BarDoc> findAllByStaffsId(String adminId);
	List<BarDoc> findAllByStaffsId(String adminId, Sort sort);
}
