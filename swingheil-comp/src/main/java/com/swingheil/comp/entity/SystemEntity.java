package com.swingheil.comp.entity;

import com.swingheil.domain.env.SystemEnvironment;

public interface SystemEntity {
	
	SystemEnvironment retrieve();
}
