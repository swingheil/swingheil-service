package com.swingheil.comp.entity.doc;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.notification.Push;

@Document(collection = "pushs")
public class PushDoc implements Serializable {

	private static final long serialVersionUID = -4149031175387744663L;
	
	@Id
	private String id;
	private int alertCount;
	private Long attempt;
	private Long created;
	private String deviceId;
	private String deviceType;
	private String errorMsg;
	private String message;
	private String pushType;
	private String sound;
	private String state;
	private String tag1;
	private String tag2;
	private String tag3;
	private String tag4;
	
	public PushDoc(Push push) {
		//
		this.id = push.getId();
		this.alertCount = push.getAlertCount();
		this.attempt = push.getAttempt().getTime();
		this.created = push.getCreated().getTime();
		this.deviceId = push.getDeviceId();
		this.deviceType = push.getDeviceType();
		this.errorMsg = push.getErrorMsg();
		this.message = push.getMessage();
		this.pushType = push.getPushType();
		this.sound = push.getSound();
		this.state = push.getState();
		this.tag1 = push.getTag1();
		this.tag2 = push.getTag2();
		this.tag3 = push.getTag3();
		this.tag4 = push.getTag4();
	}
	
	public Push createDomain() {
		//
		Push push = new Push();
		push.setId(id);
		push.setAlertCount(alertCount);
		if (attempt != null) push.setAttempt(new Date(attempt));
		if (created != null) push.setCreated(new Date(created));
		push.setDeviceId(deviceId);
		push.setDeviceType(deviceType);
		push.setErrorMsg(errorMsg);
		push.setMessage(message);
		push.setPushType(pushType);
		push.setSound(sound);
		push.setState(state);
		push.setTag1(tag1);
		push.setTag2(tag2);
		push.setTag3(tag3);
		push.setTag4(tag4);
		
		return push;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getAlertCount() {
		return alertCount;
	}
	public void setAlertCount(int alertCount) {
		this.alertCount = alertCount;
	}
	public Long getAttempt() {
		return attempt;
	}
	public void setAttempt(Long attempt) {
		this.attempt = attempt;
	}
	public Long getCreated() {
		return created;
	}
	public void setCreated(Long created) {
		this.created = created;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPushType() {
		return pushType;
	}
	public void setPushType(String pushType) {
		this.pushType = pushType;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getTag1() {
		return tag1;
	}
	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}
	public String getTag2() {
		return tag2;
	}
	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}
	public String getTag3() {
		return tag3;
	}
	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}
	public String getTag4() {
		return tag4;
	}
	public void setTag4(String tag4) {
		this.tag4 = tag4;
	}

}
