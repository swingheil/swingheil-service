package com.swingheil.comp.entity;

import java.util.List;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface BarEntity {
	//
	String create(Bar bar);
	void update(Bar bar);
	Bar retrieve(String barId);
	Page<Bar> retrieveAll(PageInfo pageInfo);
	
	List<Bar> retriveManagedBars(String adminId);
	
	void addStaff(String barId, Staff staff);
	void removeStaff(String barId, String staffId);
	
	void activateAsStaff(String barId, String staffId);
	
	void delegateAdmin(String barId, Staff staff);
}
