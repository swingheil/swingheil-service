package com.swingheil.comp.entity.repo;

import java.util.List;

import com.swingheil.comp.entity.doc.UserDoc.DeviceDoc;
import com.swingheil.domain.user.SocialUser;

public interface UserRepositoryCustom {
	//
	void addFavoriteEvent(String userId, String eventId);
	void removeFavoriteEvent(String userId, String eventId);
	
	void addFavoriteClub(String userId, String clubId);
	void removeFavoriteClub(String userId, String clubId);
	
	void addFavoriteBar(String userId, String barId);
	void removeFavoriteBar(String userId, String barId);

	void addUserDevice(String userId, DeviceDoc device);
	void removeUserDevice(String userId, String deviceUid);
	
	SocialUser findByDeviceUid(String deviceUid);
	
	List<SocialUser> findByBar(String barId);
	List<SocialUser> findByClub(String clubId);
}
