package com.swingheil.comp.entity.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

import com.mongodb.BasicDBObject;
import com.swingheil.comp.entity.doc.ClubDoc;
import com.swingheil.comp.entity.doc.StaffDoc;
import com.swingheil.comp.entity.repo.ClubRepositoryCustom;
import com.swingheil.domain.type.enums.StaffState;

public class ClubRepositoryImpl extends SimpleMongoRepository<ClubDoc, String> implements ClubRepositoryCustom {

	@Autowired
	public ClubRepositoryImpl(MongoRepositoryFactory factory, MongoTemplate template) {
		super(factory.<ClubDoc, String>getEntityInformation(ClubDoc.class), template);
	}
	
	@Override
	public void addStaff(String clubId, StaffDoc staffDoc) {
		// 
		Query query = new Query(Criteria.where("_id").is(clubId));
		Update update = new Update();
		update.push("staffs", staffDoc);
		
		getMongoOperations().updateFirst(query, update, ClubDoc.class);
	}

	@Override
	public void removeStaff(String clubId, String staffId) {
		//
		Query query = new Query(Criteria.where("_id").is(clubId));
		Update update = new Update();
		update.pull("staffs", new BasicDBObject("_id", staffId));
		
		getMongoOperations().updateFirst(query, update, ClubDoc.class);
	}

	@Override
	public void delegateAdmin(String clubId, StaffDoc staffDoc) {
		// 
		Query query = new Query(Criteria.where("_id").is(clubId));
		Update update = new Update();
		update.set("admin", staffDoc);
		
		getMongoOperations().updateFirst(query, update, ClubDoc.class);
	}

	@Override
	public void activateAsStaff(String clubId, String staffId) {
		//
		Query query = new Query(Criteria.where("_id").is(clubId).andOperator(Criteria.where("staffs").elemMatch(Criteria.where("_id").is(staffId))));
		
		Update update = new Update().set("staffs.$.state", StaffState.Active);
		getMongoOperations().updateFirst(query, update, ClubDoc.class);
	}
}
