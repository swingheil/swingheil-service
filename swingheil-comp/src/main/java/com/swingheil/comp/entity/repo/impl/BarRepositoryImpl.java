package com.swingheil.comp.entity.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory;
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository;

import com.mongodb.BasicDBObject;
import com.swingheil.comp.entity.doc.BarDoc;
import com.swingheil.comp.entity.doc.StaffDoc;
import com.swingheil.comp.entity.repo.BarRepositoryCustom;
import com.swingheil.domain.type.enums.StaffState;

public class BarRepositoryImpl extends SimpleMongoRepository<BarDoc, String> implements BarRepositoryCustom {

	@Autowired
	public BarRepositoryImpl(MongoRepositoryFactory factory, MongoTemplate template) {
		super(factory.<BarDoc, String>getEntityInformation(BarDoc.class), template);
	}

	@Override
	public void addStaff(String barId, StaffDoc staffDoc) {
		// 
		Query query = new Query(Criteria.where("_id").is(barId));
		Update update = new Update();
		update.push("staffs", staffDoc);
		
		getMongoOperations().updateFirst(query, update, BarDoc.class);
	}

	@Override
	public void removeStaff(String barId, String staffId) {
		// 
		Query query = new Query(Criteria.where("_id").is(barId));
		Update update = new Update();
		update.pull("staffs", new BasicDBObject("_id", staffId));
		
		getMongoOperations().updateFirst(query, update, BarDoc.class);
	}
	
	@Override
	public void activateAsStaff(String barId, String staffId) {
		// 
		Query query = new Query(Criteria.where("staffs").elemMatch(Criteria.where("_id").is(staffId)));
		
		Update update = new Update().set("staffs.$.state", StaffState.Active);
		getMongoOperations().updateFirst(query, update, BarDoc.class);
	}

	@Override
	public void delegateAdmin(String barId, StaffDoc staffDoc) {
		//
		Query query = new Query(Criteria.where("_id").is(barId));
		Update update = new Update();
		update.set("admin", staffDoc);
		
		getMongoOperations().updateFirst(query, update, BarDoc.class);
	}
}
