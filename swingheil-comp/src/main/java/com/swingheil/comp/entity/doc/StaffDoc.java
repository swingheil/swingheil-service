package com.swingheil.comp.entity.doc;

import java.util.ArrayList;
import java.util.List;

import com.swingheil.domain.group.Staff;
import com.swingheil.domain.type.enums.StaffState;
import com.swingheil.domain.user.SocialUser;

public class StaffDoc {
	//
	private String id;
	private String nickname;
	private StaffState state;

	//--------------------------------------------------------------------------
	
	public StaffDoc() {
		//
	}
	
	public StaffDoc(Staff staff) {
		//
		this.id = staff.getId();
		this.nickname = staff.getNickname();
		this.state = staff.getState();
	}
	
	//--------------------------------------------------------------------------
	
	public static List<Staff> createDomains(List<StaffDoc> staffDocs) {
		//
		if (staffDocs != null) {
			List<Staff> staffs = new ArrayList<Staff>();
			for (StaffDoc doc : staffDocs) {
				staffs.add(doc.createDomain());
			}
			return staffs;
		}
		return null;
	}
	
	public Staff createDomain() {
		//
		SocialUser roleUser = new SocialUser(nickname);
		roleUser.setId(id);
		
		Staff staff = new Staff(roleUser);
		staff.setState(state);
		
		return staff;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public StaffState getState() {
		return state;
	}

	public void setState(StaffState state) {
		this.state = state;
	}
}
