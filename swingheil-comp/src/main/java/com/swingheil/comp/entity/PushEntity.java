package com.swingheil.comp.entity;

import java.util.List;

import com.swingheil.domain.notification.Push;

public interface PushEntity {
	//
	String create(Push push);
	Push retrieve(String id);
	List<Push> retrieveAllByState();
	void update(Push push);
	void delete(String id);
}
