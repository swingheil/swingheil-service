package com.swingheil.comp.entity.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.UserDoc;
import com.swingheil.domain.user.SocialUser;

public interface UserRepository extends MongoRepository<UserDoc, String>, UserRepositoryCustom {
	//
	List<SocialUser> findByNickname(String nickName);
	List<SocialUser> findByEmail(String email);
	List<SocialUser> findByPhone(String phone);
}
