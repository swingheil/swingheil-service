package com.swingheil.comp.entity.doc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.type.enums.BarType;
import com.swingheil.domain.type.place.Address;

@Document(collection = "bars")
public class BarDoc {
	//
	@Id
	private String id;
	private String nameKor;
	private String nameEng;
	private String homePage;
	
	private StaffDoc admin;
	private List<StaffDoc> staffs;
	
	private BarType type;
	private Address address;
	private long lastUpdateDate;
	
	// image
	private String thumbnailImageId;
	private String bannerImageId;
	
	//--------------------------------------------------------------------------
	
	public BarDoc() {
		//
	}
	
	public BarDoc(Bar bar) {
		//
		this.id = bar.getId();
		this.nameKor = bar.getNameKor();
		this.nameEng = bar.getNameEng();
		this.homePage = bar.getHomePage();
		this.type = bar.getType();
		this.address = bar.getAddress();
		this.lastUpdateDate = bar.getLastUpdateDate().getTime();
		this.thumbnailImageId = bar.getThumbnailImageId();
		this.bannerImageId = bar.getBannerImageId();
		
		if(bar.getAdmin() != null) {
			this.admin = new StaffDoc(bar.getAdmin());			
		}
		
		List<StaffDoc> staffs = new ArrayList<StaffDoc>();
		
		for(Staff staff : bar.getStaffs()) {
			StaffDoc staffDoc = new StaffDoc(staff);
			staffs.add(staffDoc);
		}
		
		this.staffs = staffs;
	}

	//--------------------------------------------------------------------------
	
	public static List<Bar> createDomains(List<BarDoc> docs) {
		// 
		if (docs != null) {
			List<Bar> bars = new ArrayList<Bar>();
			for (BarDoc doc : docs) {
				bars.add(doc.createDomain());
			}
			return bars;
		}
		return null;
	}
	
	public Bar createDomain() {
		//
		Bar bar = new Bar();
		bar.setId(id);
		bar.setNameKor(nameKor);
		bar.setNameEng(nameEng);
		bar.setHomePage(homePage);
		bar.setType(type);
		bar.setAddress(address);
		
		if (admin != null) {
			bar.setAdmin(admin.createDomain());
		}
		
		if (staffs != null) {
			bar.setStaffs(StaffDoc.createDomains(staffs));
		}
		
		bar.setThumbnailImageId(thumbnailImageId);
		bar.setBannerImageId(bannerImageId);
		bar.setLastUpdateDate(new Date(lastUpdateDate));
		
		return bar;
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNameKor() {
		return nameKor;
	}

	public void setNameKor(String nameKor) {
		this.nameKor = nameKor;
	}

	public String getNameEng() {
		return nameEng;
	}

	public void setNameEng(String nameEng) {
		this.nameEng = nameEng;
	}

	public String getHomePage() {
		return homePage;
	}

	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	public BarType getType() {
		return type;
	}

	public void setType(BarType type) {
		this.type = type;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public StaffDoc getAdmin() {
		return admin;
	}

	public void setAdmin(StaffDoc admin) {
		this.admin = admin;
	}

	public List<StaffDoc> getStaffs() {
		return staffs;
	}

	public void setStaffs(List<StaffDoc> staffs) {
		this.staffs = staffs;
	}
	
	public long getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(long lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getThumbnailImageId() {
		return thumbnailImageId;
	}

	public void setThumbnailImageId(String thumbnailImageId) {
		this.thumbnailImageId = thumbnailImageId;
	}

	public String getBannerImageId() {
		return bannerImageId;
	}

	public void setBannerImageId(String bannerImageId) {
		this.bannerImageId = bannerImageId;
	}
}
