package com.swingheil.comp.entity.repo;

import com.swingheil.comp.entity.doc.StaffDoc;

public interface BarRepositoryCustom {
	void addStaff(String barId, StaffDoc staffDoc);
	void removeStaff(String barId, String staffId);
	void activateAsStaff(String barId, String staffId);
	void delegateAdmin(String barId, StaffDoc staffDoc);
}
