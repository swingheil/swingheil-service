package com.swingheil.comp.entity.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.swingheil.comp.entity.doc.PushDoc;
import com.swingheil.domain.notification.Push;

public interface PushRepository extends MongoRepository<PushDoc, String> {

	List<Push> findAllByState(String state);

}
