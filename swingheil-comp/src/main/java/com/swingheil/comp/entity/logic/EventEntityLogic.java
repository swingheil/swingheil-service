package com.swingheil.comp.entity.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.EventEntity;
import com.swingheil.comp.entity.doc.EventDoc;
import com.swingheil.comp.entity.repo.EventRepository;
import com.swingheil.comp.entity.shared.util.IdGenerator;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.enums.EventCategory;

@Repository
public class EventEntityLogic implements EventEntity {
	
	@Autowired
	private EventRepository repository;
	
	@Autowired
	private IdGenerator idGenerator;

	@Override
	public String create(Event event) {
		// 
		event.setId(idGenerator.generate("event"));
		EventDoc doc = new EventDoc(event);
		repository.save(doc);
		
		return doc.getId();
	}

	@Override
	public Event retrieve(String eventId) {
		// 
		EventDoc doc = repository.findOne(eventId);
		if (doc != null) {
			return doc.createDomain();
		}
		return null;
	}
	
	@Override
	public Page<Event> retriveAllByClubId(String clubId, PageInfo pageInfo) {
		// 
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "regTime");
		
		
		
		org.springframework.data.domain.Page<EventDoc> resultPage = null;
		
		if (pageInfo.isForce()) {
			resultPage = repository.findAllBySponsorIdAndCategoryCode(clubId, EventCategory.Club.code(), pageRequest);
		} else {
			resultPage = repository.findAllBySponsorIdAndCategoryCodeAndVisible(clubId, EventCategory.Club.code(), true, pageRequest);
		}
		
		Page<Event> page = new Page<Event>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(EventDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}
	
	@Override
	public Page<Event> retriveAllByBarId(String barId, PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "regTime");
		
		org.springframework.data.domain.Page<EventDoc> resultPage = null;
		if (pageInfo.isForce()) {
			resultPage = repository.findAllBySponsorIdAndCategoryCode(barId, EventCategory.SwingBar.code(), pageRequest);
		} else {
			resultPage = repository.findAllBySponsorIdAndCategoryCodeAndVisible(barId, EventCategory.SwingBar.code(), true, pageRequest);
		}
		
		Page<Event> page = new Page<Event>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(EventDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}

	@Override
	public Page<Event> retriveAllBySponsorIds(List<String> groupIds, PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "regTime");
		
		org.springframework.data.domain.Page<EventDoc> resultPage = null;
		if (pageInfo.isForce()) {
			resultPage = repository.findAllBySponsorIdIn(groupIds, pageRequest);
		} else {
			resultPage = repository.findAllBySponsorIdInAndVisible(groupIds, true, pageRequest);
		}
		
		
		Page<Event> page = new Page<Event>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(EventDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}

	@Override
	public Page<Event> retriveAllByCategory(String category, PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "regTime");
		
		org.springframework.data.domain.Page<EventDoc> resultPage = null;
		if (pageInfo.isForce()) {
			resultPage = repository.findAllByCategoryCode(category, pageRequest);
		} else {
			resultPage = repository.findAllByCategoryCodeAndVisible(category, true, pageRequest);
		}
		
		Page<Event> page = new Page<Event>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(EventDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}
	
	@Override
	public Page<Event> retriveAll(PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "regTime");
		
		org.springframework.data.domain.Page<EventDoc> resultPage = 
				repository.findAll(pageRequest);
		
		Page<Event> page = new Page<Event>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(EventDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}	

	@Override
	public void update(Event event) {
		//
		EventDoc doc = new EventDoc(event);
		repository.save(doc);
	}

	@Override
	public void delete(String eventId) {
		// 
		repository.delete(eventId);
	}

}
