package com.swingheil.comp.entity.doc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Group;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.DateTime;
import com.swingheil.domain.type.Period;
import com.swingheil.domain.type.enums.EventCategory;
import com.swingheil.domain.type.enums.PeriodType;
import com.swingheil.domain.type.place.Address;
import com.swingheil.domain.type.place.GeoLocation;

@Document(collection = "events")
public class EventDoc {
	//
	@Id
	private String id;
	private CodeName category;
	private GroupDoc sponsor;
	private String title;
	private String summary;
	private String periodType;
	private String fromDate; // yyyy-mm-dd
	private String toDate;
	private String fromTime; // hh:mm
	private String toTime;
	private Address place;
	private String detail;
	private long regTime;
	private boolean visible;
	private boolean notice;

	private String smallImageId;
	private String largeImageId;
	
	private GeoLocation location;

	//--------------------------------------------------------------------------
	
	public EventDoc() {
		//
	}
	
	public EventDoc(Event event) {
		//
		this.id = event.getId();
		this.category = EventCategory.findBy(event.getCategory().getCode()).convertCodeName();
		this.sponsor = new GroupDoc(event.getSponsor());
		this.title = event.getTitle();
		this.summary = event.getSummary();
		this.detail = event.getDetail();
		this.place = event.getPlace();
		
		DateTime from = event.getPeriod().getFrom();
		DateTime to = event.getPeriod().getTo();
		this.periodType = event.getPeriod().getType().code();
		if (from != null) {
			if (from.getDate() != null) this.fromDate = from.getDate();
			if (from.getTime() != null) this.fromTime = from.getTime();
		}
		if (to != null) {
			if (to.getDate() != null) this.toDate = to.getDate();
			if (to.getTime() != null) this.toTime = to.getTime();
		}

		this.regTime = event.getRegDate().getTime();
		this.smallImageId = event.getSmallImageId();
		this.largeImageId = event.getLargeImageId();
		this.visible = event.isVisible();
		this.notice = event.isNotice();
		this.location = event.getLocation();
	}

	//--------------------------------------------------------------------------

	public Event createDomain() {
		//
		Event event = new Event();
		event.setId(id);
		event.setCategory(EventCategory.findBy(this.category.getCode()).convertCodeName());
		event.setTitle(title);
		event.setSummary(summary);
		event.setSponsor(new Group());
		
		DateTime from = new DateTime();
		if (fromDate != null) from.setDate(fromDate);
		if (fromTime != null) from.setTime(fromTime);
		DateTime to = new DateTime();
		if (toDate != null) to.setDate(toDate);
		if (toTime != null) to.setTime(toTime);
		
		Period period = new Period();
		period.setType(PeriodType.findBy(periodType));
		period.setFrom(from);
		period.setTo(to);
		event.setPeriod(period);
		
		event.setPlace(place);
		if (sponsor != null) {
			event.setSponsor(sponsor.createDomain());
		}
		event.setDetail(detail);
		event.setRegDate(new Date(regTime));
		event.setSmallImageId(smallImageId);
		event.setLargeImageId(largeImageId);
		event.setVisible(visible);
		event.setNotice(notice);
		
		event.setLocation(location);
		
		return event;
	}
	
	public static List<Event> createDomains(List<EventDoc> eventDocs) {
		//
		List<Event> events = new ArrayList<Event>();
		if (eventDocs != null) {
			for (EventDoc doc : eventDocs) {
				events.add(doc.createDomain());
			}
		}
		return events;
	}
	
	//--------------------------------------------------------------------------

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CodeName getCategory() {
		return category;
	}
	public void setCategory(CodeName category) {
		this.category = category;
	}
	public GroupDoc getSponsor() {
		return sponsor;
	}
	public void setSponsor(GroupDoc sponsor) {
		this.sponsor = sponsor;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public Address getPlace() {
		return place;
	}
	public void setPlace(Address place) {
		this.place = place;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getSmallImageId() {
		return smallImageId;
	}
	public void setSmallImageId(String smallImageId) {
		this.smallImageId = smallImageId;
	}
	public String getLargeImageId() {
		return largeImageId;
	}
	public void setLargeImageId(String largeImageId) {
		this.largeImageId = largeImageId;
	}
	public long getRegTime() {
		return regTime;
	}
	public void setRegTime(long regTime) {
		this.regTime = regTime;
	}
	
	//--------------------------------------------------------------------------

	public static class GroupDoc {
		//
		private String id;
		private String nameKor;
		private String nameEng;
		
		//----------------------------------------------------------------------
		
		public GroupDoc() {
			//
		}
		
		public GroupDoc(Group group) {
			//
			this.id = group.getId();
			this.nameKor = group.getNameKor();
			this.nameEng = group.getNameEng();
		}

		//----------------------------------------------------------------------
		
		public Group createDomain() {
			//
			Group group = new Group();
			group.setId(this.id);
			group.setNameKor(nameKor);
			group.setNameEng(nameEng);
			
			return group;
		}
		
		//----------------------------------------------------------------------
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getNameKor() {
			return nameKor;
		}

		public void setNameKor(String nameKor) {
			this.nameKor = nameKor;
		}

		public String getNameEng() {
			return nameEng;
		}

		public void setNameEng(String nameEng) {
			this.nameEng = nameEng;
		}
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public boolean isNotice() {
		return notice;
	}
	
	public void setNotice(boolean notice) {
		this.notice = notice;
	}

	public GeoLocation getLocation() {
		return location;
	}

	public void setLocation(GeoLocation location) {
		this.location = location;
	}
}
