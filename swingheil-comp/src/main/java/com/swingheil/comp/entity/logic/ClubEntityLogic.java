package com.swingheil.comp.entity.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import com.swingheil.comp.entity.ClubEntity;
import com.swingheil.comp.entity.doc.ClubDoc;
import com.swingheil.comp.entity.doc.StaffDoc;
import com.swingheil.comp.entity.repo.ClubRepository;
import com.swingheil.comp.entity.shared.util.IdGenerator;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

@Repository
public class ClubEntityLogic implements ClubEntity {
	//
	@Autowired
	private ClubRepository repository;
	
	@Autowired
	private IdGenerator idGenerator;
	
	@Override
	public String create(Club club) {
		// 
		// bar, club 구분 없이 group 키로 채번
		club.setId(idGenerator.generate("group")); 
		ClubDoc doc = new ClubDoc(club);
		repository.save(doc);
		
		return doc.getId();
	}

	@Override
	public void update(Club club) {
		// 
		repository.save(new ClubDoc(club));
	}

	@Override
	public Club retrieve(String clubId) {
		//
		ClubDoc doc = repository.findOne(clubId);
		if (doc != null) {
			return doc.createDomain();
		}
		return null;
	}

	@Override
	public Page<Club> retrieveAll(PageInfo pageInfo) {
		//
		PageRequest pageRequest = new PageRequest(
				pageInfo.getPage() - 1, 
				pageInfo.getLimit(), 
				Sort.Direction.DESC, "lastUpdateDate");
		
		org.springframework.data.domain.Page<ClubDoc> resultPage = 
				repository.findAll(pageRequest);
		
		Page<Club> page = new Page<Club>(pageInfo);
		if (resultPage.hasContent()) {
			page.setResults(ClubDoc.createDomains(resultPage.getContent()));
			page.setNext(resultPage.hasNextPage());
		}
		
		return page;
	}

	@Override
	public List<Club> retriveManagedClubs(String adminId) {
		//
		Sort sort = new Sort(Sort.Direction.ASC, "nameKor");
		
		List<ClubDoc> clubDocs = repository.findAllByStaffsId(adminId, sort);
		
		return ClubDoc.createDomains(clubDocs);
	}

	@Override
	public void addStaff(String clubId, Staff staff) {
		// 
		repository.addStaff(clubId, new StaffDoc(staff));
	}

	@Override
	public void removeStaff(String clubId, String staffId) {
		// 
		repository.removeStaff(clubId, staffId);
	}
	
	@Override
	public void activateAsStaff(String clubId, String staffId) {
		//
		repository.activateAsStaff(clubId, staffId);
	}

	@Override
	public void delegateAdmin(String clubId, Staff staff) {
		// 
		repository.delegateAdmin(clubId, new StaffDoc(staff));
	}

	@Override
	public List<Club> retrieveByBar(String barId) {
		//
		List<ClubDoc> clubDocs = repository.findAllByBarId(barId);
		
		return ClubDoc.createDomains(clubDocs);
	}
}
