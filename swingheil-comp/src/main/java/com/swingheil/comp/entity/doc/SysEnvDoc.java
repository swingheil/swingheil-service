package com.swingheil.comp.entity.doc;

import org.springframework.data.mongodb.core.mapping.Document;

import com.swingheil.domain.env.SystemEnvironment;

@Document(collection = "sysenv")
public class SysEnvDoc {
	//
	private String googleAppKey;
	
	//--------------------------------------------------------------------------
	
	public SysEnvDoc() {
		//
	}
	
	public SysEnvDoc(SystemEnvironment environment) {
		//
		this.googleAppKey = environment.getGoogleAppKey();
	}
	
	//--------------------------------------------------------------------------
	
	public SystemEnvironment createDomain() {
		//
		SystemEnvironment environment = new SystemEnvironment();
		environment.setGoogleAppKey(googleAppKey);
		
		return environment;
	}
	
	//--------------------------------------------------------------------------

	public String getGoogleAppKey() {
		return googleAppKey;
	}

	public void setGoogleAppKey(String googleAppKey) {
		this.googleAppKey = googleAppKey;
	}
}
