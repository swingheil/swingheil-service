package com.swingheil.comp.process.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swingheil.comp.entity.UserEntity;
import com.swingheil.comp.process.UserService;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@Service
public class UserServiceLogic implements UserService {

	@Autowired
	private UserEntity userEntity;
	
	@Override
	public SocialUser registerUser(String nickname, Device device) {
		//
		SocialUser user = new SocialUser(nickname);
		user.addDevice(device);
		String userId = userEntity.create(user);
		
		return userEntity.retrieve(userId);
	}

	@Override
	public List<SocialUser> getAllUsers() {
		// 
		return userEntity.retrieveAll();
	}

	@Override
	public List<SocialUser> getUsersByNickname(String nickname) {
		//
		return userEntity.retrieveByNickname(nickname);
	}

	@Override
	public List<SocialUser> getUsersByPhoneNo(String phoneNo) {
		//
		return userEntity.retrieveByPhoneNo(phoneNo);
	}

	@Override
	public List<SocialUser> getUsersByEmail(String email) {
		//
		return userEntity.retrieveByEmail(email);
	}

	@Override
	public List<Device> getUserDeviceList(String userId) {
		// 
		SocialUser user = userEntity.retrieve(userId);
		return user.getDevices();
	}

	@Override
	public void addUserDevice(String userId, Device device) {
		// 
		userEntity.addDevice(userId, device);
	}

	@Override
	public void removeUserDevice(String userId, String deviceUid) {
		// 
		userEntity.removeDevice(userId, deviceUid);
	}

	@Override
	public SocialUser getUserByDeviceUid(String deviceUid) {
		//
		SocialUser socialUser = userEntity.retrieveByDeviceUid(deviceUid);
		return filterPrivateInfo(socialUser);
	}

	@Override
	public String getUserIdByEmailAndPassword(String email, String password) {
		// 
		List<SocialUser> socialUsers = userEntity.retrieveByEmail(email);
		if (socialUsers != null && !socialUsers.isEmpty()) {
			SocialUser user = socialUsers.get(0);
			if (password.equals(user.getPassword())) {
				return user.getId();
			}
		}
		return null;
	}

	@Override
	public SocialUser getUserByUserid(String userId) {
		// 
		SocialUser socialUser = userEntity.retrieve(userId);
		return filterPrivateInfo(socialUser);
	}
	
	@Override
	public void updateUserDetail(String userId, String email, String password, String phone) {
		//
		SocialUser user = userEntity.retrieve(userId);
		user.setEmail(email);
		user.setPassword(password);
		user.setPhone(phone);
		
		userEntity.update(user);
	}

	/**
	 * 중요한 개인정보를 제거한다.
	 * 
	 * @param socialUser
	 * @return
	 */
	private SocialUser filterPrivateInfo(SocialUser socialUser) {
		//
		if (socialUser != null) {
			socialUser.setPhone(null);
			socialUser.setPassword(null);
		}
		return socialUser;
	}

	@Override
	public List<SocialUser> getUsersByBar(String barId) {
		//
		return userEntity.retrieveByBar(barId);
	}

	@Override
	public List<SocialUser> getUsersByClub(String clubId) {
		//
		return userEntity.retrieveByClub(clubId);
	}
}
