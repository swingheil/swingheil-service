package com.swingheil.comp.process;

import java.util.List;

import com.swingheil.domain.type.enums.NotiType;

public interface NotificationService {
	void sendPushMessage(String type, String message, int badgeCount, String sound, List<String> regIds);
	void sendPushMessage(String title, String message, NotiType notiType, String eventId, List<String> regIds);
	void sendPushMessage(String title, String message, String notiType, String eventId, String regId);
}
