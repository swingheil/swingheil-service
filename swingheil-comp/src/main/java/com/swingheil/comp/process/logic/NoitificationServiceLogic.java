package com.swingheil.comp.process.logic;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.swingheil.comp.process.NotificationService;
import com.swingheil.domain.type.enums.NotiType;

@Service
public class NoitificationServiceLogic implements NotificationService {

	private Sender sender;
	
	@Override
	public void sendPushMessage(String type, String message, int badgeCount,
			String sound, List<String> regIds) {
		//

	}

	@Override
	public void sendPushMessage(String title, String message, NotiType notiType, String eventId, List<String> regIds) {
		// 
		sender = new Sender("AIzaSyA7rTLvdfjLmTCt-elN_uNbwZud_YpxhHM");
		
		Builder builder = new Message.Builder();
		builder.addData("title", title);
		builder.addData("message", message);
		builder.addData("notiType", notiType.getCode());
		builder.addData("eventId", eventId);
		
		Message m = builder.build();
		
		try {
			MulticastResult result = sender.send(m, regIds, 5);
			
			if(result == null) {
				throw new RuntimeException("Result is null");
			}
		} catch(Exception ex) {
			throw new RuntimeException("IO Exception: " + ex.getMessage());
		}
	}

	@Override
	public void sendPushMessage(String title, String message, String notiType,
			String eventId, String regId) {
		sender = new Sender("AIzaSyA7rTLvdfjLmTCt-elN_uNbwZud_YpxhHM");
		
		Builder builder = new Message.Builder();
		builder.addData("title", title);
		builder.addData("message", message);
		builder.addData("notiType", notiType);
		builder.addData("eventId", eventId);
		
		Message m = builder.build();
		
		try {
			Result result = sender.send(m, regId, 5);
			
			if(result == null) {
				throw new RuntimeException("Result is null");
			}
		} catch(Exception ex) {
			throw new RuntimeException("IO Exception: " + ex.getMessage());
		}
	}
}
