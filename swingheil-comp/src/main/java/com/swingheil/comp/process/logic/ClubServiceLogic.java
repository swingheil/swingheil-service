package com.swingheil.comp.process.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swingheil.comp.entity.BarEntity;
import com.swingheil.comp.entity.ClubEntity;
import com.swingheil.comp.entity.UserEntity;
import com.swingheil.comp.process.ClubService;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.enums.StaffState;
import com.swingheil.domain.user.SocialUser;

@Service
public class ClubServiceLogic implements ClubService {

	@Autowired
	private ClubEntity clubEntity;
	
	@Autowired
	private BarEntity barEntity;
	
	@Autowired
	private UserEntity userEntity;
	
	@Override
	public String registerClub(Club club) {
		//
		return clubEntity.create(club);
	}

	@Override
	public void modifyClub(Club club) {
		//
		clubEntity.update(club);
	}

	@Override
	public Club getClub(String userId, String clubId) {
		// 
		Club club = clubEntity.retrieve(clubId);
		
		// 바 상세정보 세팅
		if(club != null) {
			club.setBar(barEntity.retrieve(club.getBar().getId()));
		}
		
		return markFavoriteAndReturn(userId, club);
	}

	@Override
	public Page<Club> getClubs(String userId, PageInfo pageInfo) {
		// 
		Page<Club> page = clubEntity.retrieveAll(pageInfo);;
		return markFavoriteAndReturn(userId, page);
	}

	@Override
	public void followClub(String userId, String clubId) {
		//
		userEntity.addFavoriteClub(userId, clubId);
	}

	@Override
	public void unfollowClub(String userId, String clubId) {
		//
		userEntity.removeFavoriteClub(userId, clubId);
	}
	
	//--------------------------------------------------------------------------
	
	/**
	 * 즐겨찾기한 클럽에 마킹한다.
	 * 
	 * @param userId
	 * @param club
	 * @return
	 */
	private Club markFavoriteAndReturn(String userId, Club club) {
		//
		if (userId == null) return club;
		
		List<String> favorites = userEntity.retriveFavoriteClubs(userId);
		if (favorites != null && !favorites.isEmpty() && club != null) {

			if (favorites.contains(club.getId())) {
				club.setFavorite(true);
			}
		}
		return club;
	}
	
	/**
	 * 즐겨찾기한 클럽에 마킹한다.
	 * 
	 * @param userId
	 * @param page
	 * @return
	 */
	private Page<Club> markFavoriteAndReturn(String userId, Page<Club> page) {
		//
		if (userId == null) return page;
		
		List<String> favorites = userEntity.retriveFavoriteClubs(userId);
		if (favorites != null && !favorites.isEmpty() && page.hasResults()) {
			
			for (Club club : page.getResults()) {
				if (favorites.contains(club.getId())) {
					club.setFavorite(true);
				}
			}
		}
		return page;
	}

	@Override
	public void joinAsStaff(String clubId, String userId) {
		//
		
		List<Staff> staffs = clubEntity.retrieve(clubId).getStaffs();
		if (staffs != null && !staffs.isEmpty()) {
			for(Staff staff : staffs) {
				if(staff.getId().equals(userId) == true) {
					throw new RuntimeException("이미 등록된 운영자입니다");
				}
			}
		}
		
		SocialUser socialUser = userEntity.retrieve(userId);
		Staff staff = new Staff(socialUser);
		staff.setState(StaffState.Requested);
		
		clubEntity.addStaff(clubId, staff);
		
		// 최소 등록시 운영자가 없으면 자동으로 Admin 등록
		if(staffs == null || staffs.isEmpty()) {
			userEntity.addFavoriteClub(userId, clubId);
			clubEntity.delegateAdmin(clubId, staff);
			this.activateAsStaff(clubId, userId);
		}
	}

	@Override
	public void withdrawAsStaff(String clubId, String userId) {
		//
		userEntity.removeFavoriteClub(userId, clubId);
		
		clubEntity.removeStaff(clubId, userId);
	}
	
	@Override
	public void activateAsStaff(String clubId, String userId) {
		//
		userEntity.addFavoriteClub(userId, clubId);
		
		clubEntity.activateAsStaff(clubId, userId);
	}

	@Override
	public void delegateAdmin(String clubId, String userId) {
		//
		
		SocialUser socialUser = userEntity.retrieve(userId);
		Staff staff = new Staff(socialUser);
		
		// TODO 위임하고자 하는 관리자가 해당 클럽의 운영자인지 체크해야 함
		
		clubEntity.delegateAdmin(clubId, staff);
	}

	@Override
	public List<Club> getManagedClubs(String adminId) {
		// 
		return clubEntity.retriveManagedClubs(adminId);
	}
}
