package com.swingheil.comp.process;

import java.util.List;

import com.swingheil.domain.event.Event;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface EventService {
	//
	String registerEvent(Event event);
	
	Event getEvent(String userId, String eventId);
	
	/**
	 * 사용자가 즐겨찾기한 동호회/바의 이벤트 목록을 조회한다.
	 * 
	 * @param userId
	 * @param pageInfo
	 * @return
	 */
	Page<Event> getEvents(String userId, PageInfo pageInfo);
	
	Page<Event> getEventsByCategory(String userId, String category, PageInfo pageInfo);
	Page<Event> getEventsByClubId(String userId, String clubId, PageInfo pageInfo);
	Page<Event> getEventsByBarId(String userId, String clubId, PageInfo pageInfo);
	List<Event> getFavoriteEvents(String userId);

	void modifyEvent(Event event);
	void followEvent(String userId, String eventId);
	void unfollowEvent(String userId, String eventId);

	void removeEvent(String eventId);

}
