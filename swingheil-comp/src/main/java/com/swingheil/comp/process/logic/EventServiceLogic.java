package com.swingheil.comp.process.logic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swingheil.comp.entity.BarEntity;
import com.swingheil.comp.entity.ClubEntity;
import com.swingheil.comp.entity.EventEntity;
import com.swingheil.comp.entity.UserEntity;
import com.swingheil.comp.process.EventService;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.enums.EventCategory;

@Service
public class EventServiceLogic implements EventService {
	
	@Autowired
	private EventEntity eventEntity;
	
	@Autowired
	private UserEntity userEntity;
	
	@Autowired
	private BarEntity barEntity;
	
	@Autowired
	private ClubEntity clubEntity;
	
	@Override
	public String registerEvent(Event event) {
		// 
		// 1. 이벤트 등록
		String eventId = eventEntity.create(event);
		
		// 2. 스폰서 정보 세팅 및 스폰서(클럽/바)의 최종수정일시에 이벤트 등록일시 업데이트
		EventCategory category = EventCategory.findBy(event.getCategory().getCode());
		String sponsorId = event.getSponsor().getId();
		switch (category) {
		case SwingBar:
			Bar bar = barEntity.retrieve(sponsorId);
			bar.setLastUpdateDate(event.getRegDate());
			barEntity.update(bar);
			event.setSponsor(bar);
			break;
		case Club:
			Club club = clubEntity.retrieve(sponsorId);
			club.setLastUpdateDate(event.getRegDate());
			clubEntity.update(club);
			event.setSponsor(club);
			break;
		default:
			break;
		}
			
		return eventId;
	}

	@Override
	public Event getEvent(String userId, String eventId) {
		// 
		Event event = eventEntity.retrieve(eventId);
		
		if (event != null && event.getCategory() != null) {
			CodeName category = event.getCategory();
			category.setName(EventCategory.findBy(category.getCode()).nameKor());
		}
		
		return markFavoriteAndReturn(userId, event);
	}
	
	@Override
	public Page<Event> getEvents(String userId, PageInfo pageInfo) {
		// 
		List<String> favoriteSponsors = new ArrayList<String>();
		
		// SwingHeil은 필수조회 - swingHeil은 sponsorId가 null임
		if(userId.equals("admin")){
			favoriteSponsors.add(null);			
		}
		
		List<String> favoriteBars = userEntity.retriveFavoriteBars(userId);
		if (favoriteBars != null) {
			favoriteSponsors.addAll(favoriteBars);
		}
		
		List<String> favoriteClubs = userEntity.retriveFavoriteClubs(userId);
		if (favoriteClubs != null) {
			favoriteSponsors.addAll(favoriteClubs);
		}
		
		if (!favoriteSponsors.isEmpty()) {
			Page<Event> page = eventEntity.retriveAllBySponsorIds(favoriteSponsors, pageInfo);
			return markFavoriteAndReturn(userId, page);
		}
		return new Page<Event>(pageInfo);
	}

	@Override
	public Page<Event> getEventsByCategory(String userId, String category, PageInfo pageInfo) {
		//
		Page<Event> page = eventEntity.retriveAllByCategory(category, pageInfo);
		return markFavoriteAndReturn(userId, page);
	}
	
	@Override
	public Page<Event> getEventsByClubId(String userId, String clubId, PageInfo pageInfo) {
		//
		Page<Event> page = eventEntity.retriveAllByClubId(clubId, pageInfo);
		return markFavoriteAndReturn(userId, page);
	}
	
	@Override
	public Page<Event> getEventsByBarId(String userId, String barId, PageInfo pageInfo) {
		//
		Page<Event> page = eventEntity.retriveAllByBarId(barId, pageInfo);
		return markFavoriteAndReturn(userId, page);
	}

	@Override
	public List<Event> getFavoriteEvents(String userId) {
		// 
		List<String> favorites = userEntity.retriveFavoriteEvents(userId);
		if (favorites == null || favorites.isEmpty()) 
			return null;
		
		List<Event> events = new ArrayList<Event>();
		for (String eventId : favorites) {
			Event event = eventEntity.retrieve(eventId);
			event.setFavorite(true);
			if (event != null) events.add(event);
		}
		return events;
	}

	@Override
	public void modifyEvent(Event event) {
		// 
		eventEntity.update(event);
	}

	@Override
	public void followEvent(String userId, String eventId) {
		//
		userEntity.addFavoriteEvent(userId, eventId);
	}

	@Override
	public void unfollowEvent(String userId, String eventId) {
		//
		userEntity.removeFavoriteEvent(userId, eventId);
	}

	@Override
	public void removeEvent(String eventId) {
		// 
		eventEntity.delete(eventId);
	}
	
	//--------------------------------------------------------------------------
	
	/**
	 * 즐겨찾기한 이벤트에 마킹한다.
	 * 
	 * @param userId
	 * @param event
	 * @return
	 */
	private Event markFavoriteAndReturn(String userId, Event event) {
		//
		if (userId == null) return event;
		
		List<String> favorites = userEntity.retriveFavoriteEvents(userId);
		if (favorites != null && !favorites.isEmpty() && event != null) {

			if (favorites.contains(event.getId())) {
				event.setFavorite(true);
			}
		}
		return event;
	}
	
	/**
	 * 즐겨찾기한 이벤트에 마킹한다.
	 * 
	 * @param userId
	 * @param page
	 * @return
	 */
	private Page<Event> markFavoriteAndReturn(String userId, Page<Event> page) {
		//
		if (userId == null) return page;
		
		List<String> favorites = userEntity.retriveFavoriteEvents(userId);
		if (favorites != null && !favorites.isEmpty() && page.hasResults()) {
			
			for (Event event : page.getResults()) {
				if (favorites.contains(event.getId()) && event.isVisible() == true) {
					event.setFavorite(true);
				}
			}
		}
		return page;
	}
}
