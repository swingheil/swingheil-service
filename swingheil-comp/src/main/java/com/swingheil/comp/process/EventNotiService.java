package com.swingheil.comp.process;

import com.swingheil.domain.event.Event;

public interface EventNotiService {
	void sendPushMessage(Event event);
}
