package com.swingheil.comp.process;

import java.util.List;

import com.swingheil.domain.group.Club;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface ClubService {
	//
	String registerClub(Club club);
	void modifyClub(Club club);
	
	Club getClub(String userId, String clubId);
	Page<Club> getClubs(String userId, PageInfo pageInfo);

	void followClub(String userId, String clubId);
	void unfollowClub(String userId, String clubId);
	
	List<Club> getManagedClubs(String adminId);
	
	void joinAsStaff(String clubId, String userId);
	void withdrawAsStaff(String clubId, String userId);
	
	void activateAsStaff(String clubId, String userId);
	
	void delegateAdmin(String clubId, String userId);

}
