package com.swingheil.comp.process.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swingheil.comp.entity.BarEntity;
import com.swingheil.comp.entity.ClubEntity;
import com.swingheil.comp.entity.UserEntity;
import com.swingheil.comp.process.BarService;
import com.swingheil.domain.group.Bar;
import com.swingheil.domain.group.Staff;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;
import com.swingheil.domain.type.enums.StaffState;
import com.swingheil.domain.user.SocialUser;

@Service
public class BarServiceLogic implements BarService {

	@Autowired
	private BarEntity barEntity;
	
	@Autowired
	private ClubEntity clubEntity;
	
	@Autowired
	private UserEntity userEntity;
	
	@Override
	public String registerBar(Bar bar) {
		//
		return barEntity.create(bar);
	}

	@Override
	public void modifyBar(Bar bar) {
		// 
		barEntity.update(bar);
	}

	@Override
	public Bar getBar(String userId, String barId) {
		// 
		Bar bar = barEntity.retrieve(barId);
		
		// 상세 조회시 클럽 정보(id)가 필요함
		bar.setClubs(clubEntity.retrieveByBar(barId));
		
		return markFavoriteAndReturn(userId, bar);
	}

	@Override
	public Page<Bar> getBars(String userId, PageInfo pageInfo) {
		// 
		Page<Bar> page = barEntity.retrieveAll(pageInfo);;
		return markFavoriteAndReturn(userId, page);
	}

	@Override
	public void followBar(String userId, String barId) {
		//
		userEntity.addFavoriteBar(userId, barId);
	}

	@Override
	public void unfollowBar(String userId, String barId) {
		//
		userEntity.removeFavoriteBar(userId, barId);
	}
	
	//--------------------------------------------------------------------------
	
	/**
	 * 즐겨찾기한 클럽에 마킹한다.
	 * 
	 * @param userId
	 * @param bar
	 * @return
	 */
	private Bar markFavoriteAndReturn(String userId, Bar bar) {
		//
		if (userId == null) return bar;
		
		List<String> favorites = userEntity.retriveFavoriteBars(userId);
		if (favorites != null && !favorites.isEmpty() && bar != null) {

			if (favorites.contains(bar.getId())) {
				bar.setFavorite(true);
				bar.setClubs(clubEntity.retrieveByBar(bar.getId()));
			}
		}
		return bar;
	}
	
	/**
	 * 즐겨찾기한 클럽에 마킹한다.
	 * 
	 * @param userId
	 * @param page
	 * @return
	 */
	private Page<Bar> markFavoriteAndReturn(String userId, Page<Bar> page) {
		//
		if (userId == null) return page;
		
		List<String> favorites = userEntity.retriveFavoriteBars(userId);
		if (favorites != null && !favorites.isEmpty() && page.hasResults()) {
			
			for (Bar bar : page.getResults()) {
				if (favorites.contains(bar.getId())) {
					bar.setFavorite(true);
					bar.setClubs(clubEntity.retrieveByBar(bar.getId()));
				}
			}
		}
		return page;
	}

	@Override
	public void joinAsStaff(String barId, String userId) {
		//
		
		List<Staff> staffs = barEntity.retrieve(barId).getStaffs();
		if (staffs != null && !staffs.isEmpty()) {
			for(Staff staff : staffs) {
				if(staff.getId().equals(userId) == true) {
					throw new RuntimeException("이미 등록된 운영자입니다");
				}
			}
		}
		
		SocialUser socialUser = userEntity.retrieve(userId);
		Staff staff = new Staff(socialUser);
		staff.setState(StaffState.Requested);
		
		barEntity.addStaff(barId, staff);
		
		// 최초 등록시 운영자가 없으면 자동으로 Admin 등록
		if(staffs == null || staffs.isEmpty()) {
			userEntity.addFavoriteBar(userId, barId);
			barEntity.delegateAdmin(barId, staff);
			this.activateAsStaff(barId, userId);
		}
	}

	@Override
	public void withdrawAsStaff(String barId, String userId) {
		//
		userEntity.removeFavoriteBar(userId, barId);
		
		barEntity.removeStaff(barId, userId);
	}
	
	@Override
	public void activateAsStaff(String barId, String userId) {
		//
		// 운영자로 승인시 즐겨찾기
		userEntity.addFavoriteBar(userId, barId);
		
		barEntity.activateAsStaff(barId, userId);
	}

	@Override
	public void deleageAdmin(String barId, String userId) {
		//
		// TODO 위임하고자 하는 관리자가 해당 바의 운영자인지 체크해야 함
		SocialUser socialUser = userEntity.retrieve(userId);
		Staff staff = new Staff(socialUser);
		
		barEntity.delegateAdmin(barId, staff);
	}

	@Override
	public List<Bar> getManagedBars(String adminId) {
		//
		return barEntity.retriveManagedBars(adminId);
	}
}
