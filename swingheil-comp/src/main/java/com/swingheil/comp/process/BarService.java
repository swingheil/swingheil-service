package com.swingheil.comp.process;

import java.util.List;

import com.swingheil.domain.group.Bar;
import com.swingheil.domain.shared.Page;
import com.swingheil.domain.shared.PageInfo;

public interface BarService {
	//
	String registerBar(Bar bar);
	void modifyBar(Bar bar);
	
	Bar getBar(String userId, String barId);
	Page<Bar> getBars(String userId, PageInfo pageInfo);
	
	List<Bar> getManagedBars(String adminId);

	void followBar(String userId, String barId);
	void unfollowBar(String userId, String barId);
	
	void joinAsStaff(String barId, String userId);
	void withdrawAsStaff(String barId, String userId);
	
	void activateAsStaff(String barId, String userId);
	
	void deleageAdmin(String barId, String userId);

}
