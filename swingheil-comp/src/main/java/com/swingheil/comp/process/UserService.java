package com.swingheil.comp.process;

import java.util.List;

import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

public interface UserService {
	//
	SocialUser registerUser(String nickname, Device device);
	List<SocialUser> getAllUsers();
	List<SocialUser> getUsersByNickname(String nickname);
	List<SocialUser> getUsersByPhoneNo(String phoneNo);
	List<SocialUser> getUsersByEmail(String email);
	List<Device> getUserDeviceList(String userId);
	
	List<SocialUser> getUsersByBar(String barId);
	List<SocialUser> getUsersByClub(String clubId);
	
	void addUserDevice(String userId, Device device);
	void removeUserDevice(String userId, String deviceUid);

	/**
	 * deviceUid로 사용자 기본정보를 조회한다.
	 * <pre>
	 *   userId, email, nickname
	 * </pre>
	 * @param deviceUid
	 * @return {@link SocialUser}
	 */
	SocialUser getUserByDeviceUid(String deviceUid);
	
	/**
	 * 로그인 가능여부 체크
	 * 
	 * @param email
	 * @param password
	 * @return 
	 */
	String getUserIdByEmailAndPassword(String email, String password);
	
	/**
	 * userId로 사용자 기본정보를 조회한다.
	 * 
	 * @param deviceUid
	 * @return {@link SocialUser}
	 */
	SocialUser getUserByUserid(String userId);
	
	/**
	 * 사용자정보 수정
	 * 
	 * @param userId
	 * @param email
	 * @param password
	 * @param phone
	 */
	void updateUserDetail(String userId, String email, String password, String phone);	
}
