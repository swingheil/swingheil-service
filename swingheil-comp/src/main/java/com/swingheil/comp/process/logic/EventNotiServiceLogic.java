package com.swingheil.comp.process.logic;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.swingheil.comp.entity.PushEntity;
import com.swingheil.comp.entity.repo.PushRepository;
import com.swingheil.comp.process.EventNotiService;
import com.swingheil.comp.process.NotificationService;
import com.swingheil.comp.process.UserService;
import com.swingheil.domain.event.Event;
import com.swingheil.domain.notification.Push;
import com.swingheil.domain.type.CodeName;
import com.swingheil.domain.type.enums.NotiType;
import com.swingheil.domain.user.Device;
import com.swingheil.domain.user.SocialUser;

@Service
public class EventNotiServiceLogic implements EventNotiService {

	@Autowired
	private UserService userServcie;
	
	@Autowired
	private NotificationService notiService;
	
	@Autowired
	private PushEntity pushEntity;
	
	@Override
	public void sendPushMessage(Event event) {
		//
		NotiType notiType = null;
		
		CodeName codeName = event.getCategory();
		
		List<SocialUser> users = null;
		
		String id = event.getSponsor().getId();
		
		if(codeName.getCode().equals("01")) {
			// 스윙하일
			// 전체 사용자에 대해 push message를 전송
			users = userServcie.getAllUsers();
			
			notiType = NotiType.SwingHeil;
			
		} else if(codeName.getCode().equals("02")) {
			// 스윙바
			users = userServcie.getUsersByBar(id);
			
			notiType = NotiType.Bar;
			
		} else if(codeName.getCode().equals("03")) {
			// 동호회
			users = userServcie.getUsersByClub(id);
			
			notiType = NotiType.Club;
		}
		
		String message = StringUtils.hasText(event.getSummary()) ? event.getSummary() : event.getDetail();
		
		List<String> regIds = new ArrayList<String>();
		
		if(users != null) {
			for(SocialUser user : users) {
				if(user.getDevices() != null) {
					for(Device device : user.getDevices()) {
						Push push = new Push();
						push.setMessage(message);
						push.setDeviceId(device.getRegistrationId());
						push.setPushType(notiType.toString());
						push.setTag1(event.getId());
						
						pushEntity.create(push);
						
//						regIds.add(device.getRegistrationId());
					}
				}
			}
		}
		
//		if(regIds != null && regIds.size() > 0) {
//			
//			
//			notiService.sendPushMessage(event.getTitle(), message, notiType, event.getId(), regIds);
//		}
	}
}
